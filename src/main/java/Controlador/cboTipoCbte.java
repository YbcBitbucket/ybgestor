package Controlador;

public class cboTipoCbte {
    
  
    private int idcbte;
    private String descripcion;
    
    public cboTipoCbte(){}
    
    public cboTipoCbte(int idcbte, String descripcion){
        this.idcbte = idcbte;
        this.descripcion = descripcion;
    }

    public int getidcbte() {
        return idcbte;
    }

    public void setcodigo(int idcbte) {
        this.idcbte = idcbte;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
