package Controlador;

public class cboCategoria {
    
  
    private int idCategorias;
    private String nombre;
    
    public cboCategoria(){}
    
    public cboCategoria(int idCategorias, String nombre){
        this.idCategorias = idCategorias;
        this.nombre = nombre;
    }

    public int getidCategorias() {
        return idCategorias;
    }

    public void setidCategorias(int idCategorias) {
        this.idCategorias = idCategorias;
    }

    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString(){
        return this.nombre;
    }
    
}
