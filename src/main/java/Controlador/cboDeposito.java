package Controlador;

public class cboDeposito {
    
  
    private int idDeposito;
    private String nombreDeposito;
    
    public cboDeposito(){}
    
    public cboDeposito(int id, String nombre){
        this.idDeposito = id;
        this.nombreDeposito = nombre;
    }

    public int getidDeposito() {
        return idDeposito;
    }

    public void setidDeposito(int idDeposito) {
        this.idDeposito = idDeposito;
    }

    public String getnombreDeposito() {
        return nombreDeposito;
    }

    public void setnombreDeposito(String nombreDeposito) {
        this.nombreDeposito = nombreDeposito;
    }
    
    public String toString(){
        return this.nombreDeposito;
    }
    
}
