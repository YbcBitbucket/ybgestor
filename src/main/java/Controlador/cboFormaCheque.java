package Controlador;

public class cboFormaCheque {
    
  
    private int idFormaPago_Cheque;
    private String descripcionFormaPago_Cheque;
    private double descuentoFormaPago_Cheque;
    
    public cboFormaCheque(){}
    
    public cboFormaCheque(int id, String nombre, double descuento){
        this.idFormaPago_Cheque = id;
        this.descripcionFormaPago_Cheque = nombre;
        this.descuentoFormaPago_Cheque = descuento;
    }

    public int getidFormaPago_Cheque() {
        return idFormaPago_Cheque;
    }

    public void setidFormaPago_Cheque(int idFormaPago_Cheque) {
        this.idFormaPago_Cheque = idFormaPago_Cheque;
    }

    public String getdescripcionFormaPago_Cheque() {
        return descripcionFormaPago_Cheque;
    }

    public void setdescripcionFormaPago_Cheque(String descripcionFormaPago_Cheque) {
        this.descripcionFormaPago_Cheque = descripcionFormaPago_Cheque;
    }
    
    public Double getdescuentoFormaPago_Cheque() {
        return descuentoFormaPago_Cheque;
    }

    public void setdescuentoFormaPago_Cheque(Double descuentoFormaPago_Cheque) {
        this.descuentoFormaPago_Cheque = descuentoFormaPago_Cheque;
    }
    
    public String toString(){
        return this.descripcionFormaPago_Cheque;
    }
    
}
