package Controlador;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class fnCargarFecha {
    
    public fnCargarFecha() {
    }
    
    public static String cargarfecha() {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
    public static String cargarfechaTipoDate() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String fecha = formato.format(currentDate);
        return fecha;
    }
    
    public static String cargarHora() {
        SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        String hora = formato.format(currentDate);
        return hora;
    }
    
}
  
