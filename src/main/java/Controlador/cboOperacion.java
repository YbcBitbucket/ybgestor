package Controlador;

public class cboOperacion {
    
  
    private int idOperacion;
    private String descripcion;
    
    public cboOperacion(){}
    
    public cboOperacion(int idOperacion, String descripcion){
        this.idOperacion = idOperacion;
        this.descripcion = descripcion;
    }

    public int getidOperacion() {
        return idOperacion;
    }

    public void setidOperacion(int idOperacion) {
        this.idOperacion = idOperacion;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
