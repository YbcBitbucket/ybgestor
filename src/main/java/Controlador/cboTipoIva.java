package Controlador;

public class cboTipoIva {
    
  
    private int idTipoIva;
    private String descripcion;
    
    public cboTipoIva(){}
    
    public cboTipoIva(int idTipoIva, String descripcion){
        this.idTipoIva = idTipoIva;
        this.descripcion = descripcion;
    }

    public int getidTipoIva() {
        return idTipoIva;
    }

    public void setidTipoIva(int idTipoIva) {
        this.idTipoIva = idTipoIva;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
