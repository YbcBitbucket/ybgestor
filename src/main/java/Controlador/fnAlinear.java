package Controlador;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class fnAlinear {

    DefaultTableCellRenderer alinearCentro, alinearDerecha, alinearIzquierda;

    public DefaultTableCellRenderer alinearCentro() {
        alinearCentro = new DefaultTableCellRenderer();
        alinearCentro.setHorizontalAlignment(SwingConstants.CENTER);
        return alinearCentro;
    }

    public DefaultTableCellRenderer alinearDerecha() {
        alinearDerecha = new DefaultTableCellRenderer();
        alinearDerecha.setHorizontalAlignment(SwingConstants.RIGHT);
        return alinearDerecha;
    }

    public DefaultTableCellRenderer alinearIzquierda() {
        alinearIzquierda = new DefaultTableCellRenderer();
        alinearIzquierda.setHorizontalAlignment(SwingConstants.LEFT);
        return alinearIzquierda;
    }

}
