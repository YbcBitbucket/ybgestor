package Controlador;

public class camposingresos { 
    
    String id_pedido, cliente,forma_de_pago,fecha_ingreso ,codigo,producto,precio_venta,bonificacion,cantidad,iva,descuento ,interes, total;

    public camposingresos(String id_pedido, String cliente, String forma_de_pago, String fecha_ingreso, String codigo, String producto,String precio_venta,String bonificacion, String cantidad,String iva, String descuento,String interes, String total) {
        this.id_pedido = id_pedido;
        this.cliente = cliente;
        this.forma_de_pago = forma_de_pago;
        this.fecha_ingreso = fecha_ingreso;
        this.codigo = codigo;
        this.producto = producto;
        this.bonificacion = bonificacion;
        this.precio_venta = precio_venta;
        this.iva = iva;
        this.interes = interes;
        this.cantidad = cantidad;
        this.descuento = descuento;
        this.total = total;
    }

    public void setPrecio_venta(String precio_venta) {
        this.precio_venta = precio_venta;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }

    public String getPrecio_venta() {
        return precio_venta;
    }

    public String getIva() {
        return iva;
    }

    public String getInteres() {
        return interes;
    }

    public String getId_pedido() {
        return id_pedido;
    }

    public String getCliente() {
        return cliente;
    }

    public String getForma_de_pago() {
        return forma_de_pago;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getProducto() {
        return producto;
    }

    public String getBonificacion() {
        return bonificacion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public String getDescuento() {
        return descuento;
    }

    public String getTotal() {
        return total;
    }

    public void setId_pedido(String id_pedido) {
        this.id_pedido = id_pedido;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public void setForma_de_pago(String forma_de_pago) {
        this.forma_de_pago = forma_de_pago;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public void setTotal(String total) {
        this.total = total;
    }

   

   
    
   
}