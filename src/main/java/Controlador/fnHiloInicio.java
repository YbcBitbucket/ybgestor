package Controlador;

import static Controlador.Configuracion.tomaDatos;
import Vista.Login;
import javax.swing.JProgressBar;

public class fnHiloInicio extends Thread {

    JProgressBar progreso;

    public fnHiloInicio(JProgressBar progreso1) {
        super();
        this.progreso = progreso1;
    }

    public void run() {
        for (int i = 1; (i <= 100); i++) {
            progreso.setValue(i);
            pausa(60);
        }
         tomaDatos();
        new Login().setVisible(true);
        
    }

    public void pausa(int mlSeg) {
        try {
            // pausa para el splash
            Thread.sleep(mlSeg);
        } catch (Exception e) {
        }
    }
}
