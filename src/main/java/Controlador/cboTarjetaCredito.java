package Controlador;

public class cboTarjetaCredito {
    
  
    private int idTarjetaCredito;
    private String descripcionTarjetaCredito;
    private double descuentoTarjetaCredito;
    
    public cboTarjetaCredito(){}
    
    public cboTarjetaCredito(int id, String nombre, double descuento){
        this.idTarjetaCredito = id;
        this.descripcionTarjetaCredito = nombre;
        this.descuentoTarjetaCredito = descuento;
    }

    public int getidTarjetaCredito() {
        return idTarjetaCredito;
    }

    public void setidTarjetaCredito(int idTarjetaCredito) {
        this.idTarjetaCredito = idTarjetaCredito;
    }

    public String getdescripcionTarjetaCredito() {
        return descripcionTarjetaCredito;
    }

    public void setdescripcionTarjetaCredito(String descripcionTarjetaCredito) {
        this.descripcionTarjetaCredito = descripcionTarjetaCredito;
    }
    
    public Double getdescuentoTarjetaCredito() {
        return descuentoTarjetaCredito;
    }

    public void setdescuentoTarjetaCredito(Double descuentoTarjetaCredito) {
        this.descuentoTarjetaCredito = descuentoTarjetaCredito;
    }
    
    public String toString(){
        return this.descripcionTarjetaCredito;
    }
    
}
