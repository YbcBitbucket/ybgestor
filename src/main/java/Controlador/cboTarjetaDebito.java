package Controlador;

public class cboTarjetaDebito {
    
  
    private int idTarjetaDebito;
    private String descripcionTarjetaDebito;
    private double descuentoTarjetaDebito;
    
    public cboTarjetaDebito(){}
    
    public cboTarjetaDebito(int id, String nombre, double descuento){
        this.idTarjetaDebito = id;
        this.descripcionTarjetaDebito = nombre;
        this.descuentoTarjetaDebito = descuento;
    }

    public int getidTarjetaDebito() {
        return idTarjetaDebito;
    }

    public void setidTarjetaDebito(int idTarjetaDebito) {
        this.idTarjetaDebito = idTarjetaDebito;
    }

    public String getdescripcionTarjetaDebito() {
        return descripcionTarjetaDebito;
    }

    public void setdescripcionTarjetaDebito(String descripcionTarjetaDebito) {
        this.descripcionTarjetaDebito = descripcionTarjetaDebito;
    }
    
    public Double getdescuentoTarjetaDebito() {
        return descuentoTarjetaDebito;
    }

    public void setdescuentoTarjetaDebito(Double descuentoTarjetaDebito) {
        this.descuentoTarjetaDebito = descuentoTarjetaDebito;
    }
    
    public String toString(){
        return this.descripcionTarjetaDebito;
    }
    
}
