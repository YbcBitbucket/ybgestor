package Controlador;

import static Controlador.Configuracion.direccionip;
import static Controlador.Configuracion.puerto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class ConexionMySQL {
    public String db = "carniceria";
  //  public String url = "jdbc:mysql://localhost:3306/"+db;
    public String url ="jdbc:mysql://"+direccionip+":"+puerto+"/"+db;
    public String user = "root";
   
 
     //public String pass = "root";
     public String pass = "bruluma31";

    

    
    public Connection Conectar()
    {
        Connection link = null;
        try
        {
            //Cargamos el Driver MySQL
            Class.forName("org.gjt.mm.mysql.Driver");
            //Creamos un enlace hacia la base de datos
            link = DriverManager.getConnection(this.url, this.user, this.pass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexionMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
        return link;  
    }
}

