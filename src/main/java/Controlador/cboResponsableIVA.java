package Controlador;

public class cboResponsableIVA {
    
  
    private int idTiporesponsable;
    private String descripcion;
    
    public cboResponsableIVA(){}
    
    public cboResponsableIVA(int idTiporesponsable, String descripcion){
        this.idTiporesponsable = idTiporesponsable;
        this.descripcion = descripcion;
    }

    public int getidTiporesponsable() {
        return idTiporesponsable;
    }

    public void setidTiporesponsable(int idTiporesponsable) {
        this.idTiporesponsable = idTiporesponsable;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
