package Controlador;

public class cboFormaCtaCte {
    
  
    private int idFormaPago_CtaCte;
    private String descripcionFormaPago_CtaCte;
    private double descuentoFormaPago_CtaCte;
    
    public cboFormaCtaCte(){}
    
    public cboFormaCtaCte(int id, String nombre, double descuento){
        this.idFormaPago_CtaCte = id;
        this.descripcionFormaPago_CtaCte = nombre;
        this.descuentoFormaPago_CtaCte = descuento;
    }

    public int getidFormaPago_CtaCte() {
        return idFormaPago_CtaCte;
    }

    public void setidFormaPago_CtaCte(int idFormaPago_CtaCte) {
        this.idFormaPago_CtaCte = idFormaPago_CtaCte;
    }

    public String getdescripcionFormaPago_CtaCte() {
        return descripcionFormaPago_CtaCte;
    }

    public void setdescripcionFormaPago_CtaCte(String descripcionFormaPago_CtaCte) {
        this.descripcionFormaPago_CtaCte = descripcionFormaPago_CtaCte;
    }
    
    public Double getdescuentoFormaPago_CtaCte() {
        return descuentoFormaPago_CtaCte;
    }

    public void setdescuentoFormaPago_CtaCte(Double descuentoFormaPago_CtaCte) {
        this.descuentoFormaPago_CtaCte = descuentoFormaPago_CtaCte;
    }
    
    public String toString(){
        return this.descripcionFormaPago_CtaCte;
    }
    
}
