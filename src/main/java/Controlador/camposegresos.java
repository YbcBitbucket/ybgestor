

package Controlador;

public class camposegresos {
    
    String nombre_egreso, fecha_egreso, total;

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public camposegresos(String nombre_egreso, String fecha_egreso, String total) {
        this.nombre_egreso = nombre_egreso;
        this.fecha_egreso = fecha_egreso;
        this.total = total;
    }

    public String getNombre_egreso() {
        return nombre_egreso;
    }

    public void setNombre_egreso(String nombre_egreso) {
        this.nombre_egreso = nombre_egreso;
    }

    public String getFecha_egreso() {
        return fecha_egreso;
    }

    public void setFecha_egreso(String fecha_egreso) {
        this.fecha_egreso = fecha_egreso;
    }

    
    
    
}
