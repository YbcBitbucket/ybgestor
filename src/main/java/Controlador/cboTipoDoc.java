package Controlador;

public class cboTipoDoc {
    
  
    private int idTipodoc;
    private String descripcion;
    
    public cboTipoDoc(){}
    
    public cboTipoDoc(int idTipodoc, String descripcion){
        this.idTipodoc = idTipodoc;
        this.descripcion = descripcion;
    }

    public int getidTipodoc() {
        return idTipodoc;
    }

    public void setidTipodoc(int idTipodoc) {
        this.idTipodoc = idTipodoc;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
