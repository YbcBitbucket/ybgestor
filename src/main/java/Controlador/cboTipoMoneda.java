package Controlador;

public class cboTipoMoneda {
    
  
    private int idMoneda;
    private String descripcion;
    
    public cboTipoMoneda(){}
    
    public cboTipoMoneda(int idMoneda, String descripcion){
        this.idMoneda = idMoneda;
        this.descripcion = descripcion;
    }

    public int getidMoneda() {
        return idMoneda;
    }

    public void setidMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
