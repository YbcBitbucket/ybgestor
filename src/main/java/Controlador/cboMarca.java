package Controlador;

public class cboMarca {
  
    private int idMarcas;
    private String nombre;
    
    public cboMarca(){}
    
    public cboMarca(int idMarcas, String nombre){
        this.idMarcas = idMarcas;
        this.nombre = nombre;
    }

    public int getidMarcas() {
        return idMarcas;
    }

    public void setidMarcas(int idMarcas) {
        this.idMarcas = idMarcas;
    }

    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString(){
        return this.nombre;
    }
    
}
