package Controlador;

public class fnRedondear {
    
    
    public double tresDigitos (double numero) {
        return Math.rint(numero * 1000) / 1000;
    }
    
    public double dosDigitos(double numero) {
        return Math.rint(numero * 100) / 100;
    }
    
     public double UNDigitos(double numero) {
        return Math.round(numero);
    }
}
  
