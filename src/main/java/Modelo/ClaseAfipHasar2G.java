package Modelo;

import hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal;
import hfl.argentina.HasarException;
import hfl.argentina.HasarImpresoraFiscalRG3561;
import hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarConfiguracionImpresoraFiscal;
import hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora;
import hfl.argentina.Hasar_Funcs.AtributosDeTexto;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import Controlador.ConexionMySQL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ClaseAfipHasar2G {

    public String[] ObtenerConfiguracion() {
        String[] DatosConfig = new String[4];

        String facturacion_usuario = "SELECT * FROM vista_facturacion_usuario";
        String Usuario_direccionip = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectfacturacion_usuario = null;
        try {

            Selectfacturacion_usuario = cn.createStatement();
            ResultSet rs_usuario = Selectfacturacion_usuario.executeQuery(facturacion_usuario);
            rs_usuario.next();
            Usuario_direccionip = rs_usuario.getString("direccionip");

            HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();
            p.conectar(Usuario_direccionip);

            RespuestaConsultarConfiguracionImpresoraFiscal r = p.ConsultarConfiguracionImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.LIMITE_BC);
            //System.out.println("Limete BC:                  " + r.getValor());
            DatosConfig[0] = r.getValor();
            RespuestaConsultarConfiguracionImpresoraFiscal r1 = p.ConsultarConfiguracionImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CAMBIO);
            DatosConfig[1] = r1.getValor();
            RespuestaConsultarConfiguracionImpresoraFiscal r2 = p.ConsultarConfiguracionImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_LEYENDAS);
            DatosConfig[2] = r2.getValor();
            RespuestaConsultarConfiguracionImpresoraFiscal r3 = p.ConsultarConfiguracionImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.CORTE_PAPEL);
            DatosConfig[3] = r3.getValor();

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }

        return DatosConfig;
    }

    public void Configuracion(String LimiteBC, String ImpresionCambio, String ImpresionLeyendas, String CortePapel) {

        String facturacion_usuario = "SELECT * FROM vista_facturacion_usuario";
        String Usuario_direccionip = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectfacturacion_usuario = null;
        try {

            Selectfacturacion_usuario = cn.createStatement();
            ResultSet rs_usuario = Selectfacturacion_usuario.executeQuery(facturacion_usuario);
            rs_usuario.next();
            Usuario_direccionip = rs_usuario.getString("direccionip");

            HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();
            p.conectar(Usuario_direccionip);

            p.ConfigurarImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.LIMITE_BC, LimiteBC);
            p.ConfigurarImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CAMBIO, ImpresionCambio);
            p.ConfigurarImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_LEYENDAS, ImpresionLeyendas);
            p.ConfigurarImpresoraFiscal(HasarImpresoraFiscalRG3561.Configuracion.CORTE_PAPEL, CortePapel);
            JOptionPane.showMessageDialog(null, "La Configuracion se realizo con exito");

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ReporteZ() {
        String facturacion_usuario = "SELECT * FROM vista_facturacion_usuario";
        String Usuario_direccionip = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectfacturacion_usuario = null;
        PreparedStatement Insert = null;
        try {

            Selectfacturacion_usuario = cn.createStatement();
            ResultSet rs_usuario = Selectfacturacion_usuario.executeQuery(facturacion_usuario);
            rs_usuario.next();
            Usuario_direccionip = rs_usuario.getString("direccionip");

            HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();
            p.conectar(Usuario_direccionip);
            HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal rjf = p.CerrarJornadaFiscal(HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String stringDate = dateFormat.format(rjf.Z.getFecha());
            System.out.println("Fecha:                  " + stringDate);
            java.sql.Date sqlDate = java.sql.Date.valueOf(stringDate);

            String SQLInsertZ = "INSERT INTO afip_hasar_z (NroComprobante, Fecha , DF_CantidadCancelados , DF_CantidadEmitidos , DF_Total , DF_TotalExento , DF_TotalGravado , DF_TotalIVA , DF_TotalNoGravado , DF_TotalTributo , DNFH_CantidadEmitidos , DNFH_Total , NC_CantidadCancelados , NC_CantidadEmitidos , NC_Total , NC_TotalExento , NC_TotalGravado , NC_TotalIVA , NC_TotalNoGravado , NC_TotalTributo) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            Insert = cn.prepareStatement(SQLInsertZ);
            Insert.setInt(1, rjf.Z.getNumero());
            Insert.setDate(2, sqlDate);
            Insert.setInt(3, rjf.Z.getDF_CantidadCancelados());
            Insert.setInt(4, rjf.Z.getDF_CantidadEmitidos());
            Insert.setDouble(5, rjf.Z.getDF_Total());
            Insert.setDouble(6, rjf.Z.getDF_TotalExento());
            Insert.setDouble(7, rjf.Z.getDF_TotalGravado());
            Insert.setDouble(8, rjf.Z.getDF_TotalIVA());
            Insert.setDouble(9, rjf.Z.getDF_TotalNoGravado());
            Insert.setDouble(10, rjf.Z.getDF_TotalTributos());
            Insert.setInt(11, rjf.Z.getDNFH_CantidadEmitidos());
            Insert.setDouble(12, rjf.Z.getDNFH_Total());
            Insert.setInt(13, rjf.Z.getNC_CantidadCancelados());
            Insert.setInt(14, rjf.Z.getNC_CantidadEmitidos());
            Insert.setDouble(15, rjf.Z.getNC_Total());
            Insert.setDouble(16, rjf.Z.getNC_TotalExento());
            Insert.setDouble(17, rjf.Z.getNC_TotalGravado());
            Insert.setDouble(18, rjf.Z.getNC_TotalIVA());
            Insert.setDouble(19, rjf.Z.getNC_TotalNoGravado());
            Insert.setDouble(20, rjf.Z.getNC_TotalTributos());

            Insert.executeUpdate();

            System.out.println("Nro. comprobante:       " + rjf.Z.getNumero());
            System.out.println("Fecha:                  " + rjf.Z.getFecha());
            System.out.println("DF Cantidad cancelados: " + rjf.Z.getDF_CantidadCancelados());
            System.out.println("DF Cantidad emitidos:   " + rjf.Z.getDF_CantidadEmitidos());
            System.out.println("DF Total:               " + rjf.Z.getDF_Total());
            System.out.println("DF Total Exento:        " + rjf.Z.getDF_TotalExento());
            System.out.println("DF Total Gravado:       " + rjf.Z.getDF_TotalGravado());
            System.out.println("DF Total IVA:           " + rjf.Z.getDF_TotalIVA());
            System.out.println("DF Total No Gravado:    " + rjf.Z.getDF_TotalNoGravado());
            System.out.println("DF Total Tributo:       " + rjf.Z.getDF_TotalTributos());
            System.out.println("DNFH Cantidad emitidos: " + rjf.Z.getDNFH_CantidadEmitidos());
            System.out.println("DNFH Total:             " + rjf.Z.getDNFH_Total());
            System.out.println("NC Cantidad cancelados: " + rjf.Z.getNC_CantidadCancelados());
            System.out.println("NC Cantidad emitidos:   " + rjf.Z.getNC_CantidadEmitidos());
            System.out.println("NC Total:               " + rjf.Z.getNC_Total());
            System.out.println("NC Total Exento:        " + rjf.Z.getNC_TotalExento());
            System.out.println("NC Total Gravado:       " + rjf.Z.getNC_TotalGravado());
            System.out.println("NC Total IVA:           " + rjf.Z.getNC_TotalIVA());
            System.out.println("NC Total No Gravado:    " + rjf.Z.getNC_TotalNoGravado());
            System.out.println("NC Total Tributo:       " + rjf.Z.getNC_TotalTributos());

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ReporteX() {
        String facturacion_usuario = "SELECT * FROM vista_facturacion_usuario";
        String Usuario_direccionip = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Selectfacturacion_usuario = null;
        try {

            Selectfacturacion_usuario = cn.createStatement();
            ResultSet rs_usuario = Selectfacturacion_usuario.executeQuery(facturacion_usuario);
            rs_usuario.next();
            Usuario_direccionip = rs_usuario.getString("direccionip");

            HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();
            p.conectar(Usuario_direccionip);
            HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal rjf = p.CerrarJornadaFiscal(HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public String[] Factura_C(String ip, Object[][] datos, String formadepago, Double total, Double descuento, Double interes) {

        String[] datosFacC = new String[4];
        HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();

        try {
            p.conectar(ip);
            p.establecerTiempoDeEsperaConexion(15000);
            p.establecerTiempoDeEsperaRespuesta(15000);
            datosFacC[3] = String.valueOf(p.ConsultarFechaHora().getHora());
            HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento r = p.AbrirDocumento(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_C);
            int nroCompr = r.getNumeroComprobante();
            HasarImpresoraFiscalRG3561.CondicionesIVA i = HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO;
            AtributosDeTexto atri = new AtributosDeTexto();
            atri.setCentrado(true);
            atri.setDobleAncho(true);
            for (int j = 0; j < datos.length; j++) {
                p.ImprimirItem(String.valueOf(datos[j][0])/*descripcion*/, Double.valueOf(String.valueOf(datos[j][1]))/*CAntidad*/, Double.valueOf(String.valueOf(datos[j][2]))/*Precio*/, i/*CondicionesIVA.GRAVADO*/, Double.valueOf(String.valueOf(datos[j][3])) /*iva*/, HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, String.valueOf(datos[j][4])/*codigo producto*/);
                //Bonificacion
                if (Double.valueOf(String.valueOf(datos[j][5])) != 0) {
                    p.ImprimirDescuentoItem("Bonificacion", Double.valueOf(String.valueOf(datos[j][5])), HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);

                }
            }
            if (descuento != 0.0) {
                p.ImprimirDescuentoItem("Descuento", descuento, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);
            }
            if (interes != 0.0) {
                p.ImprimirAnticipoBonificacionEnvases("Recargo financiero ", interes, i/*CondicionesIVA.GRAVADO*/, 0, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001047"/*codigo producto*/, HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
            }

            HasarImpresoraFiscalRG3561.RespuestaImprimirPago rp = p.ImprimirPago(formadepago, total, HasarImpresoraFiscalRG3561.ModosDePago.PAGAR);
            p.CerrarDocumento();
            HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosComprobante ra = p.ConsultarAcumuladosComprobante(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_C, nroCompr);
            datosFacC[0] = String.valueOf(nroCompr);
            datosFacC[1] = String.valueOf(ra.RegDF.getTotal());
            datosFacC[2] = String.valueOf(ra.RegDF.getTotalIVA());

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
        return datosFacC;
    }

    public String[] Factura_B(String ip, Object[][] datos, String formadepago, Double total, Double descuento, Double interes) {

        String[] datosFacB = new String[4];
        HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();

        try {
            p.conectar(ip);
            p.establecerTiempoDeEsperaConexion(15000);
            p.establecerTiempoDeEsperaRespuesta(15000);
            datosFacB[3] = String.valueOf(p.ConsultarFechaHora().getHora());
            HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento r = p.AbrirDocumento(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B);

            int nroComprobante = r.getNumeroComprobante();
            HasarImpresoraFiscalRG3561.CondicionesIVA i = HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO;
            AtributosDeTexto atri = new AtributosDeTexto();
            atri.setCentrado(true);
            atri.setDobleAncho(true);
            for (int j = 0; j < datos.length; j++) {
                p.ImprimirItem(String.valueOf(datos[j][0])/*descripcion*/, Double.valueOf(String.valueOf(datos[j][1]))/*CAntidad*/, Double.valueOf(String.valueOf(datos[j][2]))/*Precio*/, i/*CondicionesIVA.GRAVADO*/, Double.valueOf(String.valueOf(datos[j][3])) /*iva*/, HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, String.valueOf(datos[j][4])/*codigo producto*/);
                //Bonificacion
                if (Double.valueOf(String.valueOf(datos[j][5])) != 0) {
                    p.ImprimirDescuentoItem("Bonificacion", Double.valueOf(String.valueOf(datos[j][5])), HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);

                }
            }
            if (descuento != 0.0) {
                p.ImprimirDescuentoItem("Descuento", descuento, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);
            }
            if (interes != 0.0) {
                p.ImprimirAnticipoBonificacionEnvases("Recargo financiero ", interes, i/*CondicionesIVA.GRAVADO*/, 0, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001047"/*codigo producto*/, HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
            }

            HasarImpresoraFiscalRG3561.RespuestaImprimirPago rp = p.ImprimirPago(formadepago, total, HasarImpresoraFiscalRG3561.ModosDePago.PAGAR);
            p.CerrarDocumento();
            HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosComprobante ra = p.ConsultarAcumuladosComprobante(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B, nroComprobante);

            datosFacB[0] = String.valueOf(nroComprobante);
            datosFacB[1] = String.valueOf(ra.RegDF.getTotal());
            datosFacB[2] = String.valueOf(ra.RegDF.getTotalIVA());

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }

        return datosFacB;

    }

    public String[] Factura_A(String ip, Object[][] datos, String formadepago, Double total, String Cli_RazonSocial, String Cli_Documento, String Cli_Domicilio, Double descuento, Double interes) {

        String[] datosFacA = new String[4];
        HasarImpresoraFiscalRG3561 p = new HasarImpresoraFiscalRG3561();

        try {
            p.conectar(ip);
            p.establecerTiempoDeEsperaConexion(15000);
            p.establecerTiempoDeEsperaRespuesta(15000);
            datosFacA[3] = String.valueOf(p.ConsultarFechaHora().getHora());
            p.CargarDatosCliente(Cli_RazonSocial, Cli_Documento, HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, Cli_Domicilio, "", "", "");

            HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento r = p.AbrirDocumento(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A);

            int nroComprobante = r.getNumeroComprobante();
            HasarImpresoraFiscalRG3561.CondicionesIVA i = HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO;
            AtributosDeTexto atri = new AtributosDeTexto();
            atri.setCentrado(true);
            atri.setDobleAncho(true);
            for (int j = 0; j < datos.length; j++) {
                p.ImprimirItem(String.valueOf(datos[j][0])/*descripcion*/, Double.valueOf(String.valueOf(datos[j][1]))/*CAntidad*/, Double.valueOf(String.valueOf(datos[j][2]))/*Precio*/, i/*CondicionesIVA.GRAVADO*/, Double.valueOf(String.valueOf(datos[j][3])) /*iva*/, HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, String.valueOf(datos[j][4])/*codigo producto*/);
                //Bonificacion
                if (Double.valueOf(String.valueOf(datos[j][5])) != 0) {
                    p.ImprimirDescuentoItem("Bonificacion", Double.valueOf(String.valueOf(datos[j][5])), HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);

                }
            }
            if (descuento != 0.0) {
                p.ImprimirDescuentoItem("Descuento", descuento, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL);
            }
            if (interes != 0.0) {
                p.ImprimirAnticipoBonificacionEnvases("Recargo financiero ", interes, i/*CondicionesIVA.GRAVADO*/, 0, HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0.00, HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001047"/*codigo producto*/, HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
            }
            HasarImpresoraFiscalRG3561.RespuestaImprimirPago rp = p.ImprimirPago(formadepago, total, HasarImpresoraFiscalRG3561.ModosDePago.PAGAR);
            p.CerrarDocumento();
            HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosComprobante ra = p.ConsultarAcumuladosComprobante(HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A, nroComprobante);

            datosFacA[0] = String.valueOf(nroComprobante);
            datosFacA[1] = String.valueOf(ra.RegDF.getTotal());
            datosFacA[2] = String.valueOf(ra.RegDF.getTotalIVA());

        } catch (HasarException ex) {
            Logger.getLogger(ClaseAfipHasar2G.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }

        return datosFacA;

    }

}
