package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import Controlador.ConexionMySQL;
import Controlador.camposbd;
import Controlador.fnCargarFecha;
import Controlador.fnCompletar;
import Controlador.fnRedondear;

public class ClaseCuentaCorriente {

    public ClaseCuentaCorriente() {
    }

    public void AgregarCtaCte(int IdCliente) {
        int IdCtaCte = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarCtaCte = null;
        Statement UltimoIdCtaCte = null;
        PreparedStatement AgregardetalleCtaCte = null;
        String sSQL = "";
        String SQL = "";
        fnCargarFecha f = new fnCargarFecha();
        if (IdCliente != 1) {
            try {
                cn.setAutoCommit(false); //transaction block start

                sSQL = "INSERT INTO cuentacorriente (fecha, estado, idClientes) VALUES(?,?,?)";
                AgregarCtaCte = cn.prepareStatement(sSQL);
                AgregarCtaCte.setString(1, f.cargarfecha());
                AgregarCtaCte.setString(2, "HABILITADO");
                AgregarCtaCte.setInt(3, IdCliente);
                AgregarCtaCte.executeUpdate();

                //Busco Ultimo ID CtaCte
                UltimoIdCtaCte = cn.createStatement();
                ResultSet rs = UltimoIdCtaCte.executeQuery("SELECT MAX(idCuentaCorriente) AS idCuentaCorriente FROM cuentacorriente");
                rs.next();
                IdCtaCte = rs.getInt("idCuentaCorriente");

                SQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, debe, haber, fecha) VALUES(?,?,?,?,?)";
                AgregardetalleCtaCte = cn.prepareStatement(SQL);
                AgregardetalleCtaCte.setInt(1, IdCtaCte);
                AgregardetalleCtaCte.setString(2, "INICIO DE ACTIVIDAD");
                AgregardetalleCtaCte.setDouble(3, 0.0);
                AgregardetalleCtaCte.setDouble(4, 0.0);
                AgregardetalleCtaCte.setString(5, f.cargarfecha());
                AgregardetalleCtaCte.executeUpdate();

                cn.commit(); //transaction block end

                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (AgregarCtaCte != null) {
                        AgregarCtaCte.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se puede crear una Cta Cte a un CONSUMIDOR FINAL");
        }
    }

    public void ModificarCatCte(int IdCliente, String estado) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarCtaCte = null;

        try {
            String sSQL = "UPDATE cuentacorriente SET"
                    + " estado=?"
                    + " WHERE idClientes=" + IdCliente;
            ModificarCtaCte = cn.prepareStatement(sSQL);
            ModificarCtaCte.setString(1, estado);

            int n = ModificarCtaCte.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarCtaCte != null) {
                    ModificarCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void PagoCatCte(int idCuentaCorriente, double haber, String detalle) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement PagoCtaCte = null;
        fnCargarFecha f = new fnCargarFecha();
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        try {
            String sSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, haber, fecha, detalle,id_caja) VALUES(?,?,?,?,?,?)";
            PagoCtaCte = cn.prepareStatement(sSQL);
            PagoCtaCte.setInt(1, idCuentaCorriente);
            PagoCtaCte.setString(2, "PAGO DE CTA CTE");
            PagoCtaCte.setDouble(3, haber);
            PagoCtaCte.setString(4, f.cargarfecha());
            PagoCtaCte.setString(5, detalle);
            PagoCtaCte.setInt(6, idCaja);
            int n = PagoCtaCte.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (PagoCtaCte != null) {
                    PagoCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void ImprimirPagoCtaCte(int IdPedidos) {
        LinkedList<camposbd> Resultados = new LinkedList<camposbd>();
        Resultados.clear();
        String cliente = null, fecha = null, subtotal = null, descuento = null, total = null, formadepago = null;
        String sqlPedidos = "SELECT * FROM vista_pedidos_detalle WHERE idPedidos=" + IdPedidos;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectPedidos = null;
        try {
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidos = SelectPedidos.executeQuery(sqlPedidos);
            while (rsPedidos.next()) {
                fnRedondear redondear = new fnRedondear();
                String Total = String.valueOf(redondear.dosDigitos(rsPedidos.getDouble("precioVenta") * (1 - (rsPedidos.getDouble("bonificacion") / 100)) * (1 + (rsPedidos.getDouble("tipoiva") / 100))) * rsPedidos.getInt("cantidad"));
                camposbd tipo;
                tipo = new camposbd(rsPedidos.getString("codigo_de_barra"), rsPedidos.getString("nombre"), rsPedidos.getString("precioVenta"), rsPedidos.getString("bonificacion"), rsPedidos.getString("tipoiva"), rsPedidos.getString("cantidad"), Total);
                Resultados.add(tipo);
                cliente = rsPedidos.getString("cliente");
                fecha = rsPedidos.getString("fecha");
                subtotal = rsPedidos.getString("subtotal");
                descuento = rsPedidos.getString("descuento");
                total = rsPedidos.getString("total");
                formadepago = rsPedidos.getString("formadepago");

            }
            JasperReport reporte;
            try {
                fnCompletar c = new fnCompletar();
                reporte = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/Ventas.jasper"));
                Map parametros = new HashMap();
                parametros.put("tipo", "Pedido");
                parametros.put("cliente", cliente);
                parametros.put("numero", c.completarString(String.valueOf(IdPedidos), 8));
                parametros.put("fecha", fecha);
                parametros.put("subtotal", subtotal);
                parametros.put("descuento", descuento);
                parametros.put("total", total);
                parametros.put("forma", formadepago);
                JasperPrint jPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(Resultados));
                JasperViewer visualizador = new JasperViewer(jPrint, false);
                visualizador.setVisible(true);

                //JasperPrintManager.printReport(jPrint, true);
            } catch (JRException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "El pedido no se pudo imprimir");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPedidos != null) {
                    SelectPedidos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public int ActualizacionCuentaCorriente(int idCuentaCorriente, double debe) {
        int estado = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement PagoCtaCte = null;
        fnCargarFecha f = new fnCargarFecha();
        try {
            String sSQL = "INSERT INTO detalledecuentacorriente (idCuentaCorriente, descripcion, debe, fecha,id_caja) VALUES(?,?,?,?,?)";
            PagoCtaCte = cn.prepareStatement(sSQL);
            PagoCtaCte.setInt(1, idCuentaCorriente);
            PagoCtaCte.setString(2, "ACTUALIZACION CUENTA CORRIENTE");
            PagoCtaCte.setDouble(3, debe);
            PagoCtaCte.setString(4, f.cargarfecha());
            PagoCtaCte.setInt(5, 0);
            int n = PagoCtaCte.executeUpdate();
            if (n > 0) {
                estado = 1;
            }
        } catch (SQLException ex) {
            estado = 0;
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (PagoCtaCte != null) {
                    PagoCtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return estado;
    }
}
