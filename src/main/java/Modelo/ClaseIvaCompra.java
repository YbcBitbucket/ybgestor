package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import Controlador.ConexionMySQL;
import Controlador.fnCompletar;
import Controlador.fnReversa;

public class ClaseIvaCompra {

    public ClaseIvaCompra() {
    }

    public void AgregarIvaCompra(String fechacbte, int idTipoCbte, int PuntosdeVentas, String numerocbte, int idProveedores, double importeoperacion, double importenointegraprecioneto, double importeoperacionesexentas, double inportevaloragregado, double importeimpuestosnacionales, double importeingresosbrutos, double importeimpuestosmunicipales, double importeimpuestosinternos, double otrostributos, int idTipomoneda, int idCodigoOperacion) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarIvaCompra = null;
        String sSQL = "INSERT INTO iva_compra(fechacbte, idTipoCbte, puntodeventa, "
                + "numerocbte, idProveedores, importeoperacion, "
                + "importenointegraprecioneto, importeoperacionesexentas, inportevaloragregado, "
                + "importeimpuestosnacionales, importeingresosbrutos, importeimpuestosmunicipales, "
                + "importeimpuestosinternos, otrostributos, idTipomoneda, idCodigoOperacion, tipocambio) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            cn.setAutoCommit(false); //transaction block start
            AgregarIvaCompra = cn.prepareStatement(sSQL);
            AgregarIvaCompra.setString(1, fechacbte);
            AgregarIvaCompra.setInt(2, idTipoCbte);
            AgregarIvaCompra.setInt(3, PuntosdeVentas);
            AgregarIvaCompra.setString(4, numerocbte);
            AgregarIvaCompra.setInt(5, idProveedores);
            AgregarIvaCompra.setDouble(6, importeoperacion);
            AgregarIvaCompra.setDouble(7, importenointegraprecioneto);
            AgregarIvaCompra.setDouble(8, importeoperacionesexentas);
            AgregarIvaCompra.setDouble(9, inportevaloragregado);
            AgregarIvaCompra.setDouble(10, importeimpuestosnacionales);
            AgregarIvaCompra.setDouble(11, importeingresosbrutos);
            AgregarIvaCompra.setDouble(12, importeimpuestosmunicipales);
            AgregarIvaCompra.setDouble(13, importeimpuestosinternos);
            AgregarIvaCompra.setDouble(14, otrostributos);
            AgregarIvaCompra.setInt(15, idTipomoneda);
            AgregarIvaCompra.setInt(16, idCodigoOperacion);
            AgregarIvaCompra.setInt(17, 1);
            int n = AgregarIvaCompra.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            cn.commit(); //transaction block end

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (AgregarIvaCompra != null) {
                    AgregarIvaCompra.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void ModificarIvaCompra(int idIvaCompra, String fechacbte, int idTipoCbte, int PuntosdeVentas, String numerocbte, int idProveedores, double importeoperacion, double importenointegraprecioneto, double importeoperacionesexentas, double inportevaloragregado, double importeimpuestosnacionales, double importeingresosbrutos, double importeimpuestosmunicipales, double importeimpuestosinternos, double otrostributos, int idTipomoneda, int idCodigoOperacion) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarIvaCompra = null;

        String sSQL = "UPDATE iva_compra SET"
                + " fechacbte=?, idTipoCbte=?, puntodeventa=?,"
                + " numerocbte=?, idProveedores=?, importeoperacion=?, importenointegraprecioneto=?,"
                + " importeoperacionesexentas=?, inportevaloragregado=?, importeimpuestosnacionales=?,"
                + " importeingresosbrutos=?, importeimpuestosmunicipales=?, importeimpuestosinternos=?,"
                + " otrostributos=?, idTipomoneda=?, idCodigoOperacion=?,"
                + " tipocambio=?"
                + " WHERE idIvaCompra=" + idIvaCompra;

        try {
            cn.setAutoCommit(false); //transaction block start
            ModificarIvaCompra = cn.prepareStatement(sSQL);
            ModificarIvaCompra.setString(1, fechacbte);
            ModificarIvaCompra.setInt(2, idTipoCbte);
            ModificarIvaCompra.setInt(3, PuntosdeVentas);
            ModificarIvaCompra.setString(4, numerocbte);
            ModificarIvaCompra.setInt(5, idProveedores);
            ModificarIvaCompra.setDouble(6, importeoperacion);
            ModificarIvaCompra.setDouble(7, importenointegraprecioneto);
            ModificarIvaCompra.setDouble(8, importeoperacionesexentas);
            ModificarIvaCompra.setDouble(9, inportevaloragregado);
            ModificarIvaCompra.setDouble(10, importeimpuestosnacionales);
            ModificarIvaCompra.setDouble(11, importeingresosbrutos);
            ModificarIvaCompra.setDouble(12, importeimpuestosmunicipales);
            ModificarIvaCompra.setDouble(13, importeimpuestosinternos);
            ModificarIvaCompra.setDouble(14, otrostributos);
            ModificarIvaCompra.setInt(15, idTipomoneda);
            ModificarIvaCompra.setInt(16, idCodigoOperacion);
            ModificarIvaCompra.setInt(17, 1);
            int n = ModificarIvaCompra.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            cn.commit(); //transaction block end

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (ModificarIvaCompra != null) {
                    ModificarIvaCompra.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void AgregarIvaCompraAlicuota(int id_ivacompra, double netogravado, int idTipoIva, double Iva) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarIvaCompraAlicuota = null;
        String sSQL = "INSERT INTO iva_compra_alicuotas (idIvaCompra, importenetogravado, idTipoiva, "
                + "impuestoliquidado) "
                + "VALUES(?,?,?,?)";
        try {
            cn.setAutoCommit(false); //transaction block start
            AgregarIvaCompraAlicuota = cn.prepareStatement(sSQL);
            AgregarIvaCompraAlicuota.setInt(1, id_ivacompra);
            AgregarIvaCompraAlicuota.setDouble(2, netogravado);
            AgregarIvaCompraAlicuota.setInt(3, idTipoIva);
            AgregarIvaCompraAlicuota.setDouble(4, Iva);

            int n = AgregarIvaCompraAlicuota.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            cn.commit(); //transaction block end

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (AgregarIvaCompraAlicuota != null) {
                    AgregarIvaCompraAlicuota.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void ModificarIvaCompraAlicuota(int idIvaCompraAlicuota, double netogravado, int idTipoIva, double Iva) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarIvaCompraAlicuota = null;

        String sSQL = "UPDATE iva_compra_alicuotas SET"
                + " importenetogravado=?, idTipoIva=?, impuestoliquidado=?"
                + " WHERE idIvaCompraAlicuota=" + idIvaCompraAlicuota;

        try {
            cn.setAutoCommit(false); //transaction block start
            ModificarIvaCompraAlicuota = cn.prepareStatement(sSQL);
            ModificarIvaCompraAlicuota.setDouble(1, netogravado);
            ModificarIvaCompraAlicuota.setInt(2, idTipoIva);
            ModificarIvaCompraAlicuota.setDouble(3, Iva);
            int n = ModificarIvaCompraAlicuota.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            cn.commit(); //transaction block end

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (ModificarIvaCompraAlicuota != null) {
                    ModificarIvaCompraAlicuota.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    public String CrearTXT(String finicial, String ffinal){
        String Texto=null;
        
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        
        Statement SelectDatos = null;
        String sqlDatos = "SELECT afip_datos.nombre, afip_tipodoc.descripcion AS tipodoc, "
                + "afip_datos.IvaDocumento, afip_tiporesponsable.descripcion AS tiporesponsable "
                + "FROM afip_datos "
                + "INNER JOIN afip_tipodoc ON afip_datos.idTipodoc = afip_tipodoc.idTipodoc "
                + "INNER JOIN afip_tiporesponsable ON afip_datos.idTiporesponsable = afip_tiporesponsable.idTiporesponsable";
        
        Statement SelectComprobantes = null;
        String sql = "SELECT * FROM vista_iva_compra_txt";
        
        try {
            SelectDatos = cn.createStatement();
            ResultSet rsDatos = SelectDatos.executeQuery(sqlDatos);
            rsDatos.next();
            
            
            //Encabezado
            Texto= rsDatos.getString("nombre")+"\n \n"+ rsDatos.getString("tipodoc")+" : "+rsDatos.getString("IvaDocumento")+"\n"+rsDatos.getString("tiporesponsable")+"\n";
            Texto=Texto+"\n                                                   LIBRO IVA COMPRAS \n\n Periodo Liquidado : "+ finicial + " al "+ ffinal +" \n";
            Texto=Texto+"======================================================================================================================================\n";
            Texto=Texto+"Fecha         Comprobante           Proveedor                    CUIT           Gravado        IVA     No Gravado Perc/Retenc    TOTAL\n";
            Texto=Texto+"======================================================================================================================================\n";
            ///DATOS
            SelectComprobantes = cn.createStatement();
            ResultSet rs = SelectComprobantes.executeQuery(sql);
            Double SumaIVA=0.00;
            while (rs.next()) {
                fnReversa r =new fnReversa();
                if ((r.reverse(rs.getString("fechacbte")).compareTo(ffinal) <= 0) && (r.reverse(rs.getString("fechacbte")).compareTo(finicial) >= 0)) {
                
                    fnCompletar completar=new fnCompletar();
                    Texto=Texto+rs.getString("fechacbte")+" "+String.format("%-10s", rs.getString("tipocbte"))+" "+completar.completarString(rs.getString("puntodeventa"), 4)+"-"+completar.completarString(rs.getString("numerocbte"), 8)+" "+String.format("%-25s", rs.getString("razonsocial"))+" "+String.format("%-11s", rs.getString("documento"))+" "+String.format("%12s", rs.getString("IVA"))+" "+String.format("%12s", rs.getString("Gravado"))+" "+String.format("%20s", rs.getString("No Gravado"))+" "+String.format("%12s", rs.getString("TOTAL"))+"\n";
                    SumaIVA=SumaIVA+rs.getDouble("IVA");
                
                }
                
            }
            
            ////
            Texto=Texto+"--------------------------------------------------------------------------------------------------------------------------------------\n";
            Texto=Texto+"                                                                           ***,***.** "+ String.format("%14s", SumaIVA)+"                        ***,***.**\n";
            Texto=Texto+"--------------------------------------------------------------------------------------------------------------------------------------\n";
            //Encabezado
            Texto=Texto+rsDatos.getString("nombre")+"\n \n"+ rsDatos.getString("tipodoc")+" : "+rsDatos.getString("IvaDocumento")+"\n"+rsDatos.getString("tiporesponsable")+"\n";
            Texto=Texto+"\n                                                   LIBRO IVA COMPRAS \n\n Periodo Liquidado : "+ finicial + " al "+ ffinal +" \n";
            Texto=Texto+"======================================================================================================================================\n";
            Texto=Texto+"Fecha        Comprobante          Proveedor                    CUIT           Gravado        IVA     No Gravado Perc/Retenc     TOTAL\n";
            Texto=Texto+"======================================================================================================================================\n";
            ///RESUMEN
            Texto=Texto+"\n-------------RESUMEN IVA COMPRAS PERIODO 01/03/2018 AL 31/03/2018--------------\n";
            Texto=Texto+"\nCOMPRAS QUE GENERAN CREDITO FISCAL\n";
            Texto=Texto+"COMPRAS GRAVADAS AL 21.00%      ***,***.**  IVA LIQUIDADO "+ String.format("%14s", SumaIVA)+"\n";
            Texto=Texto+"                                -----------               -----------\n";
            Texto=Texto+"TOTALES                         ***,***.**                "+ String.format("%14s", SumaIVA)+"\n";
            ////
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectComprobantes != null) {
                    SelectComprobantes.close();
                }
                if (SelectDatos != null) {
                    SelectDatos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        
        return Texto;
    } 

}
