package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import Controlador.ConexionMySQL;
import Controlador.ServiciosHasar;

public class ClaseHasar {

    public void ReporteZ() throws SQLException {
        String sql2 = "SELECT * FROM hasar_propiedades";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectHasar = null;
        String sql = "SELECT * FROM hasar_configuracion";
        Statement SelectConfig = null;
        
        try {
            SelectHasar = cn.createStatement();
            ResultSet rsHasar = SelectHasar.executeQuery(sql2);
            rsHasar.last();
            
            SelectConfig = cn.createStatement();
            ResultSet rsConfig = SelectConfig.executeQuery(sql);
            rsConfig.next();
            
            ServiciosHasar hasar = new ServiciosHasar();
            if (!"ERROR".equals(hasar.Comenzar(rsHasar.getInt("transporte"), rsHasar.getInt("puerto"), rsHasar.getInt("baudios"), rsHasar.getString("direccionip")))) {
                hasar.ReporteZ();
                //Despues del Z configuro
                boolean cambio;
                boolean leyendas;
                if(rsConfig.getInt("cambio")==0){
                    cambio=true;
                }else{
                    cambio=false;
                }
                
                if(rsConfig.getInt("leyendas")==0){
                    leyendas=true;
                }else{
                    leyendas=false;
                }
                    
                hasar.ConfigurarControladorPorBloque(rsConfig.getDouble("limitecf"), rsConfig.getDouble("limitetf"), rsConfig.getInt("copias"), cambio, leyendas, rsConfig.getInt("corte"));
                
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (SelectHasar != null) {
                SelectHasar.close();
            }
            if (cn != null) {
                cn.close();
            }
        }
    }

    public void ReporteX() throws SQLException {
        String sql2 = "SELECT * FROM hasar_propiedades";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectHasar = null;
        try {
            SelectHasar = cn.createStatement();
            ResultSet rsHasar = SelectHasar.executeQuery(sql2);
            rsHasar.last();
            ServiciosHasar hasar = new ServiciosHasar();
            if (!"ERROR".equals(hasar.Comenzar(rsHasar.getInt("transporte"), rsHasar.getInt("puerto"), rsHasar.getInt("baudios"), rsHasar.getString("direccionip")))) {
                hasar.ReporteX();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            if (SelectHasar != null) {
                SelectHasar.close();
            }
            if (cn != null) {
                cn.close();
            }
        }
    }

    public void Configuracion(Double limitecf, Double limitetf, int copias, boolean cambio, boolean leyendas, int corte) throws SQLException {
        String sql2 = "SELECT * FROM hasar_propiedades";

        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectHasar = null;

        PreparedStatement UpdateConfig = null;
        String sSQL = "UPDATE hasar_configuracion SET"
                + " limitecf=?, limitetf=?, copias=?,"
                + " cambio=?, leyendas=?, corte=?";
        try {
            SelectHasar = cn.createStatement();
            ResultSet rsHasar = SelectHasar.executeQuery(sql2);
            rsHasar.last();
            ServiciosHasar hasar = new ServiciosHasar();
            if (!"ERROR".equals(hasar.Comenzar(rsHasar.getInt("transporte"), rsHasar.getInt("puerto"), rsHasar.getInt("baudios"), rsHasar.getString("direccionip")))) {
                hasar.ConfigurarControladorPorBloque(limitecf, limitetf, copias, cambio, leyendas, corte);

            }
            UpdateConfig = cn.prepareStatement(sSQL);
            
            UpdateConfig.setDouble(1, limitecf);
            UpdateConfig.setDouble(2, limitetf);
            UpdateConfig.setInt(3, copias);
            
            if(cambio==true){
                UpdateConfig.setInt(4, 0);
            }
            else{
                UpdateConfig.setInt(4, 1);
            }
            
            if(leyendas==true){
                UpdateConfig.setInt(5, 0);
            }
            else{
                UpdateConfig.setInt(5, 1);
            }

            UpdateConfig.setInt(6, corte);
            
            int n = UpdateConfig.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se Configuraron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectHasar != null) {
                    SelectHasar.close();
                }
                if (UpdateConfig != null) {
                    UpdateConfig.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}
