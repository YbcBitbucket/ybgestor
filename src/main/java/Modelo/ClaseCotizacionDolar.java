/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import javax.swing.JOptionPane;
import Controlador.ConexionMySQL;
import Controlador.fnCargarFecha;

/**
 *
 * @author Alumno
 */
public class ClaseCotizacionDolar {

    public int Carga_Cotizacion(double cotizacion) {
        int estado = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertDolar = null;
        String sSQL = "INSERT INTO dolar(cotizacion, fecha)"
                + "VALUES(?,?)";
        fnCargarFecha f = new fnCargarFecha();
        try {
            InsertDolar = cn.prepareStatement(sSQL);            
            InsertDolar.setDouble(1, cotizacion);
            InsertDolar.setString(2, f.cargarfechaTipoDate());
            int n = InsertDolar.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                estado = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (InsertDolar != null) {
                    InsertDolar.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        return estado;
    }
}
