package Modelo;

import Controlador.ConexionMySQL;
import static Vista.Login.id_usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseStock {

    public ClaseStock() {
    }

    public int AgregarStock(int cantidad, String fechastock, double pcompra, String pventa, int idProducto, int idDeposito, int idTipoiva, int idProveedor, String nComprobante) {
        int stock=0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarStock = null;
        try {
            String sSQL = "INSERT INTO stock (cantidad, fecha, precioCompra, precioVenta, "
                    + "idProductos, idDepositos, idTipoiva, descripcion, idProveedores, numerocbte) VALUES(?,?,?,?,?,?,?,?,?,?)";
            AgregarStock = cn.prepareStatement(sSQL);
            AgregarStock.setInt(1, cantidad);
            AgregarStock.setString(2, fechastock);
            AgregarStock.setDouble(3, pcompra);
            AgregarStock.setString(4, pventa);
            AgregarStock.setInt(5, idProducto);
            AgregarStock.setInt(6, idDeposito);
            AgregarStock.setInt(7, idTipoiva);
            AgregarStock.setString(8, "COMPRA DE PRODUCTO");
            AgregarStock.setInt(9, idProveedor);
            AgregarStock.setString(10, nComprobante);
            int n = AgregarStock.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            stock = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (AgregarStock != null) {
                    AgregarStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return stock;
    }

    
    
    public void ModificarBonificacion(int idProducto,String pcompra, String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva, int idProveedor) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            PreparedStatement ModificarBonificacion = null;
            
            try {
                String sSQL = "INSERT INTO stock (idProductos, precioCompra, precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion, idProveedores, precioVenta2) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
                ModificarBonificacion = cn.prepareStatement(sSQL);
                ModificarBonificacion.setInt(1, idProducto);
                ModificarBonificacion.setString(2, pcompra);
                ModificarBonificacion.setString(3, pventa);
                ModificarBonificacion.setString(4, bonificacion);
                ModificarBonificacion.setInt(5, 0);
                ModificarBonificacion.setString(6, fechabonificacion);
                ModificarBonificacion.setInt(7, idDeposito);
                ModificarBonificacion.setInt(8, idTipoiva);
                ModificarBonificacion.setString(9, "MODIFICACION DE BONIFICACION");
                ModificarBonificacion.setInt(10, idProveedor);
                int n = ModificarBonificacion.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (ModificarBonificacion != null) {
                        ModificarBonificacion.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    public void ActualizacionPrecio(int idProducto, String pcompra,String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva) {
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            PreparedStatement ActualizacionPrecio = null;
            
            try {
                String sSQL = "INSERT INTO stock (idProductos, precioCompra,precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion) "
                    + "VALUES (?,?,?,?,?,?,?,?,?)";
                ActualizacionPrecio = cn.prepareStatement(sSQL);
                ActualizacionPrecio.setInt(1, idProducto);
                ActualizacionPrecio.setString(2, pcompra);
                ActualizacionPrecio.setString(3, pventa);
                ActualizacionPrecio.setString(4, bonificacion);
                ActualizacionPrecio.setInt(5, 0);
                ActualizacionPrecio.setString(6, fechabonificacion);
                ActualizacionPrecio.setInt(7, idDeposito);
                ActualizacionPrecio.setInt(8, idTipoiva);
                ActualizacionPrecio.setString(9, "ACTUALIZACION PRECIO");
                
                int n = ActualizacionPrecio.executeUpdate();
                if (n > 0) {
                    System.out.println("Los Datos se modificaron exitosamente... " + idProducto);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            } finally {
                try {
                    if (ActualizacionPrecio != null) {
                        ActualizacionPrecio.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    
    public int ActualizacionPrecio(int idProducto, String pcompra, String pventa, String bonificacion, String fechabonificacion, int idDeposito, int idTipoiva, int idprov) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ActualizacionPrecio = null;
        int stock = 0;
        try {
            String sSQL = "INSERT INTO stock (idProductos, precioCompra,precioVenta, bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion,idProveedores) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
            ActualizacionPrecio = cn.prepareStatement(sSQL);
            ActualizacionPrecio.setInt(1, idProducto);
            ActualizacionPrecio.setString(2, pcompra);
            ActualizacionPrecio.setString(3, pventa);
            ActualizacionPrecio.setString(4, bonificacion);
            ActualizacionPrecio.setInt(5, 0);
            ActualizacionPrecio.setString(6, fechabonificacion);
            ActualizacionPrecio.setInt(7, idDeposito);
            ActualizacionPrecio.setInt(8, idTipoiva);
            ActualizacionPrecio.setString(9, "ACTUALIZACION PRECIO");
            ActualizacionPrecio.setInt(10, idprov);
            int n = ActualizacionPrecio.executeUpdate();
            if (n > 0) {
                ///System.out.println("Los Datos se modificaron exitosamente... " + idProducto);
                stock = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            stock = 0;
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ActualizacionPrecio != null) {
                    ActualizacionPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return stock;
    }
    
    public int AgregarPromos(String[][] datos, String codigo, String nombre, String precio, String precioTotal, String descuento, int idTipoiva, String fechaString) {
        int promo = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregarPromo = null;
        try {
            String sSQL = "INSERT INTO promociones (codigo, nombre,precio, preciototal, "
                    + "descuento, idiva, estado,fecha,idusuario) VALUES(?,?,?,?,?,?,?,?,?)";
            AgregarPromo = cn.prepareStatement(sSQL);
            AgregarPromo.setString(1, codigo);
            AgregarPromo.setString(2, nombre);
            AgregarPromo.setString(3, precio);
            AgregarPromo.setString(4, precioTotal);
            AgregarPromo.setString(5, descuento);
            AgregarPromo.setInt(6, idTipoiva);
            AgregarPromo.setInt(7, 1);
            AgregarPromo.setString(8, fechaString);
            AgregarPromo.setInt(9, id_usuario);
            int n = AgregarPromo.executeUpdate();
            if (n > 0) {

                //Busco Ultimo ID Pedidos
                Statement UltimoId = cn.createStatement();
                ResultSet rs = UltimoId.executeQuery("SELECT MAX(idPromociones) AS idPromociones FROM Promociones");
                rs.next();
                int idPromociones = rs.getInt("idPromociones");

                String SQLInsertDetalle = "INSERT INTO detallepromociones (idPromociones, idproductos, cantidad,precioventa) "
                        + "VALUES (?,?,?,?)";
                PreparedStatement InsertDetalle = cn.prepareStatement(SQLInsertDetalle);

                for (int i = 0; i < datos.length; i++) {
                    //Insero Detalle Pedido
                    InsertDetalle.setInt(1, idPromociones);
                    InsertDetalle.setDouble(2, Double.valueOf(datos[i][0]));
                    InsertDetalle.setDouble(3, Double.valueOf(datos[i][3]));
                    InsertDetalle.setDouble(4, Double.valueOf(datos[i][4]));
                    InsertDetalle.executeUpdate();

                }
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
            promo = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (AgregarPromo != null) {
                    AgregarPromo.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return promo;
    }

    public int ModificarPromocion(String codigo, String nombre, String precio, String precioTotal, String descuento, int idTipoiva, String fechaString, int estado,int idpromociones) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarPromos = null;
        int bandera = 0;
        try {//UPDATE pedidos SET estado=? WHERE idPedidos=
            String sSQL = "UPDATE promociones  SET codigo=?, nombre=?, precio=?, precioTotal=?, descuento=?, idiva=?, fecha=?,estado=? where idpromociones="+idpromociones;
            ModificarPromos = cn.prepareStatement(sSQL);
            ModificarPromos.setString(1, codigo);
            ModificarPromos.setString(2, nombre);
            ModificarPromos.setString(3, precio);
            ModificarPromos.setString(4, precioTotal);
            ModificarPromos.setString(5, descuento);
            ModificarPromos.setInt(6, idTipoiva);
            ModificarPromos.setString(7, fechaString);
            ModificarPromos.setInt(8, estado);
            int n = ModificarPromos.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
                bandera = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarPromos != null) {
                    ModificarPromos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        if (bandera == 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
}
