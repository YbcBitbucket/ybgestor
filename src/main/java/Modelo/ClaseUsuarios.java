package Modelo;

import Controlador.ConexionMySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseUsuarios {
    
    public ClaseUsuarios() {
    }
    
    public int AgregarUsuarios(String apellido, String nombre, String usuario, String contraseña, String Datos, String Ventas, String Consultas, String Afip) {
        int usuarios = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuario = null;
        PreparedStatement AgregarUsuario = null;
        //Veo si usuario esta cargado
        String sqlSel = "SELECT usuario FROM usuarios WHERE usuario='" + usuario + "'";
        try {
            SelectUsuario = cn.createStatement();
            ResultSet rsusuario = SelectUsuario.executeQuery(sqlSel);
            if (!rsusuario.next()) {
                cn.setAutoCommit(false); //transaction block start
                //Inserto el producto
                String sSQL = "INSERT INTO usuarios(apellido, nombre, usuario, contraseña"
                        + ", acceso_datos, acceso_ventas, acceso_consultas, acceso_afip) VALUES(?,?,?,?,?,?,?,?)";
                AgregarUsuario = cn.prepareStatement(sSQL);
                AgregarUsuario.setString(1, apellido);
                AgregarUsuario.setString(2, nombre);
                AgregarUsuario.setString(3, usuario);
                AgregarUsuario.setString(4, contraseña);
                AgregarUsuario.setString(5, Datos);
                AgregarUsuario.setString(6, Ventas);
                AgregarUsuario.setString(7, Consultas);
                AgregarUsuario.setString(8, Afip);
                int n = AgregarUsuario.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
                cn.commit(); //transaction block end
                usuarios = 1;
            } else {
                JOptionPane.showMessageDialog(null, "El Usuario ya esta en la base de datos");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (SelectUsuario != null) {
                    SelectUsuario.close();
                }
                if (AgregarUsuario != null) {
                    AgregarUsuario.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return usuarios;
    }
    
    public void ModificarUsuarios(int idUsuarios, String apellido, String nombre, String usuario, String contraseña, String Datos, String Ventas, String Consultas, String Afip) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarUsuario = null;
        try {
            String sSQL = "UPDATE usuarios SET apellido=?, nombre=?, usuario=?, contraseña=?, acceso_datos=?, acceso_ventas=?,"
                    + " acceso_consultas=?, acceso_afip=? WHERE idUsuarios=" + idUsuarios;
            ModificarUsuario = cn.prepareStatement(sSQL);
            ModificarUsuario.setString(1, apellido);
            ModificarUsuario.setString(2, nombre);
            ModificarUsuario.setString(3, usuario);
            ModificarUsuario.setString(4, contraseña);
            ModificarUsuario.setString(5, Datos);
            ModificarUsuario.setString(6, Ventas);
            ModificarUsuario.setString(7, Consultas);
            ModificarUsuario.setString(8, Afip);
            int n = ModificarUsuario.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarUsuario != null) {
                    ModificarUsuario.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
}
