/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnCargarFecha;
import Controlador.fnesNumerico;
import Modelo.ClaseStock;
import java.sql.*;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author LUKS1
 */
public class BonificacionModifica extends javax.swing.JDialog {

      static String Pventa, Pventa2;
    static String Pcompra;
    static int idtipoiva;
    static int idproveedores;
    
    public BonificacionModifica(java.awt.Frame parent, boolean modal) {
       super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardatos();
    }

    
    //////////////////////FUNCION CARGAR DATOS //////////////////////
    void cargardatos() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProductos = null;
        String sSQL = "SELECT productos.codigo, productos.nombre, stock.bonificacion, stock.precioVenta, stock.idTipoiva,stock.precioCompra, stock.idproveedores, stock.precioVenta2 "
                + "FROM stock INNER JOIN productos ON stock.idProductos = productos.idProductos "
                + "WHERE stock.idProductos =" + Bonificaciones.idProductos + " "
                + "AND stock.idDepositos =" + Bonificaciones.idDeposito + " ORDER BY stock.idStock DESC";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sSQL);
            rs.next();
            txtcodigo.setText(rs.getString(1));
            txtnombre.setText(rs.getString(2));
            txtbonificacion.setText(rs.getString(3));
            Pventa=rs.getString(4);            
            idtipoiva=rs.getInt(5);
            Pcompra=rs.getString(6);
            idproveedores=rs.getInt(7);
            Pventa2=rs.getString(8);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu2 = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        txtbonificacion = new RSMaterialComponent.RSTextFieldMaterial();
        txtcodigo = new RSMaterialComponent.RSTextFieldMaterial();
        txtnombre = new RSMaterialComponent.RSTextFieldMaterial();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu2.setEditable(false);
        lblMenu2.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu2.setText("Bonificar Producto");
        lblMenu2.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu2.setOpaque(false);
        lblMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenu2ActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu2, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 320, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(322, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 20));

        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 250, 141, -1));

        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 141, -1));

        txtbonificacion.setPlaceholder("Bonificación  %");
        txtbonificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbonificacionActionPerformed(evt);
            }
        });
        txtbonificacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbonificacionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbonificacionKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtbonificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 300, -1));

        txtcodigo.setPlaceholder("Codigo");
        txtcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodigoActionPerformed(evt);
            }
        });
        txtcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcodigoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcodigoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 300, -1));

        txtnombre.setPlaceholder("Nombre");
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 300, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenu2ActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed

    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
 if (txtbonificacion.getText().equals("")) {
            ///Ingresar todos los datos
            JOptionPane.showMessageDialog(null, "No ingreso todos los datos de la bonificacion");
        } else {
            fnCargarFecha fecha = new fnCargarFecha();
            String fechastock = fecha.cargarfecha();
            
            ClaseStock bon = new ClaseStock();
            bon.ModificarBonificacion(Bonificaciones.idProductos, Pcompra, Pventa, txtbonificacion.getText(), fechastock, Bonificaciones.idDeposito, idtipoiva, idproveedores);
        }       
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void txtbonificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbonificacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbonificacionActionPerformed

    private void txtbonificacionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbonificacionKeyPressed
       
    }//GEN-LAST:event_txtbonificacionKeyPressed

    private void txtbonificacionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbonificacionKeyReleased
        fnesNumerico num=new fnesNumerico();
        if (!num.isNumeric(txtbonificacion.getText())) {
            txtbonificacion.setText("");
        }
    }//GEN-LAST:event_txtbonificacionKeyReleased

    private void txtcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoActionPerformed

    private void txtcodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoKeyPressed

    private void txtcodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyPressed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyReleased

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private rojeru_san.rsfield.RSTextField lblMenu2;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSTextFieldMaterial txtbonificacion;
    private RSMaterialComponent.RSTextFieldMaterial txtcodigo;
    private RSMaterialComponent.RSTextFieldMaterial txtnombre;
    // End of variables declaration//GEN-END:variables
}
