/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.cboDeposito;
import Controlador.cboFormaCheque;
import Controlador.cboFormaCtaCte;
import Controlador.cboFormasdepago;
import Controlador.cboTarjetaCredito;
import Controlador.cboTarjetaDebito;
import Controlador.fnAlinear;
import Controlador.fnCargarFecha;
import Controlador.fnEditarCeldas;
import Controlador.fnRedondear;
import Modelo.ClaseCaja;
import Modelo.ClasePedidos;
import static Vista.Descripcion_Pedido.descripcion;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Ybc
 */
public class Main extends javax.swing.JFrame {

    DefaultTableModel ModeloProductos;
    public static int ConvertirPresupuesto = 0, AgregarPresupuesto = 0, pesable = 0;
    public static double bcant = 0;
    public static String total, subtotal;
    public static String nombreCliente, ResposabilidadCliente, TipodocCliente, DocCliente;
    public static int idpedido = 0, IdDeposito = 0, IdCliente = 0, idPtodeVenta = 0, idPresupuesto = 0;
    static double[] descuentobase = new double[12];
    static double[] descuentoporcentaje = new double[12];
    static double[] interesporcentaje = new double[12];
    static double[] interesbase = new double[12];
    static int[] formas_detalle = new int[100];
    cboFormasdepago[] forma = new cboFormasdepago[7];
    static int indexformapago = 0;
    DefaultComboBoxModel modelo_combo_forma = new DefaultComboBoxModel();
    int bandera_combo = 0;

    public Main() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        //////////////////////////////////////////////////////////////////////
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        ////////////////////////////////////////////////////////////////////
        AccesoUsuarios();
        ClaseCaja caja = new ClaseCaja();
        caja.iniciocaja();
        cargarconsumidorfinal();
        //// cargarnumero();
        cargarformadepago();

        /*cargartarjetacredito();
        cargartarjetadebito();
        cargarformactacte();
        cargarformacheque();*/
        cargardepositos();
        cargartablabuscar("");

//txtinteres.setText("0.0");
        cargatotales();

        //dobleclickagregaproducto();
        tamañotablapedido();
        txtbuscarpedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Character c = evt.getKeyChar();
                if (Character.isLetter(c)) {
                    evt.setKeyChar(Character.toUpperCase(c));
                }
            }
        });
        txtbuscarpedidos.requestFocus();
    }

    ///// CARGAR TABLA BUSCAR /////
    void cargartablabuscar(String valor) {
        //                  0       1        2               3         4      5        6       7       8           9
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal", "Descripcion", "pesable"};
        Object[] Registros = new Object[11];
        String sql = "SELECT * FROM vista_productos_busqueda WHERE idDepositos=" + IdDeposito + "";
        ModeloProductos = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            cursor();
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString("precioVenta");
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);

                ////////////////////////
                Registros[7] = rs.getString("stock");
                Registros[8] = rs.getString("precioVenta");
                Registros[9] = rs.getString("descripcion");
                Registros[10] = rs.getInt("pesable");
                ModeloProductos.addRow(Registros);

            }

            tablabuscar.setModel(ModeloProductos);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(3).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(4).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(0);
            //////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(5).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(0);
            ////////////////////////////////////////////////////////////////////////
            tablabuscar.getColumnModel().getColumn(6).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(0);

            tablabuscar.getColumnModel().getColumn(9).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(9).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(9).setPreferredWidth(0);

            tablabuscar.getColumnModel().getColumn(10).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(10).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(10).setPreferredWidth(0);

            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(20);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(30);
            // tablabuscar.getColumnModel().getColumn(8).setCellRenderer(new fnEditarCeldas());
            cursor2();
        } catch (SQLException ex) {
            cursor2();
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
                cursor2();
            } catch (Exception ex) {
                cursor2();
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cursor() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.pack();
    }

    void cursor2() {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.pack();
    }

    void AccesoUsuarios() {
        /* //Datos
        char[] acceso_datos = Login.acceso_datos.toCharArray();
        if (acceso_datos[0] == '0') {
            mncategoria.setVisible(false);
        }
        if (acceso_datos[1] == '0') {
            mnmarcas.setVisible(false);
        }
        if (acceso_datos[2] == '0') {
            mnproveedor.setVisible(false);
        }
        if (acceso_datos[3] == '0') {
            mnproducto.setVisible(false);
        }
        if (acceso_datos[4] == '0') {
            mnstock.setVisible(false);
        }
        if (acceso_datos[5] == '0') {
            mnclientes.setVisible(false);
        }
        if (acceso_datos[6] == '0') {
            mnivacompra.setVisible(false);
        }
        if (acceso_datos[7] == '0') {
            mnegresos.setVisible(false);
        }
        if (acceso_datos[8] == '0') {
            mnusuarios.setVisible(false);
        }
        //Ventas
        char[] acceso_ventas = Login.acceso_ventas.toCharArray();
        if (acceso_ventas[0] == '0') {
            mnpedidos.setVisible(false);
        }
        if (acceso_ventas[1] == '0') {
            mnpresupuestos.setVisible(false);
        }
        if (acceso_ventas[2] == '0') {
            mnbonificacion.setVisible(false);
        }
        if (acceso_ventas[3] == '0') {
            mnformasdepago.setVisible(false);
        }
        if (acceso_ventas[4] == '0') {
            mnctacte.setVisible(false);
        }
        if (acceso_ventas[5] == '0') {
            mncaja.setVisible(false);
        }
        //Consultas
        char[] acceso_consultas = Login.acceso_consultas.toCharArray();
        if (acceso_consultas[0] == '0') {
            RankingClientes.setVisible(false);
        }
        if (acceso_consultas[1] == '0') {
            RankingProductos.setVisible(false);
        }
        if (acceso_consultas[2] == '0') {
            OrdenesdeCompra.setVisible(false);
        }
        //Afip
        char[] acceso_afip = Login.acceso_afip.toCharArray();
        if (acceso_afip[0] == '0') {
            mntipofacturacion.setVisible(false);
        }

        /////////Tipo Facturacion
        String sql = "SELECT descripcion FROM afip_tipo_facturacion WHERE tipo=" + 1;
        String tipoFacturacion = "";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement Select = null;
        try {
            Select = cn.createStatement();
            ResultSet rs = Select.executeQuery(sql);
            while (rs.next()) {
                tipoFacturacion = rs.getString(1);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (Select != null) {
                    Select.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

        if (tipoFacturacion.equals("WSFE")) {
            jMenuWebServices.setVisible(true);
        }
         */
    }

    void cargarformadepago() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboformadepago.setModel(value);
        Statement SelectTarjetaDebito = null;
        String sSQL = "SELECT * FROM formasdepago";
        try {
            SelectTarjetaDebito = cn.createStatement();
            ResultSet rs = SelectTarjetaDebito.executeQuery(sSQL);
            int i = 0;
            while (rs.next()) {
                System.out.println("idFormasDePago " + rs.getInt("idFormasDePago") + " nombre " + rs.getString("nombre") + " descuento " + rs.getDouble("descuento") + " signo " + rs.getString("signo"));
                forma[i] = new cboFormasdepago(rs.getInt("idFormasDePago"), rs.getString("nombre"), rs.getDouble("descuento"), rs.getString("signo"));
                value.addElement(new cboFormasdepago(rs.getInt("idFormasDePago"), rs.getString("nombre"), rs.getDouble("descuento"), rs.getString("signo")));
                i++;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaDebito != null) {
                    SelectTarjetaDebito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }

    ///// CARGAR CBO TarjetaCredito /////
    void cargartarjetacredito() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboforma.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTarjetaCredito = null;
        String sSQL = "SELECT * FROM tarjetacredito";
        try {
            SelectTarjetaCredito = cn.createStatement();
            ResultSet rs = SelectTarjetaCredito.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTarjetaCredito(rs.getInt("idTarjetaCredito"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
            System.out.println("Vista.Main.cargartarjetacredito()");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaCredito != null) {
                    SelectTarjetaCredito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void borrarcboforma() {
        bandera_combo = 1;
        cboforma.setModel(modelo_combo_forma);
        modelo_combo_forma.removeAllElements();
        System.out.println("cboforma cant " + cboforma.getItemCount());
        bandera_combo = 0;
    }

    ////////////////////////////////////////
    ///// CARGAR CBO CtaCte /////
    void cargarformactacte() {
        cboforma.setModel(modelo_combo_forma);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectFormaPago_CtaCte = null;
        String sSQL = "SELECT * FROM formasdepago_ctacte";
        try {
            SelectFormaPago_CtaCte = cn.createStatement();
            ResultSet rs = SelectFormaPago_CtaCte.executeQuery(sSQL);
            while (rs.next()) {
                modelo_combo_forma.addElement(new cboFormaCtaCte(rs.getInt("idFormaPago_CtaCte"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
            System.out.println("Vista.Main.cargarformactacte()");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFormaPago_CtaCte != null) {
                    SelectFormaPago_CtaCte.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// CARGAR CBO Cheque /////
    void cargarformacheque() {
        cboforma.setModel(modelo_combo_forma);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectFormaPago_Cheque = null;
        String sSQL = "SELECT * FROM formasdepago_cheque";
        try {
            SelectFormaPago_Cheque = cn.createStatement();
            ResultSet rs = SelectFormaPago_Cheque.executeQuery(sSQL);
            while (rs.next()) {
                modelo_combo_forma.addElement(new cboFormaCheque(rs.getInt("idFormaPago_Cheque"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
            System.out.println("Vista.Main.cargarformacheque()");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectFormaPago_Cheque != null) {
                    SelectFormaPago_Cheque.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ////////////////////////////////////////
    ///// CARGAR CBO TarjetaDebito /////
    void cargartarjetadebito() {
        cboforma.setModel(modelo_combo_forma);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTarjetaDebito = null;
        String sSQL = "SELECT * FROM tarjetadebito";
        try {
            SelectTarjetaDebito = cn.createStatement();
            ResultSet rs = SelectTarjetaDebito.executeQuery(sSQL);
            while (rs.next()) {
                modelo_combo_forma.addElement(new cboTarjetaDebito(rs.getInt("idTarjetaDebito"), rs.getString("descripcion"), rs.getDouble("descuento")));
            }
            System.out.println("Vista.Main.cargartarjetadebito()");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTarjetaDebito != null) {
                    SelectTarjetaDebito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    void cargarconsumidorfinal() {
        String sql = "SELECT * FROM vista_clientes WHERE idClientes=" + 1;
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectCliente = null;
        try {
            SelectCliente = cn.createStatement();
            ResultSet rs = SelectCliente.executeQuery(sql);
            while (rs.next()) {
                IdCliente = rs.getInt(1);
                nombreCliente = rs.getString(2);
                ResposabilidadCliente = rs.getString(3);
                TipodocCliente = rs.getString(4);
                DocCliente = rs.getString(5);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCliente != null) {
                    SelectCliente.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        txtnombreCliente.setText(nombreCliente);
        txtresposabilidadCliente.setText(ResposabilidadCliente);
        txttipodocCliente.setText(TipodocCliente);
        txtdocCliente.setText(DocCliente);
    }

    ///// CARGAR CBO DEPOSITOS /////
    void cargardepositos() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        IdDeposito = 1;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDepositos = null;
        String sSQL = "SELECT idDepositos, nombre FROM deposito";
        try {
            SelectDepositos = cn.createStatement();
            ResultSet rs = SelectDepositos.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboDeposito(rs.getInt("idDepositos"), rs.getString("nombre")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDepositos != null) {
                    SelectDepositos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargatotales() {
        //Suma los iportes del pedido 
        double total = 0.00, sumatoria = 0.0;
        int totalRow = tablapedidos.getRowCount();
        totalRow -= 1;
        descuentoporcentaje[0] = 0;
        interesporcentaje[0] = 0;
        descuentobase[0] = 0;
        interesbase[0] = 0;
        fnRedondear redondear = new fnRedondear();
        String signo = "";

        signo = forma[indexformapago].getSignoFormasdepago();
        System.out.println("signo " + signo + " indexformapago " + indexformapago);

        //Ninguna de los anteriores
        if (indexformapago == 0) {
            // System.out.println("indexformapago == 0");
            descuentoporcentaje[0] = forma[indexformapago].getdescuentoFormasdepago();
            //System.out.println("descuento " + descuentoporcentaje[0]);

            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            formas_detalle[0] = 0;

        }

        //Forma CtaCte
        if (indexformapago == 1) {
            //System.out.println("indexformapago == 1");
            if (signo.equals("NEGATIVO")) {

                cboFormaCtaCte formactacte = (cboFormaCtaCte) cboforma.getSelectedItem();
                formas_detalle[0] = formactacte.getidFormaPago_CtaCte();
                descuentoporcentaje[0] = formactacte.getdescuentoFormaPago_CtaCte();
                //System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboFormaCtaCte formactacte = (cboFormaCtaCte) cboforma.getSelectedItem();
                formas_detalle[0] = formactacte.getidFormaPago_CtaCte();
                // System.out.println("fornas_detalle[0] " + fornas_detalle[0]);
                interesporcentaje[0] = formactacte.getdescuentoFormaPago_CtaCte();
                // System.out.println("interesporcentaje[0] " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }

        }

        //Tarjeta Credito
        if (indexformapago == 2) {
            //System.out.println("indexformapago == 2");
            if (signo.equals("NEGATIVO")) {
                cboTarjetaCredito tarjetacredito = (cboTarjetaCredito) cboforma.getSelectedItem();
                formas_detalle[0] = tarjetacredito.getidTarjetaCredito();
                descuentoporcentaje[0] = tarjetacredito.getdescuentoTarjetaCredito();
                //System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboTarjetaCredito tarjetacredito = (cboTarjetaCredito) cboforma.getSelectedItem();
                formas_detalle[0] = tarjetacredito.getidTarjetaCredito();
                //System.out.println("fornas_detalle[0] " + fornas_detalle[0]);
                interesporcentaje[0] = tarjetacredito.getdescuentoTarjetaCredito();
                //System.out.println("interesporcentaje[0] " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }
        }

        //Tarjeta de Debito
        if (indexformapago == 3) {
            //System.out.println("indexformapago == 3");
            if (signo.equals("NEGATIVO")) {
                cboTarjetaDebito tarjetadebito = (cboTarjetaDebito) cboforma.getSelectedItem();
                formas_detalle[0] = tarjetadebito.getidTarjetaDebito();
                descuentoporcentaje[0] = tarjetadebito.getdescuentoTarjetaDebito();
                // System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboTarjetaDebito tarjetadebito = (cboTarjetaDebito) cboforma.getSelectedItem();
                formas_detalle[0] = tarjetadebito.getidTarjetaDebito();
                interesporcentaje[0] = tarjetadebito.getdescuentoTarjetaDebito();
                //System.out.println("interesporcentaje " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }

        }

        //Cheque
        if (indexformapago == 4) {
            // System.out.println("indexformapago == 4");
            if (signo.equals("NEGATIVO")) {
                cboFormaCheque cheque = (cboFormaCheque) cboforma.getSelectedItem();
                formas_detalle[0] = cheque.getidFormaPago_Cheque();
                descuentoporcentaje[0] = cheque.getdescuentoFormaPago_Cheque();
                //  System.out.println("descuento " + descuentoporcentaje[0]);

                ///Descuento base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            } else {
                cboFormaCheque cheque = (cboFormaCheque) cboforma.getSelectedItem();
                formas_detalle[0] = cheque.getidFormaPago_Cheque();
                interesporcentaje[0] = cheque.getdescuentoFormaPago_Cheque();
                //  System.out.println("interesporcentaje " + interesporcentaje[0]);
                ///interes base
                double totalbase = 0.00, sumatoriabase = 0.0;
                for (int i = 0; i <= (totalRow); i++) {
                    sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                    totalbase = totalbase + sumatoriabase;
                }
                interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            }
        }

        //interes manual
        if (indexformapago == 5) {
            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            //descuentobase[0] = redondear.dosDigitos(totalbase * (descuentoporcentaje[0] / 100));
            descuentobase[0] = Double.valueOf(txtdescuento.getText());
            formas_detalle[0] = 0;

        }
        if (indexformapago == 6) {
            interesporcentaje[0] = forma[indexformapago].getdescuentoFormasdepago();

            ///Descuento base
            double totalbase = 0.00, sumatoriabase = 0.0;
            for (int i = 0; i <= (totalRow); i++) {
                sumatoriabase = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 3).toString()) * (1 - (Double.valueOf(tablapedidos.getValueAt(i, 4).toString()) / 100))) * Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                totalbase = totalbase + sumatoriabase;
            }
            interesbase[0] = redondear.dosDigitos(totalbase * (interesporcentaje[0] / 100));
            formas_detalle[0] = 0;

        }
        //Recorrer para el descuento
        for (int i = 0; i <= (totalRow); i++) {
            double precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
            double cant = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
            double bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
            double iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
            double desc = descuentoporcentaje[0];
            double interes = interesporcentaje[0];
            tablapedidos.setValueAt(desc, i, 8);
            tablapedidos.setValueAt(interes, i, 10);
            //tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 - (desc / 100)) * (1 + interes / 100) * cant)), i, 9);
            tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * cant)), i, 9);

        }

        for (int i = 0; i <= (totalRow); i++) {
            sumatoria = redondear.dosDigitos(Double.valueOf(tablapedidos.getValueAt(i, 9).toString()));
            total = total + sumatoria;
        }

        txtsubtotal.setText(String.valueOf(redondear.dosDigitos(total)));
        txtdescuentoPorcentaje.setText(String.valueOf(descuentoporcentaje[0]));
        txtinteresPorcentaje.setText(String.valueOf(interesporcentaje[0]));
        txtinteres.setText(String.valueOf(interesbase[0]));
        txtdescuento.setText(String.valueOf(descuentobase[0]));
        txttotal.setText(String.valueOf(redondear.dosDigitos(total) + interesbase[0] - descuentobase[0]));

    }

    void tamañotablapedido() {
        tablapedidos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(3).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(3).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(3).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(5).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(6).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(6).setPreferredWidth(0);

        tablapedidos.getColumnModel().getColumn(1).setPreferredWidth(70);
        tablapedidos.getColumnModel().getColumn(2).setPreferredWidth(170);
        tablapedidos.getColumnModel().getColumn(4).setPreferredWidth(30);
        tablapedidos.getColumnModel().getColumn(7).setPreferredWidth(40);

        tablapedidos.getColumnModel().getColumn(9).setPreferredWidth(100);
        //tablapedidos.getColumnModel().getColumn(9).setCellRenderer(new fnEditarCeldas());

        tablapedidos.getColumnModel().getColumn(8).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(8).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(8).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(10).setPreferredWidth(0);
        tablapedidos.getColumnModel().getColumn(11).setMinWidth(0);
        tablapedidos.getColumnModel().getColumn(11).setMaxWidth(0);
        tablapedidos.getColumnModel().getColumn(11).setPreferredWidth(0);
    }
    ////////////////////////////////////////

    ///// FUNCION BORRAR TABLA /////
    void borrartablapedidos() {
        DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
        int a = temp.getRowCount() - 1;  //Índices van de 0 a n-1
        for (int i = a; i >= 0; i--) {
            temp.removeRow(i);
        }
    }

    void agrega_producto() {

        //Cantidad.bc = 1;
        bcant = 1;
        int bandera = 0;
        DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
        if (tablabuscar.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
        } else {

            double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());

            if (bcant != 0) {

                if (cbopromos.getSelectedItem().toString().equals("Productos")) {
                    pesable = Integer.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());
                    double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                    double cant = Double.valueOf(bcant);
                    double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                    double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                    fnRedondear redondear = new fnRedondear();
                    Object nuevo[] = {
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                        bcant,
                        "",
                        redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant),
                        "",
                        tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString()
                    };
                    /////Sacao el codigo de la tabla buscar
                    String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                    /////Recorro la tabla pedidos
                    int filaspedidos = tablapedidos.getRowCount();
                    int i = 0;
                    if (filaspedidos != 0) {
                        while (filaspedidos > i) {
                            /////Sacao el codigo tabla pedido
                            String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                            if (codigobuscar.equals(codigopedido)) {
                                bandera = 1;
                                break;
                            }
                            i++;
                        }
                        if (bandera == 0) {
                            temp.addRow(nuevo);
                            /////Resto la cantidad al stock de la tabla buscar
                            System.out.println("Formularios.Main.agrega_producto()");
                            tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                        } else {
                            /////Recooro la tabla pedidos
                            i = 0;
                            while (filaspedidos > i) {

                                String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                if (codigobuscar.equals(codigopedido2)) {
                                    double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                    precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                    bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                    iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                    tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                    tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                    System.out.println("Formularios.Main.agrega_producto()");
                                    tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                    break;
                                }
                                i++;
                            }
                        }
                    } else {
                        temp.addRow(nuevo);
                        System.out.println("Formularios.Main.agrega_producto()");
                        /////Resto la cantidad al stock de la tabla buscar
                        tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                    }
                    cargatotales();
                    /////////////////////////////////////////////////////////////
                    tamañotablapedido();
                    /////////////////////////////////////////////////////////////
                    txtbuscarpedidos.setText("");
                    txtbuscarpedidos.requestFocus();

                } else {
                    bcant = 1;
                    int id_promociones = Integer.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 0).toString());
                    System.out.println("id_promociones:" + id_promociones);
                    String sql = "SELECT * FROM vista_promociones_detalle WHERE estado=1 and idpromociones=" + id_promociones;

                    ConexionMySQL cc = new ConexionMySQL();
                    Connection cn = cc.Conectar();
                    Statement SelectVProducto = null;
                    Statement SelectStock = null;
                    try {
                        SelectVProducto = cn.createStatement();
                        ResultSet rs = SelectVProducto.executeQuery(sql);
                        while (rs.next()) {
                            Object nuevo[] = {
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8),
                                rs.getString(9),
                                rs.getString(10),
                                rs.getString(13),
                                rs.getString(14)};

                            System.out.println("sale.... " + bcant);
                            /////Sacao el codigo de la tabla buscarcvbcv
                            String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                            /////Recorro la tabla pedidos
                            int filaspedidos = tablapedidos.getRowCount();
                            int i = 0;
                            if (filaspedidos != 0) {
                                while (filaspedidos > i) {
                                    /////Sacao el codigo tabla pedido
                                    String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                    if (codigobuscar.equals(codigopedido)) {
                                        bandera = 1;
                                        break;
                                    }
                                    i++;
                                }
                                if (bandera == 0) {
                                    temp.addRow(nuevo);
                                    /////Resto la cantidad al stock de la tabla buscar                                            
                                } else {
                                    /////Recooro la tabla pedidos
                                    i = 0;
                                    fnRedondear redondear = new fnRedondear();
                                    while (filaspedidos > i) {

                                        String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                        /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                        if (codigobuscar.equals(codigopedido2)) {
                                            double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                            double precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                            double bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                            double iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                            tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                            tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 + (iva / 100))) * (cantidad + bcant)), i, 8);
                                            break;
                                        }
                                        i++;
                                    }
                                }
                            } else {
                                temp.addRow(nuevo);
                            }
                        }

                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        JOptionPane.showMessageDialog(null, ex);
                    } finally {
                        try {
                            if (SelectVProducto != null) {
                                SelectVProducto.close();
                            }
                            if (SelectStock != null) {
                                SelectStock.close();
                            }
                            if (cn != null) {
                                cn.close();
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                    /// System.out.println("tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString() " + tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());

                    cargatotales();
                    /////////////////////////////////////////////////////////////
                    tamañotablapedido();
                    /////////////////////////////////////////////////////////////
                    txtbuscarpedidos.setText("");
                    txtbuscarpedidos.requestFocus();

                }

            } else {
                JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
            }

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelPrincipal = new necesario.Panel();
        MenuVertical = new necesario.Panel();
        btnproductos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnclientes = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnpedidos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnconsultas = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnproveedores = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnpagos = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        jLabel1 = new javax.swing.JLabel();
        btncaja = new RSMaterialComponent.RSButtonMaterialIconOne();
        PanelMenu = new necesario.Panel();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        btncerrar = new RSMaterialComponent.RSButtonIconUno();
        PanelContenedor = new necesario.Panel();
        PanelPedidos = new necesario.Panel();
        btncliente = new RSMaterialComponent.RSButtonMaterialIconUno();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablapedidos = new rojerusan.RSTableMetro1();
        txtbuscarpedidos = new RSMaterialComponent.RSTextFieldMaterial();
        rSLabelFecha1 = new rojeru_san.rsdate.RSLabelFecha();
        rSLabelHora1 = new rojeru_san.rsdate.RSLabelHora();
        txtnombreCliente = new RSMaterialComponent.RSTextFieldMaterial();
        txtresposabilidadCliente = new RSMaterialComponent.RSTextFieldMaterial();
        txttipodocCliente = new RSMaterialComponent.RSTextFieldMaterial();
        txtdocCliente = new RSMaterialComponent.RSTextFieldMaterial();
        btnaceptar = new RSMaterialComponent.RSButtonMaterialIconUno();
        btncancelar = new RSMaterialComponent.RSButtonMaterialIconUno();
        panel1 = new necesario.Panel();
        txttotal = new rojeru_san.rsfield.RSTextField();
        lblTotal = new rojeru_san.rsfield.RSTextField();
        txtinteres = new rojeru_san.rsfield.RSTextField();
        lblInteres = new rojeru_san.rsfield.RSTextField();
        txtdescuento = new rojeru_san.rsfield.RSTextField();
        txtsubtotal = new rojeru_san.rsfield.RSTextField();
        lblSubtotal = new rojeru_san.rsfield.RSTextField();
        txtdescuentoPorcentaje = new rojeru_san.rsfield.RSTextField();
        cboforma = new RSMaterialComponent.RSComboBoxMaterial();
        cboformadepago = new RSMaterialComponent.RSComboBoxMaterial();
        lblDescuento1 = new rojeru_san.rsfield.RSTextField();
        txtinteresPorcentaje = new rojeru_san.rsfield.RSTextField();
        cbopromos = new RSMaterialComponent.RSComboBoxMaterial();
        PanelProductos = new necesario.Panel();
        btnproductosadministra = new RSMaterialComponent.RSButtonMaterialIconUno();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablabuscar = new rojerusan.RSTableMetro1();
        txtbuscarproductos = new RSMaterialComponent.RSTextFieldMaterial();
        btnstockadministra = new RSMaterialComponent.RSButtonMaterialIconUno();
        cbopromos1 = new RSMaterialComponent.RSComboBoxMaterial();
        cbopromos2 = new RSMaterialComponent.RSComboBoxMaterial();
        cbopromos3 = new RSMaterialComponent.RSComboBoxMaterial();
        btnstockadministra1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnstockadministra2 = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnstockadministra3 = new RSMaterialComponent.RSButtonMaterialIconUno();
        PanelBlanco = new necesario.Panel();
        jLabel2 = new javax.swing.JLabel();
        PanelConsultas = new necesario.Panel();
        jLabel4 = new javax.swing.JLabel();
        PanelPagos = new necesario.Panel();
        jLabel5 = new javax.swing.JLabel();
        PanelUtilitarios = new necesario.Panel();
        panel2 = new necesario.Panel();
        rSButtonMaterialIconUno1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        rSButtonMaterialIconUno2 = new RSMaterialComponent.RSButtonMaterialIconUno();
        rSButtonMaterialIconUno3 = new RSMaterialComponent.RSButtonMaterialIconUno();
        PanelCaja = new necesario.Panel();
        jLabel7 = new javax.swing.JLabel();
        PanelClientes = new necesario.Panel();
        btnproductosadministra1 = new RSMaterialComponent.RSButtonMaterialIconUno();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablabuscar1 = new rojerusan.RSTableMetro1();
        txtbuscarproductos1 = new RSMaterialComponent.RSTextFieldMaterial();
        btnstockadministra4 = new RSMaterialComponent.RSButtonMaterialIconUno();
        cbopromos4 = new RSMaterialComponent.RSComboBoxMaterial();
        cbopromos5 = new RSMaterialComponent.RSComboBoxMaterial();
        cbopromos6 = new RSMaterialComponent.RSComboBoxMaterial();
        btnstockadministra5 = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnstockadministra6 = new RSMaterialComponent.RSButtonMaterialIconUno();
        btnstockadministra7 = new RSMaterialComponent.RSButtonMaterialIconUno();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PanelPrincipal.setColorBackground(new java.awt.Color(255, 255, 255));
        PanelPrincipal.setPreferredSize(new java.awt.Dimension(1300, 650));
        PanelPrincipal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnproductos.setBackground(new java.awt.Color(102, 102, 102));
        btnproductos.setText("Produtos");
        btnproductos.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnproductos.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnproductos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnproductos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.TV);
        btnproductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnproductosActionPerformed(evt);
            }
        });
        btnproductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnproductosKeyPressed(evt);
            }
        });

        btnclientes.setBackground(new java.awt.Color(102, 102, 102));
        btnclientes.setText("Clientes");
        btnclientes.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnclientes.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnclientes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnclientes.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_CIRCLE);
        btnclientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclientesActionPerformed(evt);
            }
        });
        btnclientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnclientesKeyPressed(evt);
            }
        });

        btnpedidos.setBackground(new java.awt.Color(102, 102, 102));
        btnpedidos.setText("Pedidos");
        btnpedidos.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnpedidos.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnpedidos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnpedidos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SHOPPING_CART);
        btnpedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpedidosActionPerformed(evt);
            }
        });
        btnpedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnpedidosKeyPressed(evt);
            }
        });

        btnconsultas.setBackground(new java.awt.Color(102, 102, 102));
        btnconsultas.setText("Consultas");
        btnconsultas.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnconsultas.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnconsultas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnconsultas.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LOCAL_ATM);
        btnconsultas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnconsultasActionPerformed(evt);
            }
        });
        btnconsultas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnconsultasKeyPressed(evt);
            }
        });

        btnproveedores.setBackground(new java.awt.Color(102, 102, 102));
        btnproveedores.setText("Utilitarios");
        btnproveedores.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnproveedores.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnproveedores.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnproveedores.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.POLL);
        btnproveedores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnproveedoresActionPerformed(evt);
            }
        });
        btnproveedores.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnproveedoresKeyPressed(evt);
            }
        });

        btnpagos.setBackground(new java.awt.Color(102, 102, 102));
        btnpagos.setText("Pagos");
        btnpagos.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnpagos.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnpagos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnpagos.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.MONETIZATION_ON);
        btnpagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpagosActionPerformed(evt);
            }
        });
        btnpagos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnpagosKeyPressed(evt);
            }
        });

        btnsalir.setBackground(new java.awt.Color(102, 102, 102));
        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXIT_TO_APP);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Logos/ybc blanco.png"))); // NOI18N

        btncaja.setBackground(new java.awt.Color(102, 102, 102));
        btncaja.setText("Caja");
        btncaja.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btncaja.setEffectButton(RSMaterialComponent.RSButtonMaterialIconOne.EFFECTBUTTON.FLAT);
        btncaja.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncaja.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.POLL);
        btncaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncajaActionPerformed(evt);
            }
        });
        btncaja.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btncajaKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout MenuVerticalLayout = new javax.swing.GroupLayout(MenuVertical);
        MenuVertical.setLayout(MenuVerticalLayout);
        MenuVerticalLayout.setHorizontalGroup(
            MenuVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuVerticalLayout.createSequentialGroup()
                .addGroup(MenuVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(MenuVerticalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, MenuVerticalLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(MenuVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnproductos, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(MenuVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnclientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                                .addComponent(btnconsultas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btnproveedores, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btnpagos, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btnpedidos, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(btncaja, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        MenuVerticalLayout.setVerticalGroup(
            MenuVerticalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuVerticalLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnpedidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnproductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnclientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnconsultas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnpagos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnproveedores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btncaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        PanelPrincipal.add(MenuVertical, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 260, 630));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Menu Principal");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });

        btncerrar.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addComponent(lblMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 1274, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        PanelPrincipal.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1300, 20));

        PanelContenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        PanelContenedor.setLayout(new javax.swing.OverlayLayout(PanelContenedor));

        PanelPedidos.setColorBackground(new java.awt.Color(255, 255, 255));

        btncliente.setText("Buscar Cliente");
        btncliente.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btncliente.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_CIRCLE);
        btncliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclienteActionPerformed(evt);
            }
        });

        tablapedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id_Producto", "Codigo", "Nombre", "Precio Venta", "Bonificación", "idiva", "IVA", "Cantidad", "Desc", "Total", "Interes", "Pesable"
            }
        ));
        tablapedidos.setFontRowHover(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jScrollPane1.setViewportView(tablapedidos);

        txtbuscarpedidos.setPlaceholder("Buscar Productos");
        txtbuscarpedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarpedidosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarpedidosKeyReleased(evt);
            }
        });

        rSLabelFecha1.setForeground(new java.awt.Color(0, 0, 0));

        rSLabelHora1.setForeground(new java.awt.Color(0, 0, 0));

        txtnombreCliente.setPlaceholder("Cliente");

        txtresposabilidadCliente.setPlaceholder("Resposabilidad Cliente");

        txttipodocCliente.setPlaceholder("Tipo Doc.");

        txtdocCliente.setPlaceholder("Documento");

        btnaceptar.setText("Aceptar");
        btnaceptar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnaceptar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setText("Cancelar");
        btncancelar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btncancelar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        panel1.setColorBackground(new java.awt.Color(255, 255, 255));
        panel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txttotal.setEditable(false);
        txttotal.setForeground(new java.awt.Color(255, 255, 255));
        txttotal.setText("0.0");
        txttotal.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txttotal.setEnabled(false);
        txttotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txttotal.setOpaque(false);
        txttotal.setSelectedTextColor(new java.awt.Color(255, 0, 0));
        panel1.add(txttotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 390, 140, 50));

        lblTotal.setEditable(false);
        lblTotal.setBackground(new java.awt.Color(51, 126, 18));
        lblTotal.setForeground(new java.awt.Color(255, 255, 255));
        lblTotal.setText("  Total:          $");
        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panel1.add(lblTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 390, 270, 50));

        txtinteres.setEditable(false);
        txtinteres.setForeground(new java.awt.Color(255, 255, 255));
        txtinteres.setText("0.0");
        txtinteres.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtinteres.setEnabled(false);
        txtinteres.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtinteres.setOpaque(false);
        panel1.add(txtinteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 340, 140, 50));

        lblInteres.setEditable(false);
        lblInteres.setBackground(new java.awt.Color(252, 23, 50));
        lblInteres.setForeground(new java.awt.Color(255, 255, 255));
        lblInteres.setText(" Interes:       $");
        lblInteres.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblInteres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblInteresActionPerformed(evt);
            }
        });
        panel1.add(lblInteres, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 270, 50));

        txtdescuento.setEditable(false);
        txtdescuento.setForeground(new java.awt.Color(255, 255, 255));
        txtdescuento.setText("0.0");
        txtdescuento.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtdescuento.setOpaque(false);
        panel1.add(txtdescuento, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 290, 140, 50));

        txtsubtotal.setEditable(false);
        txtsubtotal.setForeground(new java.awt.Color(255, 255, 255));
        txtsubtotal.setText("0.0");
        txtsubtotal.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtsubtotal.setEnabled(false);
        txtsubtotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtsubtotal.setOpaque(false);
        panel1.add(txtsubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 240, 140, 50));

        lblSubtotal.setEditable(false);
        lblSubtotal.setBackground(new java.awt.Color(255, 128, 27));
        lblSubtotal.setForeground(new java.awt.Color(255, 255, 255));
        lblSubtotal.setText(" Subtotal:     $");
        lblSubtotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblSubtotalActionPerformed(evt);
            }
        });
        panel1.add(lblSubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 240, 270, 50));

        txtdescuentoPorcentaje.setEditable(false);
        txtdescuentoPorcentaje.setForeground(new java.awt.Color(255, 255, 255));
        txtdescuentoPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtdescuentoPorcentaje.setOpaque(false);
        panel1.add(txtdescuentoPorcentaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 190, 140, 50));

        cboforma.setForeground(new java.awt.Color(0, 112, 192));
        cboforma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "..." }));
        cboforma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboformaActionPerformed(evt);
            }
        });
        panel1.add(cboforma, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 250, 35));

        cboformadepago.setForeground(new java.awt.Color(0, 112, 192));
        cboformadepago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboformadepagoActionPerformed(evt);
            }
        });
        panel1.add(cboformadepago, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 5, 250, 35));

        lblDescuento1.setEditable(false);
        lblDescuento1.setBackground(new java.awt.Color(0, 102, 204));
        lblDescuento1.setForeground(new java.awt.Color(255, 255, 255));
        lblDescuento1.setText(" Descuento: $");
        lblDescuento1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblDescuento1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblDescuento1ActionPerformed(evt);
            }
        });
        panel1.add(lblDescuento1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 270, 50));

        txtinteresPorcentaje.setEditable(false);
        txtinteresPorcentaje.setForeground(new java.awt.Color(255, 255, 255));
        txtinteresPorcentaje.setText("0.0");
        txtinteresPorcentaje.setDisabledTextColor(new java.awt.Color(255, 255, 255));
        txtinteresPorcentaje.setEnabled(false);
        txtinteresPorcentaje.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtinteresPorcentaje.setOpaque(false);
        panel1.add(txtinteresPorcentaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, 140, 50));

        cbopromos.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Productos", "Promociones" }));

        javax.swing.GroupLayout PanelPedidosLayout = new javax.swing.GroupLayout(PanelPedidos);
        PanelPedidos.setLayout(PanelPedidosLayout);
        PanelPedidosLayout.setHorizontalGroup(
            PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPedidosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelPedidosLayout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(PanelPedidosLayout.createSequentialGroup()
                        .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(PanelPedidosLayout.createSequentialGroup()
                                .addComponent(txtbuscarpedidos, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(151, 151, 151)
                                .addComponent(cbopromos, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PanelPedidosLayout.createSequentialGroup()
                                    .addComponent(btncliente, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtnombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtresposabilidadCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelPedidosLayout.createSequentialGroup()
                                .addComponent(txttipodocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtdocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                            .addGroup(PanelPedidosLayout.createSequentialGroup()
                                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(PanelPedidosLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rSLabelHora1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rSLabelFecha1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        PanelPedidosLayout.setVerticalGroup(
            PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPedidosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtnombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtresposabilidadCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txttipodocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtdocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btncliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelPedidosLayout.createSequentialGroup()
                        .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rSLabelHora1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbopromos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtbuscarpedidos, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(PanelPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        PanelContenedor.add(PanelPedidos);

        PanelProductos.setColorBackground(new java.awt.Color(255, 255, 255));

        btnproductosadministra.setText("Administrar Productos");
        btnproductosadministra.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnproductosadministra.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LAYERS);
        btnproductosadministra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnproductosadministraActionPerformed(evt);
            }
        });

        tablabuscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablabuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablabuscarMouseClicked(evt);
            }
        });
        tablabuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablabuscarKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablabuscar);

        txtbuscarproductos.setPlaceholder("Buscar Productos");
        txtbuscarproductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarproductosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarproductosKeyReleased(evt);
            }
        });

        btnstockadministra.setText("Administrar Stock");
        btnstockadministra.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministraActionPerformed(evt);
            }
        });

        cbopromos1.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Productos", "Promociones" }));

        cbopromos2.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Depositos" }));

        cbopromos3.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Categorias" }));

        btnstockadministra1.setText("Promociones");
        btnstockadministra1.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra1ActionPerformed(evt);
            }
        });

        btnstockadministra2.setText("Proveedores");
        btnstockadministra2.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra2.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra2ActionPerformed(evt);
            }
        });

        btnstockadministra3.setText("Bonificaciones");
        btnstockadministra3.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra3.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelProductosLayout = new javax.swing.GroupLayout(PanelProductos);
        PanelProductos.setLayout(PanelProductosLayout);
        PanelProductosLayout.setHorizontalGroup(
            PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelProductosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(PanelProductosLayout.createSequentialGroup()
                        .addComponent(btnproductosadministra, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(btnstockadministra3, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelProductosLayout.createSequentialGroup()
                        .addComponent(txtbuscarproductos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbopromos1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbopromos3, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbopromos2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        PanelProductosLayout.setVerticalGroup(
            PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelProductosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnproductosadministra, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnstockadministra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnstockadministra1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnstockadministra2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnstockadministra3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 17, Short.MAX_VALUE)
                .addGroup(PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelProductosLayout.createSequentialGroup()
                        .addGroup(PanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtbuscarproductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbopromos3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbopromos1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 491, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbopromos2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        PanelContenedor.add(PanelProductos);

        PanelBlanco.setColorBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Logos/ybg.png"))); // NOI18N
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PanelBlancoLayout = new javax.swing.GroupLayout(PanelBlanco);
        PanelBlanco.setLayout(PanelBlancoLayout);
        PanelBlancoLayout.setHorizontalGroup(
            PanelBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBlancoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1020, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelBlancoLayout.setVerticalGroup(
            PanelBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBlancoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                .addContainerGap())
        );

        PanelContenedor.add(PanelBlanco);

        PanelConsultas.setColorBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Logos/ybg.png"))); // NOI18N
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PanelConsultasLayout = new javax.swing.GroupLayout(PanelConsultas);
        PanelConsultas.setLayout(PanelConsultasLayout);
        PanelConsultasLayout.setHorizontalGroup(
            PanelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConsultasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 1020, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelConsultasLayout.setVerticalGroup(
            PanelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelConsultasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                .addContainerGap())
        );

        PanelContenedor.add(PanelConsultas);

        PanelPagos.setColorBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Logos/ybg.png"))); // NOI18N
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PanelPagosLayout = new javax.swing.GroupLayout(PanelPagos);
        PanelPagos.setLayout(PanelPagosLayout);
        PanelPagosLayout.setHorizontalGroup(
            PanelPagosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPagosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 1020, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelPagosLayout.setVerticalGroup(
            PanelPagosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelPagosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                .addContainerGap())
        );

        PanelContenedor.add(PanelPagos);

        PanelUtilitarios.setColorBackground(new java.awt.Color(255, 255, 255));
        PanelUtilitarios.setMaximumSize(new java.awt.Dimension(1040, 630));
        PanelUtilitarios.setMinimumSize(new java.awt.Dimension(1040, 630));
        PanelUtilitarios.setLayout(new javax.swing.OverlayLayout(PanelUtilitarios));

        panel2.setColorBackground(new java.awt.Color(255, 255, 255));

        rSButtonMaterialIconUno1.setText("rSButtonMaterialIconUno1");

        rSButtonMaterialIconUno2.setText("rSButtonMaterialIconUno1");

        rSButtonMaterialIconUno3.setText("rSButtonMaterialIconUno1");

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rSButtonMaterialIconUno1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rSButtonMaterialIconUno3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rSButtonMaterialIconUno2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(394, Short.MAX_VALUE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rSButtonMaterialIconUno1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rSButtonMaterialIconUno2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rSButtonMaterialIconUno3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(591, Short.MAX_VALUE))
        );

        PanelUtilitarios.add(panel2);

        PanelContenedor.add(PanelUtilitarios);

        PanelCaja.setColorBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Logos/ybg.png"))); // NOI18N
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PanelCajaLayout = new javax.swing.GroupLayout(PanelCaja);
        PanelCaja.setLayout(PanelCajaLayout);
        PanelCajaLayout.setHorizontalGroup(
            PanelCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCajaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 1020, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelCajaLayout.setVerticalGroup(
            PanelCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCajaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                .addContainerGap())
        );

        PanelContenedor.add(PanelCaja);

        PanelClientes.setColorBackground(new java.awt.Color(255, 255, 255));

        btnproductosadministra1.setText("Administrar Productos");
        btnproductosadministra1.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnproductosadministra1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LAYERS);
        btnproductosadministra1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnproductosadministra1ActionPerformed(evt);
            }
        });

        tablabuscar1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablabuscar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablabuscar1MouseClicked(evt);
            }
        });
        tablabuscar1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablabuscar1KeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tablabuscar1);

        txtbuscarproductos1.setPlaceholder("Buscar Productos");
        txtbuscarproductos1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarproductos1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarproductos1KeyReleased(evt);
            }
        });

        btnstockadministra4.setText("Administrar Stock");
        btnstockadministra4.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra4.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra4ActionPerformed(evt);
            }
        });

        cbopromos4.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Productos", "Promociones" }));

        cbopromos5.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Depositos" }));

        cbopromos6.setForeground(new java.awt.Color(0, 112, 192));
        cbopromos6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Categorias" }));

        btnstockadministra5.setText("Promociones");
        btnstockadministra5.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra5.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra5ActionPerformed(evt);
            }
        });

        btnstockadministra6.setText("Bonificaciones");
        btnstockadministra6.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra6.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra6ActionPerformed(evt);
            }
        });

        btnstockadministra7.setText("Bonificaciones");
        btnstockadministra7.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnstockadministra7.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.EXTENSION);
        btnstockadministra7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnstockadministra7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelClientesLayout = new javax.swing.GroupLayout(PanelClientes);
        PanelClientes.setLayout(PanelClientesLayout);
        PanelClientesLayout.setHorizontalGroup(
            PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(PanelClientesLayout.createSequentialGroup()
                        .addComponent(btnproductosadministra1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra4, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra5, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(btnstockadministra7, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnstockadministra6, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelClientesLayout.createSequentialGroup()
                        .addComponent(txtbuscarproductos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbopromos4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbopromos6, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbopromos5, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        PanelClientesLayout.setVerticalGroup(
            PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnproductosadministra1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnstockadministra4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnstockadministra5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnstockadministra6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnstockadministra7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 17, Short.MAX_VALUE)
                .addGroup(PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelClientesLayout.createSequentialGroup()
                        .addGroup(PanelClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtbuscarproductos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbopromos6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbopromos4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 491, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbopromos5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        PanelContenedor.add(PanelClientes);

        PanelPrincipal.add(PanelContenedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 1040, 630));

        getContentPane().add(PanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1300, 650));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnproductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnproductosKeyPressed

    }//GEN-LAST:event_btnproductosKeyPressed

    private void btnclientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnclientesKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnclientesKeyPressed

    private void btnpedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnpedidosKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnpedidosKeyPressed

    private void btnconsultasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnconsultasKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnconsultasKeyPressed

    private void btnproveedoresKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnproveedoresKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnproveedoresKeyPressed

    private void btnpagosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnpagosKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnpagosKeyPressed

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btncerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btncerrarActionPerformed

    private void btnpedidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpedidosActionPerformed
        PanelPedidos.setVisible(true);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        txtbuscarpedidos.requestFocus();
    }//GEN-LAST:event_btnpedidosActionPerformed

    private void btnproductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnproductosActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(true);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        cargartablabuscar("");
    }//GEN-LAST:event_btnproductosActionPerformed

    private void btnclientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclientesActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(true);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnclientesActionPerformed

    private void btnconsultasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnconsultasActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(true);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnconsultasActionPerformed

    private void btnpagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpagosActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(true);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnpagosActionPerformed

    private void btnproveedoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnproveedoresActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(true);
    }//GEN-LAST:event_btnproveedoresActionPerformed

    private void btnclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclienteActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(true);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        //--------------------------------------------------
        new ClientesElegir(this, true).setVisible(true);
        txtnombreCliente.setText(nombreCliente);
        txtresposabilidadCliente.setText(ResposabilidadCliente);
        txttipodocCliente.setText(TipodocCliente);
        txtdocCliente.setText(DocCliente);
        cargatotales();
        //---------------------------------------------------------------
        PanelPedidos.setVisible(true);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(true);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnclienteActionPerformed

    private void txtbuscarpedidosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarpedidosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            bcant = 0;

            if (tablabuscar.getRowCount() == 1) {
                tablabuscar.setRowSelectionInterval(0, 0);
                System.out.println("txtbuscarpedidosKeyPressed 1 producto ");
                agrega_producto();
            } else {
                System.out.println("txtbuscarpedidosKeyPressed + producto ");
                PanelPedidos.setVisible(false);
                PanelProductos.setVisible(true);
                tablabuscar.setRowSelectionInterval(0, 0);
                tablabuscar.requestFocus();
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_ADD) {
            btnaceptar.doClick();
            txtbuscarpedidos.setText("");
        }
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            if (tablapedidos.getRowCount() >= 1) {
                tablapedidos.setRowSelectionInterval(0, 0);
            } else {
                txtbuscarpedidos.requestFocus();
            }
        }
    }//GEN-LAST:event_txtbuscarpedidosKeyPressed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        //No se Puede Hacer un Pedido si la Caja esta Cerrada
        ClaseCaja caja = new ClaseCaja();
        int idCaja = caja.iniciocaja();
        if (idCaja == 0) {
            JOptionPane.showMessageDialog(null, "Debe Realizar la Apertura de Caja");
        } else {
            if (txttotal.getText().equals("") || txtsubtotal.getText().equals("")
                    || txtnombreCliente.getText().equals("") || tablapedidos.getRowCount() == 0) {
                JOptionPane.showMessageDialog(null, "Debe completar todos los campos obligatorios...");
            } else {
                /////   Carga Variables

                total = txttotal.getText();
                String[][] datos = new String[tablapedidos.getRowCount()][12];
                for (int i = 0; i < tablapedidos.getRowCount(); i++) {
                    datos[i][0] = tablapedidos.getValueAt(i, 0).toString();
                    datos[i][1] = tablapedidos.getValueAt(i, 1).toString();
                    datos[i][2] = tablapedidos.getValueAt(i, 2).toString();
                    datos[i][3] = tablapedidos.getValueAt(i, 3).toString();
                    datos[i][4] = tablapedidos.getValueAt(i, 4).toString();
                    datos[i][5] = tablapedidos.getValueAt(i, 5).toString();
                    datos[i][6] = tablapedidos.getValueAt(i, 6).toString();
                    datos[i][7] = tablapedidos.getValueAt(i, 7).toString();
                    datos[i][8] = tablapedidos.getValueAt(i, 8).toString();
                    datos[i][9] = tablapedidos.getValueAt(i, 9).toString();
                    datos[i][10] = tablapedidos.getValueAt(i, 10).toString();
                    datos[i][11] = tablapedidos.getValueAt(i, 11).toString();

                }

                new MainImprimir(this, true).setVisible(true);
                //Pregunto si no Cancelo el Pedido
                if (MainImprimir.cancelar == 1) {
                    //////////////////////////////////
                    //Pregunto si el es CTA CTE (id=2)
                    if (forma[indexformapago].getidFormasdepago() == 2) {
                        ClasePedidos pedido = new ClasePedidos();
                        int Idctacte = pedido.ComprueboCatCte(IdCliente, Double.valueOf(txttotal.getText()));
                        //Si la cta cte esta OK
                        if (Idctacte != 0) {
                            //Si Facturar 
                            if (MainImprimir.facturar == 1) {
                                try {
                                    ///Deberia elegir pto de venta Pto de Venta=1 por defecto
                                    int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                                    pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                    idPtodeVenta = 1;
                                    pedido.FacturarPedido(IdCliente, Idpedidos, idPtodeVenta);
                                } catch (Exception e) {
                                    JOptionPane.showMessageDialog(null, "Error 1846 al cargar el pedido");
                                    System.out.println("error 1847 " + e);
                                }
                            } else {
                                //Si MainImprimir 
                                if (MainImprimir.imprimir == 1) {
                                    int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                                    pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                    pedido.ImprimirPedido(Idpedidos);

                                }
                                //No imprimir no Facturar
                                if (MainImprimir.imprimir == 0) {
                                    int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                                    pedido.CompraCtaCte(Idctacte, Idpedidos, Double.valueOf(txttotal.getText()));
                                }
                            }
                            borrartablapedidos();
                            cboformadepago.setSelectedIndex(0);
                            cboforma.setSelectedIndex(0);
                            //txtdescuento.setEditable(false);
                            txtinteres.setText("0.0");
                            txtdescuento.setText("0.0");
                            txtinteresPorcentaje.setText("0.0");
                            txtdescuentoPorcentaje.setText("0.0");
                            cargatotales();
                            cargartablabuscar("");
                            cargarconsumidorfinal();
                            btncancelar.transferFocus();
                        }
                        ///////////////////////////////////////
                        //No es CTA CTE
                    } else {

                        //Si Facturar 
                        if (MainImprimir.facturar == 1) {
                            ///Deberia elegir pto de venta Pto de Venta=1 por defecto
                            ClasePedidos pedido = new ClasePedidos();
                            int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                            idPtodeVenta = 1;
                            pedido.FacturarPedido(IdCliente, Idpedidos, idPtodeVenta);

                        } else {
                            //Si MainImprimir 
                            if (MainImprimir.imprimir == 1) {
                                ClasePedidos pedido = new ClasePedidos();
                                int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                                pedido.ImprimirPedido(Idpedidos);
                            }
                            //No imprimir no Facturar
                            if (MainImprimir.imprimir == 0) {
                                ClasePedidos pedido = new ClasePedidos();
                                int Idpedidos = pedido.AgregarPedido(datos, fnCargarFecha.cargarfecha(), fnCargarFecha.cargarHora(), Double.valueOf(txttotal.getText()), Double.valueOf(txtdescuento.getText()), IdCliente, IdDeposito, forma[indexformapago].getidFormasdepago(), descuentobase[0], descuentoporcentaje[0], Double.valueOf(txtsubtotal.getText()), idCaja, interesbase[0], interesporcentaje[0], descripcion);
                            }
                        }

                        System.out.println("borrar tabla_pedido");
                        borrartablapedidos();
                        cboformadepago.setSelectedIndex(0);
                        cboforma.setSelectedIndex(0);
                        //txtdescuento.setEditable(false);
                        txtinteres.setText("0.0");
                        txtdescuento.setText("0.0");
                        txtinteresPorcentaje.setText("0.0");
                        txtdescuentoPorcentaje.setText("0.0");
                        cargatotales();
                        cargartablabuscar("");
                        cargarconsumidorfinal();
                        btncancelar.transferFocus();

                    }
                }

            }
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        borrartablapedidos();
        cargarformadepago();
        cboformadepago.setSelectedIndex(0);
        //txtdescuento.setEditable(false);
        txtinteres.setText("0.0");
        txtdescuento.setText("0.0");
        txtinteresPorcentaje.setText("0.0");
        txtdescuentoPorcentaje.setText("0.0");
        cbopromos.setSelectedIndex(0);
        cargatotales();
        cargartablabuscar("");
        cargarconsumidorfinal();
        btncancelar.transferFocus();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txtbuscarpedidosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarpedidosKeyReleased
        TableRowSorter sorter = new TableRowSorter(ModeloProductos);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscarpedidos.getText() + ".*"));
        tablabuscar.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarpedidosKeyReleased

    private void tablabuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            //Cantidad.bc = 0;
            new Cantidad(null, true).setVisible(true);
            int bandera = 0;
            DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
            if (tablabuscar.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
            } else {
                double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());
                if (bcant != 0) {
                    if (cbopromos.getSelectedItem().toString().equals("Productos")) {
                        pesable = Integer.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());
                        double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                        double cant = bcant;
                        double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                        double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                        fnRedondear redondear = new fnRedondear();
                        Object nuevo[] = {
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                            bcant,
                            "",
                            redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant),
                            "",
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString()
                        };
                        /////Sacao el codigo de la tabla buscar
                        String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                        /////Recorro la tabla pedidos
                        int filaspedidos = tablapedidos.getRowCount();
                        int i = 0;
                        if (filaspedidos != 0) {
                            while (filaspedidos > i) {
                                /////Sacao el codigo tabla pedido
                                String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                if (codigobuscar.equals(codigopedido)) {
                                    bandera = 1;
                                    break;
                                }
                                i++;
                            }
                            if (bandera == 0) {
                                temp.addRow(nuevo);
                                System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                                /////Resto la cantidad al stock de la tabla buscar
                                tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                            } else {
                                /////Recooro la tabla pedidos
                                i = 0;
                                while (filaspedidos > i) {
                                    String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                    /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                    if (codigobuscar.equals(codigopedido2)) {
                                        double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                        precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                        bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                        iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                        tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                        tablapedidos.setValueAt(redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                        System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                                        tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                        break;
                                    }
                                    i++;
                                }
                            }
                        } else {
                            temp.addRow(nuevo);
                            System.out.println("Formularios.Main.tablabuscarKeyPressed()");
                            /////Resto la cantidad al stock de la tabla buscar
                            tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                        }
                        cargatotales();
                        /////////////////////////////////////////////////////////////
                        tamañotablapedido();
                        /////////////////////////////////////////////////////////////
                        txtbuscarpedidos.setText("");
                        txtbuscarpedidos.requestFocus();
                        PanelPedidos.setVisible(true);
                        PanelProductos.setVisible(false);
                        PanelBlanco.setVisible(false);
                    } else {
                        bcant = 1;
                        int id_promociones = Integer.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 0).toString());
                        System.out.println("id_promociones:" + id_promociones);
                        String sql = "SELECT * FROM vista_promociones_detalle WHERE estado=1 and idpromociones=" + id_promociones;

                        ConexionMySQL cc = new ConexionMySQL();
                        Connection cn = cc.Conectar();
                        Statement SelectVProducto = null;
                        Statement SelectStock = null;
                        try {
                            SelectVProducto = cn.createStatement();
                            ResultSet rs = SelectVProducto.executeQuery(sql);
                            while (rs.next()) {
                                Object nuevo[] = {
                                    rs.getString(1),
                                    rs.getString(2),
                                    rs.getString(3),
                                    rs.getString(4),
                                    rs.getString(5),
                                    rs.getString(6),
                                    rs.getString(7),
                                    rs.getString(8),
                                    rs.getString(9),
                                    rs.getString(10),
                                    rs.getString(13),
                                    rs.getString(14)};

                                System.out.println("sale.... " + bcant);
                                /////Sacao el codigo de la tabla buscarcvbcv
                                String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                                /////Recorro la tabla pedidos
                                int filaspedidos = tablapedidos.getRowCount();
                                int i = 0;
                                if (filaspedidos != 0) {
                                    while (filaspedidos > i) {
                                        /////Sacao el codigo tabla pedido
                                        String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                        if (codigobuscar.equals(codigopedido)) {
                                            bandera = 1;
                                            break;
                                        }
                                        i++;
                                    }
                                    if (bandera == 0) {
                                        temp.addRow(nuevo);
                                        /////Resto la cantidad al stock de la tabla buscar                                            
                                    } else {
                                        /////Recooro la tabla pedidos
                                        i = 0;
                                        fnRedondear redondear = new fnRedondear();
                                        while (filaspedidos > i) {

                                            String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                            /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                            if (codigobuscar.equals(codigopedido2)) {
                                                int cantidad = Integer.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                                double precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                                double bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                                double iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                                tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                                tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 + (iva / 100))) * (cantidad + bcant)), i, 8);
                                                break;
                                            }
                                            i++;
                                        }
                                    }
                                } else {
                                    temp.addRow(nuevo);
                                }
                            }

                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                            JOptionPane.showMessageDialog(null, ex);
                        } finally {
                            try {
                                if (SelectVProducto != null) {
                                    SelectVProducto.close();
                                }
                                if (SelectStock != null) {
                                    SelectStock.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                        /// System.out.println("tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString() " + tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());

                        cargatotales();
                        /////////////////////////////////////////////////////////////
                        tamañotablapedido();
                        /////////////////////////////////////////////////////////////
                        txtbuscarpedidos.setText("");
                        txtbuscarpedidos.requestFocus();

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
                }

            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            int fila = tablapedidos.getRowCount();
            if (fila != 0) {
                bcant = 0;
                tablabuscar.transferFocus();
                evt.consume();
                tablapedidos.setRowSelectionInterval(0, 0);
                txtbuscarpedidos.setText("");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_B) {
            txtbuscarpedidos.requestFocus();
            txtbuscarpedidos.setText("");
        }
    }//GEN-LAST:event_tablabuscarKeyPressed

    private void lblInteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblInteresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblInteresActionPerformed

    private void lblSubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblSubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblSubtotalActionPerformed

    private void tablabuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablabuscarMouseClicked

        if (evt.getClickCount() == 2) {
            //Cantidad.bc = 0;
            ///new Cantidad(null, true).setVisible(true);
            bcant = 1;
            int bandera = 0;
            DefaultTableModel temp = (DefaultTableModel) tablapedidos.getModel();
            if (tablabuscar.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
            } else {

                if (cbopromos.getSelectedItem().toString().equals("Productos")) {
                    System.out.println(pesable);
                    pesable = Integer.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());

                    double stock = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 7).toString());

                    if (bcant != 0) {
                        double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                        double cant = Double.valueOf(bcant);
                        double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                        double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                        fnRedondear redondear = new fnRedondear();
                        Object nuevo[] = {
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 1).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 2).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 3).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString(),
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 6).toString(),
                            bcant,
                            "",
                            redondear.dosDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * cant),
                            "",
                            tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString()};
                        /////Sacao el codigo de la tabla buscar
                        String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                        /////Recorro la tabla pedidos
                        int filaspedidos = tablapedidos.getRowCount();
                        int i = 0;
                        if (filaspedidos != 0) {
                            while (filaspedidos > i) {
                                /////Sacao el codigo tabla pedido
                                String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                if (codigobuscar.equals(codigopedido)) {
                                    bandera = 1;
                                    break;
                                }
                                i++;
                            }
                            if (bandera == 0) {
                                temp.addRow(nuevo);
                                System.out.println(".mouseClicked()");
                                /////Resto la cantidad al stock de la tabla buscar
                                tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                            } else {
                                /////Recooro la tabla pedidos
                                i = 0;
                                while (filaspedidos > i) {

                                    String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                    /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                    if (codigobuscar.equals(codigopedido2)) {
                                        double cantidad = Double.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                        precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                        bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                        iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                        tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                        tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100))) * (cantidad + bcant)), i, 8);
                                        System.out.println(".mouseClicked()");
                                        tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                                        break;
                                    }
                                    i++;
                                }
                            }
                        } else {
                            temp.addRow(nuevo);
                            /////Resto la cantidad al stock de la tabla buscar
                            System.out.println(".mouseClicked()");
                            tablabuscar.setValueAt((stock - bcant), tablabuscar.getSelectedRow(), 7);
                        }
                        cargatotales();
                        /////////////////////////////////////////////////////////////
                        tamañotablapedido();
                        /////////////////////////////////////////////////////////////
                        txtbuscarpedidos.setText("");
                        txtbuscarpedidos.requestFocus();
                        PanelPedidos.setVisible(true);
                        PanelProductos.setVisible(false);
                        PanelBlanco.setVisible(false);

                    } else {
                        JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
                    }
                } else {
                    bcant = 1;
                    int id_promociones = Integer.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 0).toString());
                    System.out.println("id_promociones:" + id_promociones);
                    String sql = "SELECT * FROM vista_promociones_detalle WHERE estado=1 and idpromociones=" + id_promociones;

                    ConexionMySQL cc = new ConexionMySQL();
                    Connection cn = cc.Conectar();
                    Statement SelectVProducto = null;
                    Statement SelectStock = null;
                    try {
                        SelectVProducto = cn.createStatement();
                        ResultSet rs = SelectVProducto.executeQuery(sql);
                        while (rs.next()) {
                            Object nuevo[] = {
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8),
                                rs.getString(9),
                                rs.getString(10),
                                rs.getString(13),
                                rs.getString(14)};

                            System.out.println("sale.... " + bcant);
                            /////Sacao el codigo de la tabla buscarcvbcv
                            String codigobuscar = tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString();

                            /////Recorro la tabla pedidos
                            int filaspedidos = tablapedidos.getRowCount();
                            int i = 0;
                            if (filaspedidos != 0) {
                                while (filaspedidos > i) {
                                    /////Sacao el codigo tabla pedido
                                    String codigopedido = tablapedidos.getValueAt(i, 0).toString();
                                    if (codigobuscar.equals(codigopedido)) {
                                        bandera = 1;
                                        break;
                                    }
                                    i++;
                                }
                                if (bandera == 0) {
                                    temp.addRow(nuevo);
                                    /////Resto la cantidad al stock de la tabla buscar                                            
                                } else {
                                    /////Recooro la tabla pedidos
                                    i = 0;
                                    fnRedondear redondear = new fnRedondear();
                                    while (filaspedidos > i) {

                                        String codigopedido2 = tablapedidos.getValueAt(i, 0).toString();
                                        /////Sumo cantidad en la tabla pedidos si el cod son iguales
                                        if (codigobuscar.equals(codigopedido2)) {
                                            int cantidad = Integer.valueOf(tablapedidos.getValueAt(i, 7).toString());
                                            double precio = Double.valueOf(tablapedidos.getValueAt(i, 3).toString());
                                            double bonif = Double.valueOf(tablapedidos.getValueAt(i, 4).toString());
                                            double iva = Double.valueOf(tablapedidos.getValueAt(i, 6).toString());
                                            tablapedidos.setValueAt(cantidad + bcant, i, 7);
                                            tablapedidos.setValueAt(redondear.UNDigitos(redondear.dosDigitos(precio * (1 - (bonif / 100)) * (1 + (iva / 100))) * (cantidad + bcant)), i, 8);
                                            break;
                                        }
                                        i++;
                                    }
                                }
                            } else {
                                temp.addRow(nuevo);
                            }
                        }

                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        JOptionPane.showMessageDialog(null, ex);
                    } finally {
                        try {
                            if (SelectVProducto != null) {
                                SelectVProducto.close();
                            }
                            if (SelectStock != null) {
                                SelectStock.close();
                            }
                            if (cn != null) {
                                cn.close();
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                    /// System.out.println("tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString() " + tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());

                    cargatotales();
                    /////////////////////////////////////////////////////////////
                    tamañotablapedido();
                    /////////////////////////////////////////////////////////////
                    txtbuscarpedidos.setText("");
                    txtbuscarpedidos.requestFocus();
                    PanelPedidos.setVisible(true);
                    PanelProductos.setVisible(false);
                    PanelBlanco.setVisible(false);
                }

            }
        }
    }//GEN-LAST:event_tablabuscarMouseClicked

    private void cboformadepagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboformadepagoActionPerformed
        if (cboformadepago.getSelectedItem().toString().equals("CONTADO")) {
            System.out.println("contado");
            borrarcboforma();
            System.out.println("contado2");
            cboforma.addItem("...");
            System.out.println("contado3");
            indexformapago = 0;
            cargatotales();
            System.out.println("contado4");
        } else if (cboformadepago.getSelectedItem().toString().equals("CUENTA CORRIENTE")) {
            System.out.println("cc");
            borrarcboforma();
            System.out.println("cc2");
            cargarformactacte();
            System.out.println("cc3");
            indexformapago = 1;
        } else if (cboformadepago.getSelectedItem().toString().equals("TARJETA DE CREDITO")) {
            System.out.println("credito");
            borrarcboforma();
            System.out.println("credito2");
            cargartarjetacredito();
            System.out.println("credito3");
            indexformapago = 2;
        } else if (cboformadepago.getSelectedItem().toString().equals("TARJETA DE DEBITO")) {
            System.out.println("debito");
            borrarcboforma();
            cargartarjetadebito();
            indexformapago = 3;
        } else if (cboformadepago.getSelectedItem().toString().equals("CHEQUE")) {
            System.out.println("cheque");
            borrarcboforma();
            cargarformacheque();
            indexformapago = 4;
        } else if (cboformadepago.getSelectedItem().toString().equals("OTROS")) {
            System.out.println("otros");
            borrarcboforma();
            txtdescuento.setEditable(true);
            indexformapago = 5;
            cargatotales();
        }
    }//GEN-LAST:event_cboformadepagoActionPerformed

    private void lblDescuento1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblDescuento1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblDescuento1ActionPerformed

    private void cboformaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboformaActionPerformed
        if (bandera_combo == 0) {
            cargatotales();
        }
    }//GEN-LAST:event_cboformaActionPerformed

    private void txtbuscarproductosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarproductosKeyReleased
        TableRowSorter sorter = new TableRowSorter(ModeloProductos);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscarproductos.getText() + ".*"));
        tablabuscar.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarproductosKeyReleased

    private void txtbuscarproductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarproductosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            bcant = 0;

            if (tablabuscar.getRowCount() == 1) {
                tablabuscar.setRowSelectionInterval(0, 0);
                System.out.println("txtbuscarpedidosKeyPressed 1 producto ");
                agrega_producto();
            } else {
                System.out.println("txtbuscarpedidosKeyPressed + producto ");
                PanelPedidos.setVisible(false);
                PanelProductos.setVisible(true);
                tablabuscar.setRowSelectionInterval(0, 0);
                tablabuscar.requestFocus();
            }
        }
    }//GEN-LAST:event_txtbuscarproductosKeyPressed

    private void btnstockadministraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministraActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        new Stock(this, true).setVisible(true);
        cargartablabuscar("");
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(true);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnstockadministraActionPerformed

    private void btnproductosadministraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnproductosadministraActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        new Productos(this, true).setVisible(true);
        cargartablabuscar("");
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(true);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnproductosadministraActionPerformed

    private void btncajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncajaActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(true);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btncajaActionPerformed

    private void btncajaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btncajaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btncajaKeyPressed

    private void btnstockadministra1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra1ActionPerformed
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        new Promociones(this, true).setVisible(true);
        cargartablabuscar("");
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(true);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnstockadministra1ActionPerformed

    private void btnstockadministra2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnstockadministra2ActionPerformed

    private void btnstockadministra3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra3ActionPerformed
         PanelPedidos.setVisible(false);
        PanelProductos.setVisible(false);
        PanelBlanco.setVisible(true);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
        new Bonificaciones(this, true).setVisible(true);
        cargartablabuscar("");
        PanelPedidos.setVisible(false);
        PanelProductos.setVisible(true);
        PanelBlanco.setVisible(false);
        PanelCaja.setVisible(false);
        PanelClientes.setVisible(false);
        PanelConsultas.setVisible(false);
        PanelPagos.setVisible(false);
        PanelUtilitarios.setVisible(false);
    }//GEN-LAST:event_btnstockadministra3ActionPerformed

    private void btnproductosadministra1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnproductosadministra1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnproductosadministra1ActionPerformed

    private void tablabuscar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablabuscar1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablabuscar1MouseClicked

    private void tablabuscar1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscar1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tablabuscar1KeyPressed

    private void txtbuscarproductos1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarproductos1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarproductos1KeyPressed

    private void txtbuscarproductos1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarproductos1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarproductos1KeyReleased

    private void btnstockadministra4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnstockadministra4ActionPerformed

    private void btnstockadministra5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnstockadministra5ActionPerformed

    private void btnstockadministra6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnstockadministra6ActionPerformed

    private void btnstockadministra7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnstockadministra7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnstockadministra7ActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel MenuVertical;
    private necesario.Panel PanelBlanco;
    private necesario.Panel PanelCaja;
    private necesario.Panel PanelClientes;
    private necesario.Panel PanelConsultas;
    private necesario.Panel PanelContenedor;
    private necesario.Panel PanelMenu;
    private necesario.Panel PanelPagos;
    private necesario.Panel PanelPedidos;
    private necesario.Panel PanelPrincipal;
    private necesario.Panel PanelProductos;
    private necesario.Panel PanelUtilitarios;
    private RSMaterialComponent.RSButtonMaterialIconUno btnaceptar;
    private RSMaterialComponent.RSButtonMaterialIconOne btncaja;
    private RSMaterialComponent.RSButtonMaterialIconUno btncancelar;
    private RSMaterialComponent.RSButtonIconUno btncerrar;
    private RSMaterialComponent.RSButtonMaterialIconUno btncliente;
    private RSMaterialComponent.RSButtonMaterialIconOne btnclientes;
    private RSMaterialComponent.RSButtonMaterialIconOne btnconsultas;
    private RSMaterialComponent.RSButtonMaterialIconOne btnpagos;
    private RSMaterialComponent.RSButtonMaterialIconOne btnpedidos;
    private RSMaterialComponent.RSButtonMaterialIconOne btnproductos;
    private RSMaterialComponent.RSButtonMaterialIconUno btnproductosadministra;
    private RSMaterialComponent.RSButtonMaterialIconUno btnproductosadministra1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnproveedores;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra1;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra2;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra3;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra4;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra5;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra6;
    private RSMaterialComponent.RSButtonMaterialIconUno btnstockadministra7;
    private RSMaterialComponent.RSComboBoxMaterial cboforma;
    private RSMaterialComponent.RSComboBoxMaterial cboformadepago;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos1;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos2;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos3;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos4;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos5;
    private RSMaterialComponent.RSComboBoxMaterial cbopromos6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private rojeru_san.rsfield.RSTextField lblDescuento1;
    private rojeru_san.rsfield.RSTextField lblInteres;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private rojeru_san.rsfield.RSTextField lblSubtotal;
    private rojeru_san.rsfield.RSTextField lblTotal;
    private necesario.Panel panel1;
    private necesario.Panel panel2;
    private RSMaterialComponent.RSButtonMaterialIconUno rSButtonMaterialIconUno1;
    private RSMaterialComponent.RSButtonMaterialIconUno rSButtonMaterialIconUno2;
    private RSMaterialComponent.RSButtonMaterialIconUno rSButtonMaterialIconUno3;
    private rojeru_san.rsdate.RSLabelFecha rSLabelFecha1;
    private rojeru_san.rsdate.RSLabelHora rSLabelHora1;
    private rojerusan.RSTableMetro1 tablabuscar;
    private rojerusan.RSTableMetro1 tablabuscar1;
    private rojerusan.RSTableMetro1 tablapedidos;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscarpedidos;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscarproductos;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscarproductos1;
    private rojeru_san.rsfield.RSTextField txtdescuento;
    private rojeru_san.rsfield.RSTextField txtdescuentoPorcentaje;
    private RSMaterialComponent.RSTextFieldMaterial txtdocCliente;
    private rojeru_san.rsfield.RSTextField txtinteres;
    private rojeru_san.rsfield.RSTextField txtinteresPorcentaje;
    private RSMaterialComponent.RSTextFieldMaterial txtnombreCliente;
    private RSMaterialComponent.RSTextFieldMaterial txtresposabilidadCliente;
    private rojeru_san.rsfield.RSTextField txtsubtotal;
    private RSMaterialComponent.RSTextFieldMaterial txttipodocCliente;
    private rojeru_san.rsfield.RSTextField txttotal;
    // End of variables declaration//GEN-END:variables
}
