/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnAlinear;
import Controlador.fnExportar;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ybc
 */
public class Stock extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String idProductos = "";

    public Stock(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocation(450, 100);
        this.setResizable(false);
        cargardatosdepositos();
        cargartabla("");
        dobleclick();
    }

    //////////////////////FUNCION CARGAR DEPOSITO //////////////////////
    void cargardatosdepositos() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDeposito = null;
        sSQL = "SELECT nombre FROM deposito";
        cbodeposito.removeAllItems();
        cbodeposito.addItem("Depositos");
        try {
            SelectDeposito = cn.createStatement();
            ResultSet rs = SelectDeposito.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbodeposito.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDeposito != null) {
                    SelectDeposito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"Id", "Codigo", "Producto", "Cantidad", "Deposito"};
        String[] Registros = new String[5];
        String sql = "SELECT * FROM vista_stock "
                + "WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + valor + "%' ";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                model.addRow(Registros);
            }
            tablastock.setModel(model);
            tablastock.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablastock.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablastock.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(0).setMaxWidth(0);
            tablastock.getColumnModel().getColumn(0).setMinWidth(0);
            tablastock.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablastock.getColumnModel().getColumn(1).setPreferredWidth(80);
            tablastock.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablastock.getColumnModel().getColumn(3).setPreferredWidth(60);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION FILTRAR DEPOSITO//////////////////////
    void filtrardeposito(String valor) {
        String[] Titulo = {"Id", "Codigo", "Producto", "Cantidad", "Deposito"};
        String[] Registros = new String[5];
        String sql = "SELECT * FROM vista_stock "
                + "WHERE deposito='" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                model.addRow(Registros);

            }
            tablastock.setModel(model);
            tablastock.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablastock.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablastock.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearIzquierda());
            tablastock.getColumnModel().getColumn(0).setMaxWidth(0);
            tablastock.getColumnModel().getColumn(0).setMinWidth(0);
            tablastock.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablastock.getColumnModel().getColumn(1).setPreferredWidth(80);
            tablastock.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablastock.getColumnModel().getColumn(3).setPreferredWidth(60);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

//////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablastock.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablastock.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idProductos = tablastock.getValueAt(tablastock.getSelectedRow(), 0).toString();
                        new StockDetalle(null, true).setVisible(true);
                        cargartabla("");
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldMaterial();
        btnexportar = new RSMaterialComponent.RSButtonMaterialIconOne();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablastock = new rojerusan.RSTableMetro1();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btndeposito = new RSMaterialComponent.RSButtonMaterialIconOne();
        btndetalle = new RSMaterialComponent.RSButtonMaterialIconOne();
        cbodeposito = new rojerusan.RSComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Stock de Productos");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 700, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(732, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, 20));

        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_IN);
        panelcontenedor.add(rSLabelIcon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 40, 40));

        txtbuscar.setPlaceholder("Buscar Productos");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 290, 40));

        btnexportar.setText("Exportar");
        btnexportar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnexportar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnexportar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.FOLDER_SHARED);
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });
        btnexportar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnexportarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnexportar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 420, 141, -1));

        tablastock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablastock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablastockMouseClicked(evt);
            }
        });
        tablastock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablastockKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablastock);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 730, 320));

        btnsalir.setText("Volver");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.HOME);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 420, 130, -1));

        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, 130, -1));

        btndeposito.setText("Deposito");
        btndeposito.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btndeposito.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btndeposito.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.GPS_FIXED);
        btndeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndepositoActionPerformed(evt);
            }
        });
        btndeposito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btndepositoKeyPressed(evt);
            }
        });
        panelcontenedor.add(btndeposito, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 420, 130, -1));

        btndetalle.setText("Detalle");
        btndetalle.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btndetalle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btndetalle.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_OUT_MAP);
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });
        btndetalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btndetalleKeyPressed(evt);
            }
        });
        panelcontenedor.add(btndetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 420, 130, -1));

        cbodeposito.setForeground(new java.awt.Color(0, 112, 192));
        cbodeposito.setColorBoton(new java.awt.Color(255, 255, 255));
        cbodeposito.setColorFondo(new java.awt.Color(255, 255, 255));
        cbodeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbodepositoActionPerformed(evt);
            }
        });
        panelcontenedor.add(cbodeposito, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 40, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        int fila = tablastock.getRowCount();
        if (fila != 0) {
            txtbuscar.transferFocus();
            tablastock.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablastock.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablastock);
            nom.add("Tabla Stock");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                fnExportar e = new fnExportar(new File(archivo), tb, nom);
                e.export();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btnexportarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnexportarKeyPressed

    }//GEN-LAST:event_btnexportarKeyPressed

    private void tablastockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablastockMouseClicked
        if (evt.getClickCount() == 2) {
            btndetalle.doClick();
        }
    }//GEN-LAST:event_tablastockMouseClicked

    private void tablastockKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablastockKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablastockKeyPressed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        new StockAgrega(null, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void btndepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndepositoActionPerformed
        new Depositos(null, true).setVisible(true);
    }//GEN-LAST:event_btndepositoActionPerformed

    private void btndepositoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btndepositoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndepositoKeyPressed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablastock.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idProductos = tablastock.getValueAt(tablastock.getSelectedRow(), 0).toString();
            new StockDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btndetalleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btndetalleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndetalleKeyPressed

    private void cbodepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbodepositoActionPerformed
        if (cbodeposito.getSelectedItem() != null) {
            if (!cbodeposito.getSelectedItem().toString().equals("Categorias")) {
                filtrardeposito(cbodeposito.getSelectedItem().toString());
            } else {
                cargartabla("");
            }
        }
    }//GEN-LAST:event_cbodepositoActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btndeposito;
    private RSMaterialComponent.RSButtonMaterialIconOne btndetalle;
    private RSMaterialComponent.RSButtonMaterialIconOne btnexportar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private rojerusan.RSComboBox cbodeposito;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSTableMetro1 tablastock;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscar;
    // End of variables declaration//GEN-END:variables
}
