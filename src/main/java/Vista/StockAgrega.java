/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.cboDeposito;
import Controlador.cboProveedor;
import Controlador.cboTipoIva;
import Controlador.fnCargarFecha;
import Controlador.fnEscape;
import Modelo.ClaseStock;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ybc
 */
public class StockAgrega extends javax.swing.JDialog {

    DefaultTableModel model;
    int mod = 0, idusuarios, idProducto;
    String fechastock;

    public StockAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(450, 100);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        cargartablaproducto("");
        cargardatosdepositos();
        cargarTipoIva();
        cargardatosproveedor();
        cbotipoiva.setSelectedIndex(2);
    }

    void cargardatos() {

        idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
        txtproducto.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());
        lblunuidad.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 3).toString());
        cargar_ultimo_precio(idProducto);
        if (Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 4).toString()) == 1) {
            txtcantidad.setEnabled(false);
            txtpcompra.setEnabled(false);
        }

    }

    void cargartablaproducto(String Valor) {
        String[] Titulo = {"Id", "Codigo", "Nombre Producto", "Unidad", "pesable"};
        String[] Registros = new String[5];
        String sql = "SELECT idProductos, codigo, nombre, unidad, pesable FROM productos "
                + "WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + Valor + "%' AND materia_prima=0";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProducto = null;
        try {
            SelectProducto = cn.createStatement();
            ResultSet rs = SelectProducto.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                model.addRow(Registros);
            }
            tablaproducto.setModel(model);
            tablaproducto.setAutoCreateRowSorter(true);
            tablaproducto.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setMinWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaproducto.getColumnModel().getColumn(1).setPreferredWidth(30);
            tablaproducto.getColumnModel().getColumn(4).setMaxWidth(0);
            tablaproducto.getColumnModel().getColumn(4).setMinWidth(0);
            tablaproducto.getColumnModel().getColumn(4).setPreferredWidth(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProducto != null) {
                    SelectProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargarTipoIva() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipoiva.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoIVA = null;
        String sSQL = "SELECT * FROM afip_tipoiva";
        try {
            SelectTipoIVA = cn.createStatement();
            ResultSet rs = SelectTipoIVA.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoIva(rs.getInt("idTipoiva"), rs.getString("descripcion")));
            }
            cbotipoiva.setSelectedIndex(2);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoIVA != null) {
                    SelectTipoIVA.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void cargar_ultimo_precio(int valor) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectPrecio = null;
        int bandera = 0;
        String sSQL = "SELECT precioCompra, precioVenta, afip_tipoiva.descripcion,precioVenta2 FROM stock inner join afip_tipoiva on afip_tipoiva.idTipoiva = stock.idTipoiva where idProductos=" + valor + " and precioCompra is not null  order by idstock DESC limit 1";
        try {
            SelectPrecio = cn.createStatement();
            ResultSet rs = SelectPrecio.executeQuery(sSQL);
            while (rs.next()) {

                txtpcompra.setText(rs.getString(1));
                txtpventa.setText(rs.getString(2));
                if (rs.getString(3).equals("21")) {
                    cbotipoiva.setSelectedIndex(4);
                } else {
                    cbotipoiva.setSelectedIndex(2);
                }
                bandera = 1;
            }
            if (bandera == 0) {
                txtpcompra.setText("");
                txtpventa.setText("");
                cbotipoiva.setSelectedIndex(2);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPrecio != null) {
                    SelectPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    /////CARGAR DEPOSITOS/////////////
    void cargardatosdepositos() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDepositos = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbodeposito.setModel(value);

        String sSQL = "SELECT * FROM deposito";
        try {
            SelectDepositos = cn.createStatement();
            ResultSet rs = SelectDepositos.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboDeposito(rs.getInt("idDepositos"), rs.getString("nombre")));
            }
            cbodeposito.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDepositos != null) {
                    SelectDepositos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    /////CARGAR PROVEEDORES/////////////
    void cargardatosproveedor() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProveedores = null;
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedores.setModel(value);

        String sSQL = "SELECT * FROM proveedores";
        try {
            SelectProveedores = cn.createStatement();
            ResultSet rs = SelectProveedores.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboProveedor(rs.getInt("idProveedores"), rs.getString("razonsocial")));
            }
            cboproveedores.setSelectedIndex(0);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu2 = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        txtncbte = new RSMaterialComponent.RSTextFieldMaterial();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        txtcantidad = new RSMaterialComponent.RSTextFieldMaterial();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        cbodeposito = new RSMaterialComponent.RSComboBoxMaterial();
        cbotipoiva = new RSMaterialComponent.RSComboBoxMaterial();
        txtproducto = new RSMaterialComponent.RSTextFieldMaterial();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproducto = new rojerusan.RSTableMetro1();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldMaterial();
        lblunuidad = new RSMaterialComponent.RSTextFieldMaterial();
        txtpventa = new RSMaterialComponent.RSTextFieldMaterial();
        txtpcompra = new RSMaterialComponent.RSTextFieldMaterial();
        cboproveedores = new RSMaterialComponent.RSComboBoxMaterial();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu2.setEditable(false);
        lblMenu2.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu2.setText("Agregar Stock");
        lblMenu2.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu2.setOpaque(false);
        lblMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenu2ActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu2, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 720, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(802, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 820, 20));

        txtncbte.setPlaceholder("Descripción");
        txtncbte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtncbteActionPerformed(evt);
            }
        });
        txtncbte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtncbteKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtncbte, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 360, 370, -1));

        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 420, 141, -1));

        txtcantidad.setPlaceholder("Cantidad");
        txtcantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcantidadActionPerformed(evt);
            }
        });
        txtcantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcantidadKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtcantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 100, 180, -1));

        btnagregar.setText("Aceptar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 420, 141, -1));

        cbodeposito.setForeground(new java.awt.Color(0, 112, 192));
        panelcontenedor.add(cbodeposito, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 230, 170, -1));

        cbotipoiva.setForeground(new java.awt.Color(0, 112, 192));
        panelcontenedor.add(cbotipoiva, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 230, 180, -1));

        txtproducto.setPlaceholder("Producto");
        txtproducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtproductoActionPerformed(evt);
            }
        });
        txtproducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtproductoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtproducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 30, 380, -1));

        tablaproducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaproducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaproductoMouseClicked(evt);
            }
        });
        tablaproducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaproductoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproducto);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 380, 360));

        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_IN);
        panelcontenedor.add(rSLabelIcon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 40, 40));

        txtbuscar.setPlaceholder("Buscar Productos");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 330, 40));

        lblunuidad.setPlaceholder("Unidad");
        lblunuidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblunuidadActionPerformed(evt);
            }
        });
        lblunuidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                lblunuidadKeyReleased(evt);
            }
        });
        panelcontenedor.add(lblunuidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 100, 170, -1));

        txtpventa.setPlaceholder("Precio Venta");
        txtpventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpventaActionPerformed(evt);
            }
        });
        txtpventa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpventaKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtpventa, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 170, 170, -1));

        txtpcompra.setPlaceholder("Precio Compra");
        txtpcompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpcompraActionPerformed(evt);
            }
        });
        txtpcompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpcompraKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtpcompra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 170, 180, -1));

        cboproveedores.setForeground(new java.awt.Color(0, 112, 192));
        panelcontenedor.add(cboproveedores, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 300, 370, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenu2ActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void txtncbteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtncbteActionPerformed

    }//GEN-LAST:event_txtncbteActionPerformed

    private void txtncbteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtncbteKeyReleased

    }//GEN-LAST:event_txtncbteKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        if (mod == 1) {
            txtproducto.setText("");
            txtcantidad.setText("");
            txtpcompra.setText("");
            txtpventa.setText("");
            cbodeposito.setSelectedIndex(0);
            cboproveedores.setSelectedIndex(0);
            txtncbte.setText("");
            cargarTipoIva();
            mod = 0;
            btnsalir.setText("Salir");
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed

    }//GEN-LAST:event_btnsalirKeyPressed

    private void txtcantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcantidadActionPerformed

    private void txtcantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcantidadKeyReleased

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        if (!"".equals(txtproducto.getText()) && !"".equals(txtpventa.getText())) {
            ///////////////////agregar datos//////////////////////////////
            fnCargarFecha fecha = new fnCargarFecha();
            String fechastock = fecha.cargarfecha();
            int cantidad;

            if (Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 4).toString()) == 1) {
                cantidad = 0;
            } else {
                cantidad = Integer.parseInt(txtcantidad.getText());
            }
            cboTipoIva iva = (cboTipoIva) cbotipoiva.getSelectedItem();
            int idTipoiva = iva.getidTipoIva();

            cboDeposito dep = (cboDeposito) cbodeposito.getSelectedItem();
            int idDeposito = dep.getidDeposito();

            cboProveedor pro = (cboProveedor) cboproveedores.getSelectedItem();
            int idProveedor = pro.getidProveedores();
            double pCompra;

            if (txtpcompra.getText().equals("")) {
                pCompra = 0;
            } else {
                pCompra = Double.valueOf(txtpcompra.getText());
            }
            //public int AgregarStock(int cantidad, String fechastock, String pcompra, String pventa,String pventa2, int idProducto, int idDeposito, int idTipoiva, int idProveedor, String nComprobante) {
            ClaseStock stock = new ClaseStock();
            if (stock.AgregarStock(cantidad, fechastock, pCompra, txtpventa.getText(), idProducto, idDeposito, idTipoiva, idProveedor, txtncbte.getText()) == 1) {
                txtproducto.setText("");
                txtcantidad.setText("");
                txtpcompra.setText("");
                txtpventa.setText("");
                cbodeposito.setSelectedIndex(0);
                cboproveedores.setSelectedIndex(0);
                txtncbte.setText("");
                cargarTipoIva();

            }
        } else {
            JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed

    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartablaproducto(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtproductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtproductoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtproductoKeyReleased

    private void txtproductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtproductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtproductoActionPerformed

    private void lblunuidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblunuidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblunuidadActionPerformed

    private void lblunuidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lblunuidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_lblunuidadKeyReleased

    private void txtpventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpventaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpventaActionPerformed

    private void txtpventaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpventaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpventaKeyReleased

    private void txtpcompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpcompraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpcompraActionPerformed

    private void txtpcompraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpcompraKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpcompraKeyReleased

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtbuscar.transferFocus();
        }
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void tablaproductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            cargardatos();
        }
    }//GEN-LAST:event_tablaproductoKeyReleased

    private void tablaproductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaproductoMouseClicked
        if (evt.getClickCount() == 2) {
            cargardatos();
        }
    }//GEN-LAST:event_tablaproductoMouseClicked

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private RSMaterialComponent.RSComboBoxMaterial cbodeposito;
    private RSMaterialComponent.RSComboBoxMaterial cboproveedores;
    private RSMaterialComponent.RSComboBoxMaterial cbotipoiva;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu2;
    private RSMaterialComponent.RSTextFieldMaterial lblunuidad;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSTableMetro1 tablaproducto;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscar;
    private RSMaterialComponent.RSTextFieldMaterial txtcantidad;
    private RSMaterialComponent.RSTextFieldMaterial txtncbte;
    private RSMaterialComponent.RSTextFieldMaterial txtpcompra;
    private RSMaterialComponent.RSTextFieldMaterial txtproducto;
    private RSMaterialComponent.RSTextFieldMaterial txtpventa;
    // End of variables declaration//GEN-END:variables
}
