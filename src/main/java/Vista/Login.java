/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame {

    public static int id_usuario;
    public static String apellido;
    public static String nombre;
    public static String usuario;
    public static String contraseña;
    public static String acceso_datos;
    public static String acceso_ventas;
    public static String acceso_consultas;
    public static String acceso_afip;

    public Login() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        txtusuarios.requestFocus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new necesario.Panel();
        jPanel1 = new javax.swing.JPanel();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        jPanel2 = new javax.swing.JPanel();
        txtusuarios = new RSMaterialComponent.RSTextFieldMaterial();
        txtcontraseña = new RSMaterialComponent.RSPasswordMaterial();
        rSButtonMaterialIconOne1 = new RSMaterialComponent.RSButtonMaterialIconOne();
        PanelMenu = new necesario.Panel();
        btncerrar = new RSMaterialComponent.RSButtonIconUno();
        lblMenu = new rojeru_san.rsfield.RSTextField();

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 122, 204));

        rSLabelIcon1.setForeground(new java.awt.Color(255, 255, 255));
        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ACCOUNT_CIRCLE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(73, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, -1, 210));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        txtusuarios.setPlaceholder("Usuario");
        txtusuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtusuariosActionPerformed(evt);
            }
        });

        txtcontraseña.setPlaceholder("Contraseña");
        txtcontraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcontraseñaActionPerformed(evt);
            }
        });

        rSButtonMaterialIconOne1.setText("Aceptar");
        rSButtonMaterialIconOne1.setBackgroundHover(new java.awt.Color(102, 102, 102));
        rSButtonMaterialIconOne1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rSButtonMaterialIconOne1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        rSButtonMaterialIconOne1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rSButtonMaterialIconOne1ActionPerformed(evt);
            }
        });
        rSButtonMaterialIconOne1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rSButtonMaterialIconOne1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtusuarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(rSButtonMaterialIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(txtusuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtcontraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rSButtonMaterialIconOne1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 310, 210));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarActionPerformed(evt);
            }
        });

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Login");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addComponent(lblMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelMenuLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                    .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        getContentPane().add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 500, 22));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtusuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtusuariosActionPerformed
        txtusuarios.transferFocus();
    }//GEN-LAST:event_txtusuariosActionPerformed

    private void txtcontraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcontraseñaActionPerformed
        int i = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;

        String sSQL = "SELECT * FROM usuarios ";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            while (rs.next()) {
                if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                    if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                        this.dispose();
                        i = 0;
                        // Cargo las variables para usarlos en el Main
                        id_usuario = rs.getInt("idUsuarios");
                        apellido = rs.getString("apellido");
                        nombre = rs.getString("nombre");
                        usuario = rs.getString("usuario");
                        contraseña = rs.getString("contraseña");
                        //////
                        acceso_datos = rs.getString("acceso_datos");
                        acceso_ventas = rs.getString("acceso_ventas");
                        acceso_consultas = rs.getString("acceso_consultas");
                        acceso_afip = rs.getString("acceso_afip");
                        /////
                        new Main().setVisible(true);
                        break;
                    } else {
                        i = 2;
                    }
                } else {
                    i = 1;
                }
            }
            if (i == 1) {
                JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
            }
            if (i == 2) {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_txtcontraseñaActionPerformed

    private void rSButtonMaterialIconOne1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rSButtonMaterialIconOne1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int i = 0;
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            Statement SelectUsuarios = null;

            String sSQL = "SELECT * FROM usuarios ";
            try {
                SelectUsuarios = cn.createStatement();
                ResultSet rs = SelectUsuarios.executeQuery(sSQL);
                // Recorro y me fijo donde coinciden usuario y contraseña
                while (rs.next()) {
                    if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                        if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                            this.dispose();
                            i = 0;
                            // Cargo las variables para usarlos en el Main
                            id_usuario = rs.getInt("idUsuarios");
                            apellido = rs.getString("apellido");
                            nombre = rs.getString("nombre");
                            usuario = rs.getString("usuario");
                            contraseña = rs.getString("contraseña");
                            //////
                            acceso_datos = rs.getString("acceso_datos");
                            acceso_ventas = rs.getString("acceso_ventas");
                            acceso_consultas = rs.getString("acceso_consultas");
                            acceso_afip = rs.getString("acceso_afip");
                            /////
                            new Main().setVisible(true);
                            break;
                        } else {
                            i = 2;
                        }
                    } else {
                        i = 1;
                    }
                }
                if (i == 1) {
                    JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
                }
                if (i == 2) {
                    JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, e);
            } finally {
                try {
                    if (SelectUsuarios != null) {
                        SelectUsuarios.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    }//GEN-LAST:event_rSButtonMaterialIconOne1KeyPressed

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btncerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btncerrarActionPerformed

    private void rSButtonMaterialIconOne1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonMaterialIconOne1ActionPerformed
         int i = 0;
            ConexionMySQL mysql = new ConexionMySQL();
            Connection cn = mysql.Conectar();
            Statement SelectUsuarios = null;

            String sSQL = "SELECT * FROM usuarios ";
            try {
                SelectUsuarios = cn.createStatement();
                ResultSet rs = SelectUsuarios.executeQuery(sSQL);
                // Recorro y me fijo donde coinciden usuario y contraseña
                while (rs.next()) {
                    if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                        if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                            this.dispose();
                            i = 0;
                            // Cargo las variables para usarlos en el Main
                            id_usuario = rs.getInt("idUsuarios");
                            apellido = rs.getString("apellido");
                            nombre = rs.getString("nombre");
                            usuario = rs.getString("usuario");
                            contraseña = rs.getString("contraseña");
                            //////
                            acceso_datos = rs.getString("acceso_datos");
                            acceso_ventas = rs.getString("acceso_ventas");
                            acceso_consultas = rs.getString("acceso_consultas");
                            acceso_afip = rs.getString("acceso_afip");
                            /////
                            new Main().setVisible(true);
                            break;
                        } else {
                            i = 2;
                        }
                    } else {
                        i = 1;
                    }
                }
                if (i == 1) {
                    JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
                }
                if (i == 2) {
                    JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, e);
            } finally {
                try {
                    if (SelectUsuarios != null) {
                        SelectUsuarios.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
    }//GEN-LAST:event_rSButtonMaterialIconOne1ActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonIconUno btncerrar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private necesario.Panel panel1;
    private RSMaterialComponent.RSButtonMaterialIconOne rSButtonMaterialIconOne1;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private RSMaterialComponent.RSPasswordMaterial txtcontraseña;
    private RSMaterialComponent.RSTextFieldMaterial txtusuarios;
    // End of variables declaration//GEN-END:variables
}
