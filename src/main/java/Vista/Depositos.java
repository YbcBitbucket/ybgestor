/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnEscape;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Ybc
 */
public class Depositos extends javax.swing.JDialog {

    int elim = 0;
    static DefaultListModel modeloLista = new DefaultListModel();  //Atributo de clase

    public Depositos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cargarlista("");
        initComponents();
        listacategoria.setModel(modeloLista);
        fnEscape.funcionescape(this);
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocation(500,200);
        this.setResizable(false);
    }

    void cargarlista(String valor) {
        String Registros = "";
        String sql = "SELECT nombre FROM deposito WHERE nombre LIKE '%" + valor + "%'";
        modeloLista.clear();
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectDeposito = null;
        try {
            SelectDeposito = cn.createStatement();
            ResultSet rs = SelectDeposito.executeQuery(sql);
            while (rs.next()) {
                Registros = rs.getString("nombre");
                modeloLista.addElement(Registros);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDeposito != null) {
                    SelectDeposito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu2 = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        txtnombre = new RSMaterialComponent.RSTextFieldMaterial();
        jScrollPane1 = new javax.swing.JScrollPane();
        listacategoria = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu2.setEditable(false);
        lblMenu2.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu2.setText("Agregar Deposito");
        lblMenu2.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu2.setOpaque(false);
        lblMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenu2ActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu2, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 590, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(592, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 610, 20));

        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 110, 141, -1));

        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 110, 141, -1));

        txtnombre.setPlaceholder("Deposito");
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 30, 300, -1));

        listacategoria.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        listacategoria.setSelectionBackground(new java.awt.Color(0, 112, 192));
        listacategoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listacategoriaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(listacategoria);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 270, 120));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        String texto = txtnombre.getText().toUpperCase();
        txtnombre.setText(texto);
        cargarlista(txtnombre.getText());
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        String nom;
        int agrega = 0;
        String sSQL = "";
        if ("".equals(txtnombre.getText())) {
            JOptionPane.showMessageDialog(null, "No ingreso ningun dato");
        } else {
            nom = txtnombre.getText();
            String sql = "SELECT nombre FROM deposito";
            Statement SelectDeposito = null;
            try {
                SelectDeposito = cn.createStatement();
                ResultSet rsr = SelectDeposito.executeQuery(sql);
                int contador = 0; //inicio
                while (rsr.next()) {
                    contador++;
                }
                if (contador != 0) {
                    rsr.beforeFirst();
                    while (rsr.next()) {
                        if (nom.equals(rsr.getString("nombre"))) {
                            JOptionPane.showMessageDialog(null, "El Deposito ya esta en la base de datos");
                            agrega = 0;
                            break;
                        } else {
                            agrega = 1;
                        }
                    }
                } else {
                    agrega = 1;
                }
                if (agrega == 1) {
                    sSQL = "INSERT INTO deposito(nombre) "
                            + "VALUES(?)";
                    PreparedStatement AgregarDepositos = null;
                    try {
                        AgregarDepositos = cn.prepareStatement(sSQL);
                        AgregarDepositos.setString(1, nom);
                        AgregarDepositos.executeUpdate();
                        cargarlista("");
                        txtnombre.setText("");
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                        JOptionPane.showMessageDialog(null, ex);
                    } finally {
                        try {
                            if (AgregarDepositos != null) {
                                AgregarDepositos.close();
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, ex);
                        }
                    }
                }
                cargarlista("");
                txtnombre.setText("");
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                JOptionPane.showMessageDialog(null, e);
            } finally {
                try {
                    if (SelectDeposito != null) {
                        SelectDeposito.close();
                    }
                    if (cn != null) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed

    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void lblMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenu2ActionPerformed

    private void listacategoriaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listacategoriaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (elim == 0) {
                int filasel;
                filasel = listacategoria.getSelectedIndex();
                if (filasel == -1) {
                    JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                } else {
                    int opcion = JOptionPane.showConfirmDialog(this, "Desea eliminar el Deposito?", "Mensaje", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        ConexionMySQL mysql = new ConexionMySQL();
                        Connection cn = mysql.Conectar();
                        Statement SelectDepositos = null;
                        Statement SelectDepositoStock = null;
                        try {
                            //////////////////consulta productos//////////////////////////////
                            String sSQL = "";
                            int idDeposito = 0;
                            String nombre = listacategoria.getSelectedValue().toString();
                            int eliminaP = 0;
                            String sqlid = "SELECT idDepositos FROM deposito WHERE nombre='" + nombre + "'";

                            JOptionPane.showMessageDialog(null, nombre);
                            SelectDepositos = cn.createStatement();
                            ResultSet rss = SelectDepositos.executeQuery(sqlid);
                            while (rss.next()) {
                                idDeposito = rss.getInt("idDepositos");
                            }

                            String sql = "SELECT idDepositos FROM stock WHERE idDepositos=" + idDeposito + "";
                            SelectDepositoStock = cn.createStatement();
                            ResultSet rs = SelectDepositoStock.executeQuery(sql);
                            if (rs.next()) {
                                JOptionPane.showMessageDialog(null, "El Deposito no puede ser eliminado." + "\n" + "Pertenece a un stock existente." + "\n");
                                eliminaP = 0;

                            } else {
                                eliminaP = 1;
                            }

                            /////////////////elimina/////////////////////////////////////////////////////////
                            if (eliminaP == 1) {
                                PreparedStatement pst = cn.prepareStatement("DELETE FROM deposito WHERE idDepositos ='" + idDeposito + "'");
                                pst.execute();
                                JOptionPane.showMessageDialog(null, "El Deposito fué eliminada.");
                                cargarlista("");
                                txtnombre.setText("");
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                            JOptionPane.showMessageDialog(null, e);
                        } finally {
                            try {
                                if (SelectDepositos != null) {
                                    SelectDepositos.close();
                                }
                                if (SelectDepositoStock != null) {
                                    SelectDepositoStock.close();
                                }
                                if (cn != null) {
                                    cn.close();
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                    }
                }
            } else {
                this.dispose();
            }
        }
    }//GEN-LAST:event_listacategoriaKeyPressed

    private void txtnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnagregar.doClick();
        }
    }//GEN-LAST:event_txtnombreKeyPressed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu2;
    private javax.swing.JList listacategoria;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSTextFieldMaterial txtnombre;
    // End of variables declaration//GEN-END:variables
}
