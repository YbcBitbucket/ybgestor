/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.*;

/**
 *
 * @author LUKS1
 */
public class ProductosDetalle extends javax.swing.JDialog {

   String id = Productos.idProducto;
    int id_cat, id_mar;
    
    public ProductosDetalle(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocation(600,110);
        this.setResizable(false);
        cargardatoscategoria();
        cargardatosmarca();
        cargardatos(id);
        txtcodigo.setEditable(false);
    }

   //////////////////////FUNCION CARGAR CATEGORIA //////////////////////
    void cargardatoscategoria() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCategoria = null;
        String sSQL = "SELECT nombre FROM categorias";
        cbocategoria.removeAllItems();
        cbocategoria.addItem("...");
        try {
            SelectCategoria = cn.createStatement();
            ResultSet rs = SelectCategoria.executeQuery(sSQL);
            while (rs.next()) {
                cbocategoria.addItem(rs.getString("nombre"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCategoria != null) {
                    SelectCategoria.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    
    //////CARGAR MARCA/////////////
    void cargardatosmarca() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectMarca = null;
        String sSQL = "SELECT nombre FROM marcas";
        cbomarca.removeAllItems();
        cbomarca.addItem("...");
        try {
            SelectMarca = cn.createStatement();
            ResultSet rs = SelectMarca.executeQuery(sSQL);
            while (rs.next()) {
                cbomarca.addItem(rs.getString("nombre"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarca != null) {
                    SelectMarca.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    /////////////////////CARGAR DATOS///////////////////////////
    void cargardatos(String id) {
        String sSQL = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProductos = null;
        sSQL = "SELECT * FROM vista_productos WHERE idProductos=" + id;
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sSQL);
            while (rs.next()) {
                txtnombre.setText(rs.getString("nombre"));
                cbocategoria.setSelectedItem(rs.getString("categoria"));
                cbomarca.setSelectedItem(rs.getString("marca"));
                txtcodigo.setText(rs.getString("codigo"));
                txtunidad.setText(rs.getString("unidad"));
                txtminimo.setText(rs.getString("minimo"));
                txtdescripcion.setText(rs.getString("descripcion"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        txtdescripcion = new RSMaterialComponent.RSTextFieldMaterial();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        txtcodigo = new RSMaterialComponent.RSTextFieldMaterial();
        txtnombre = new RSMaterialComponent.RSTextFieldMaterial();
        txtminimo = new RSMaterialComponent.RSTextFieldMaterial();
        radioMateriaPrimaNo = new RSMaterialComponent.RSRadioButtonMaterial();
        radioMateriaPrimaSi = new RSMaterialComponent.RSRadioButtonMaterial();
        lblSubtotal = new rojeru_san.rsfield.RSTextField();
        lblSubtotal1 = new rojeru_san.rsfield.RSTextField();
        radioPesableSi = new RSMaterialComponent.RSRadioButtonMaterial();
        radioPesableNo = new RSMaterialComponent.RSRadioButtonMaterial();
        txtunidad = new RSMaterialComponent.RSTextFieldMaterial();
        cbocategoria = new RSMaterialComponent.RSComboBoxMaterial();
        cbomarca = new RSMaterialComponent.RSComboBoxMaterial();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Detalle Productos");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 340, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(342, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 20));

        txtdescripcion.setEnabled(false);
        txtdescripcion.setPlaceholder("Descripción");
        txtdescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescripcionActionPerformed(evt);
            }
        });
        txtdescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdescripcionKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtdescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 450, 320, -1));

        btnsalir.setText("Cancelar");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 510, 141, -1));

        txtcodigo.setEnabled(false);
        txtcodigo.setPlaceholder("Codigo");
        txtcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodigoActionPerformed(evt);
            }
        });
        txtcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcodigoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 310, -1));

        txtnombre.setEnabled(false);
        txtnombre.setPlaceholder("Nombre");
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 310, -1));

        txtminimo.setEnabled(false);
        txtminimo.setPlaceholder("Mínimo");
        txtminimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtminimoActionPerformed(evt);
            }
        });
        txtminimo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtminimoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtminimo, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 390, 150, -1));

        radioMateriaPrimaNo.setText("No");
        radioMateriaPrimaNo.setEnabled(false);
        panelcontenedor.add(radioMateriaPrimaNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 270, 60, -1));

        radioMateriaPrimaSi.setText("Si");
        radioMateriaPrimaSi.setEnabled(false);
        panelcontenedor.add(radioMateriaPrimaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 270, 60, 40));

        lblSubtotal.setEditable(false);
        lblSubtotal.setBackground(new java.awt.Color(255, 255, 255));
        lblSubtotal.setText("Materia prima:");
        lblSubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblSubtotalActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblSubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, 140, 40));

        lblSubtotal1.setEditable(false);
        lblSubtotal1.setBackground(new java.awt.Color(255, 255, 255));
        lblSubtotal1.setText("Descecontar de materia prima:");
        lblSubtotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblSubtotal1ActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblSubtotal1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 210, 40));

        radioPesableSi.setText("Si");
        radioPesableSi.setEnabled(false);
        panelcontenedor.add(radioPesableSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 330, 50, 40));

        radioPesableNo.setText("No");
        radioPesableNo.setEnabled(false);
        panelcontenedor.add(radioPesableNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 330, 60, -1));

        txtunidad.setEnabled(false);
        txtunidad.setPlaceholder("Unidad");
        txtunidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtunidadActionPerformed(evt);
            }
        });
        txtunidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtunidadKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtunidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, 130, -1));

        cbocategoria.setForeground(new java.awt.Color(0, 112, 192));
        cbocategoria.setEnabled(false);
        cbocategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriaActionPerformed(evt);
            }
        });
        panelcontenedor.add(cbocategoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 310, -1));

        cbomarca.setForeground(new java.awt.Color(0, 112, 192));
        cbomarca.setEnabled(false);
        cbomarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomarcaActionPerformed(evt);
            }
        });
        panelcontenedor.add(cbomarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 310, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 566, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void txtdescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescripcionActionPerformed

    }//GEN-LAST:event_txtdescripcionActionPerformed

    private void txtdescripcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdescripcionKeyReleased

    }//GEN-LAST:event_txtdescripcionKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed

    }//GEN-LAST:event_btnsalirKeyPressed

    private void txtcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoActionPerformed

    private void txtcodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtminimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtminimoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtminimoActionPerformed

    private void txtminimoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtminimoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtminimoKeyReleased

    private void lblSubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblSubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblSubtotalActionPerformed

    private void lblSubtotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblSubtotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblSubtotal1ActionPerformed

    private void txtunidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtunidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtunidadActionPerformed

    private void txtunidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtunidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtunidadKeyReleased

    private void cbocategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriaActionPerformed
        if (cbocategoria.getSelectedItem() != null) {
            if (!cbocategoria.getSelectedItem().toString().equals("...")) {
                ConexionMySQL mysql = new ConexionMySQL();
                Connection cn = mysql.Conectar();
                Statement SelectCategoria = null;
                String sql = "SELECT idCategorias, nombre FROM categorias";
                try {
                    SelectCategoria = cn.createStatement();
                    ResultSet rs = SelectCategoria.executeQuery(sql);
                    while (rs.next()) {
                        if (rs.getString("nombre").equals(cbocategoria.getSelectedItem().toString())) {
                            id_cat = rs.getInt("idCategorias");
                            rs.last();
                        }
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    JOptionPane.showMessageDialog(null, ex);
                } finally {
                    try {
                        if (SelectCategoria != null) {
                            SelectCategoria.close();
                        }
                        if (cn != null) {
                            cn.close();
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_cbocategoriaActionPerformed

    private void cbomarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomarcaActionPerformed
        if (cbomarca.getSelectedItem() != null) {
            if (!cbomarca.getSelectedItem().toString().equals("...")) {
                ConexionMySQL mysql = new ConexionMySQL();
                Connection cn = mysql.Conectar();
                Statement SelectMarca = null;
                String sql = "SELECT idMarcas, nombre FROM marcas";
                try {
                    SelectMarca = cn.createStatement();
                    ResultSet rs = SelectMarca.executeQuery(sql);
                    while (rs.next()) {
                        if (rs.getString("nombre").equals(cbomarca.getSelectedItem().toString())) {
                            id_mar = rs.getInt("idMarcas");
                            rs.last();
                        }
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en la base de datos...");
                    JOptionPane.showMessageDialog(null, ex);
                } finally {
                    try {
                        if (SelectMarca != null) {
                            SelectMarca.close();
                        }
                        if (cn != null) {
                            cn.close();
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_cbomarcaActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private RSMaterialComponent.RSComboBoxMaterial cbocategoria;
    private RSMaterialComponent.RSComboBoxMaterial cbomarca;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private rojeru_san.rsfield.RSTextField lblSubtotal;
    private rojeru_san.rsfield.RSTextField lblSubtotal1;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSRadioButtonMaterial radioMateriaPrimaNo;
    private RSMaterialComponent.RSRadioButtonMaterial radioMateriaPrimaSi;
    private RSMaterialComponent.RSRadioButtonMaterial radioPesableNo;
    private RSMaterialComponent.RSRadioButtonMaterial radioPesableSi;
    private RSMaterialComponent.RSTextFieldMaterial txtcodigo;
    private RSMaterialComponent.RSTextFieldMaterial txtdescripcion;
    private RSMaterialComponent.RSTextFieldMaterial txtminimo;
    private RSMaterialComponent.RSTextFieldMaterial txtnombre;
    private RSMaterialComponent.RSTextFieldMaterial txtunidad;
    // End of variables declaration//GEN-END:variables
}
