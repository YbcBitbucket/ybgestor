/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnAlinear;
import Controlador.fnEditarCeldas;
import Controlador.fnExportar;
import Controlador.fnRedondear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LUKS1
 */
public class Promociones extends javax.swing.JDialog {

    DefaultTableModel model;
    public static int idpromociones, idDeposito;

    public Promociones(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocation(450, 100);
        this.setResizable(false);
        /// cargardepositos();
        cargartablabuscar("");
        dobleclick();
    }

    ///// CARGAR TABLA BUSCAR /////
    void cargartablabuscar(String valor) {
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IVA", "Estado"};
        String[] Registros = new String[7];
        String sql = "SELECT * FROM promociones WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si el estado de las promociones
                if (rs.getInt(6) != 0) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getString(4);
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(7);
                    Registros[6] = rs.getString(8);
                    model.addRow(Registros);
                }
            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(0);
            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(170);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(55);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(30);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    ///// FILTRAR TABLA BUSCAR DEPOSITOS /////
    void filtrartablabuscardep(int codigo) {
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal"};
        String[] Registros = new String[9];
        String sql = "SELECT * FROM vista_producto_buscar WHERE idDepositos2=" + codigo + "";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                if (rs.getInt(4) != 0) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getString(4);
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(6);
                    Registros[6] = rs.getString(7);
                    ////////////////////////
                    Registros[7] = rs.getString(10);
                    fnRedondear redondear = new fnRedondear();
                    Registros[8] = String.valueOf(redondear.UNDigitos(rs.getDouble(17)));
                    //Registros[8] = String.valueOf(redondear.dosDigitos(rs2.getDouble(2) * (1 - (rs2.getDouble(3) / 100)) * (1 + (rs2.getDouble(4) / 100))));
                    model.addRow(Registros);
                }
            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(0);

            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(170);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(55);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(40);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(60);
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(new fnEditarCeldas());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablabuscar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablabuscar.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idpromociones = Integer.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString());
                        new PromocionesModifica(null, true).setVisible(true);
                        cargartablabuscar("");
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldMaterial();
        btnmodificar = new RSMaterialComponent.RSButtonMaterialIconOne();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablabuscar = new rojerusan.RSTableMetro1();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btndetalle = new RSMaterialComponent.RSButtonMaterialIconOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(710, 496));
        setMinimumSize(new java.awt.Dimension(710, 496));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(710, 496));

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setMaximumSize(new java.awt.Dimension(710, 496));
        panelcontenedor.setMinimumSize(new java.awt.Dimension(710, 496));
        panelcontenedor.setPreferredSize(new java.awt.Dimension(710, 496));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Consulta de Promociones");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 690, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(692, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 710, 20));

        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_IN);
        panelcontenedor.add(rSLabelIcon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 40, 40));

        txtbuscar.setPlaceholder("Buscar Promos");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 220, -1));

        btnmodificar.setText("Modificar");
        btnmodificar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnmodificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnmodificar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.UPDATE);
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });
        btnmodificar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnmodificarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnmodificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 450, 141, -1));

        tablabuscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablabuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablabuscarMouseClicked(evt);
            }
        });
        tablabuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablabuscarKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablabuscar);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 690, 352));

        btnsalir.setText("Volver");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.HOME);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 450, 141, -1));

        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 141, -1));

        btndetalle.setText("Exportar");
        btndetalle.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btndetalle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btndetalle.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_OUT_MAP);
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });
        btndetalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btndetalleKeyPressed(evt);
            }
        });
        panelcontenedor.add(btndetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 450, 141, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        int fila = tablabuscar.getRowCount();
        if (fila != 0) {
            txtbuscar.transferFocus();
            tablabuscar.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartablabuscar(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        int filasel = tablabuscar.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idpromociones = Integer.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 0).toString());
            new PromocionesModifica(null, true).setVisible(true);
            cargartablabuscar("");
            
            
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void btnmodificarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnmodificarKeyPressed

    }//GEN-LAST:event_btnmodificarKeyPressed

    private void tablabuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablabuscarMouseClicked
        if (evt.getClickCount() == 2) {
            btnmodificar.doClick();
        }
    }//GEN-LAST:event_tablabuscarMouseClicked

    private void tablabuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablabuscarKeyPressed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
new PromocionesAgrega(null, true).setVisible(true);        
        cargartablabuscar("");
        

    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        if (this.tablabuscar.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablabuscar);
            nom.add("Tabla Promociones");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Controlador.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btndetalleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btndetalleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndetalleKeyPressed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btndetalle;
    private RSMaterialComponent.RSButtonMaterialIconOne btnmodificar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private necesario.Panel panelcontenedor;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSTableMetro1 tablabuscar;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscar;
    // End of variables declaration//GEN-END:variables
}
