/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnCargarFecha;
import Controlador.fnEscape;
import Controlador.fnRedondear;
import Controlador.fnesNumerico;
import Modelo.ClaseStock;
import static Vista.Main.bcant;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LUKS1
 */
public class PromocionesAgrega extends javax.swing.JDialog {

    DefaultTableModel model;
    int mod = 0, idusuarios, idProducto;
    String fechastock;

    public PromocionesAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
       this.setLocation(450, 40);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        cargartablaproducto("");
        //dobleclick();
        dobleclickagregaproducto();
        //cargarTipoIva();
    }

    void dobleclickagregaproducto() {
        tablaproducto.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    System.out.println("entra");

                    new Cantidad(null, true).setVisible(true);
                    int bandera = 0;
                    DefaultTableModel temp = (DefaultTableModel) tablaproducto2.getModel();
                    if (tablaproducto.getSelectedRow() == -1) {
                        JOptionPane.showMessageDialog(null, "No seleccionó ninguna fila");
                    } else {
                        if (bcant != 0) {
                            idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
                            //txtproducto.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());                            
                            //double precio = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 3).toString());
                            //double cant = Double.valueOf(bcant);
                            //double bonif = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 4).toString());
                            //double iva = Double.valueOf(tablabuscar.getValueAt((tablabuscar.getSelectedRow()), 6).toString());
                            fnRedondear redondear = new fnRedondear();
                            /// System.out.println("tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString() " + tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 10).toString());
                            Object nuevo[] = {
                                tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString(),
                                tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 1).toString(),
                                tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString(),
                                bcant,
                                cargar_ultimo_precio(idProducto)};
                            System.out.println("sale.... " + bcant);
                            temp.addRow(nuevo);
                            txtbuscar.setText("");
                            txtbuscar.requestFocus();
                            cargartotales();
                        } else {
                            JOptionPane.showMessageDialog(null, "No ingreso la cantidad");
                        }

                    }
                }
            }
        }
        );
    }

    void cargartotales() {
        double total = 0.00, sumatoria = 0.0, subtotal = 0.0;
        int totalRow = tablaproducto2.getRowCount();
        totalRow -= 1;
        fnRedondear redondear = new fnRedondear();
        double totalbase = 0.00, sumatoriabase = 0.0;
        for (int i = 0; i <= (totalRow); i++) {
            sumatoriabase = redondear.dosDigitos(Double.valueOf(tablaproducto2.getValueAt(i, 3).toString()) * Double.valueOf(tablaproducto2.getValueAt(i, 4).toString()));
            totalbase = totalbase + sumatoriabase;
        }
        txtprecio.setText(String.valueOf(totalbase));
    }

    void cargartablaproducto(String Valor) {
        String[] Titulo = {"Id", "Codigo", "Nombre Producto"};
        String[] Registros = new String[3];
        String sql = "SELECT idProductos, codigo, nombre FROM productos "
                + "WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + Valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProducto = null;
        try {
            SelectProducto = cn.createStatement();
            ResultSet rs = SelectProducto.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                model.addRow(Registros);
            }
            tablaproducto.setModel(model);
            tablaproducto.setAutoCreateRowSorter(true);
            tablaproducto.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setMinWidth(0);
            tablaproducto.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaproducto.getColumnModel().getColumn(1).setPreferredWidth(30);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProducto != null) {
                    SelectProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    /* void cargarTipoIva() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipoiva.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoIVA = null;
        String sSQL = "SELECT * FROM afip_tipoiva";
        try {
            SelectTipoIVA = cn.createStatement();
            ResultSet rs = SelectTipoIVA.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoIva(rs.getInt("idTipoiva"), rs.getString("descripcion")));
            }
            cbotipoiva.setSelectedIndex(2);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoIVA != null) {
                    SelectTipoIVA.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }

    }*/

 /*void dobleclick() {
        tablaproducto.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
                    txtproducto.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());
                    cargar_ultimo_precio(idProducto);

                }
            }
        });
    }*/
    String cargar_ultimo_precio(int valor) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectPrecio = null;
        int bandera = 0;
        String Precio_Venta = "0.0";
        String sSQL = "SELECT precioCompra, precioVenta, afip_tipoiva.descripcion FROM stock inner join afip_tipoiva on afip_tipoiva.idTipoiva = stock.idTipoiva where idProductos=" + valor + " and precioCompra is not null  order by idstock DESC limit 1";
        try {
            SelectPrecio = cn.createStatement();
            ResultSet rs = SelectPrecio.executeQuery(sSQL);
            while (rs.next()) {

                //txtpcompra.setText(rs.getString(1));
                Precio_Venta = rs.getString(2);
                /*if (rs.getString(3).equals("21")) {
                    cbotipoiva.setSelectedIndex(4);
                } else {
                    cbotipoiva.setSelectedIndex(2);
                }*/

                bandera = 1;
            }
            /* if (bandera == 0) {
                txtpcompra.setText("");
                txtpventa.setText("");
                cbotipoiva.setSelectedIndex(2);
            }*/
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPrecio != null) {
                    SelectPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return Precio_Venta;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelContenedor = new necesario.Panel();
        txtbuscar = new RSMaterialComponent.RSTextFieldMaterial();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproducto = new rojerusan.RSTableMetro1();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaproducto2 = new rojerusan.RSTableMetro1();
        txtcodigo = new RSMaterialComponent.RSTextFieldMaterial();
        txtnombre = new RSMaterialComponent.RSTextFieldMaterial();
        txtdescuento = new RSMaterialComponent.RSTextFieldMaterial();
        txtprecio = new RSMaterialComponent.RSTextFieldMaterial();
        txtpreciototal = new RSMaterialComponent.RSTextFieldMaterial();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btncancelar = new RSMaterialComponent.RSButtonMaterialIconOne();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(800, 630));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PanelContenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        PanelContenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        PanelContenedor.setMaximumSize(new java.awt.Dimension(800, 630));
        PanelContenedor.setMinimumSize(new java.awt.Dimension(800, 630));

        txtbuscar.setPlaceholder("Buscar Productos");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Crear una Promoción");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addComponent(lblMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btncerrar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMenu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_IN);

        tablaproducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaproducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaproductoMouseClicked(evt);
            }
        });
        tablaproducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaproductoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaproductoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproducto);

        tablaproducto2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Codigo", "Nombre", "Cantidad", "Precio"
            }
        ));
        jScrollPane2.setViewportView(tablaproducto2);

        txtcodigo.setPlaceholder("Codigo Promoción");
        txtcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodigoActionPerformed(evt);
            }
        });
        txtcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcodigoKeyReleased(evt);
            }
        });

        txtnombre.setPlaceholder("Nombre Promoción");
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
        });

        txtdescuento.setPlaceholder("Bonificación");
        txtdescuento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtdescuentoFocusGained(evt);
            }
        });
        txtdescuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdescuentoActionPerformed(evt);
            }
        });
        txtdescuento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdescuentoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdescuentoKeyReleased(evt);
            }
        });

        txtprecio.setPlaceholder("Precio Normal");
        txtprecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtprecioActionPerformed(evt);
            }
        });
        txtprecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprecioKeyReleased(evt);
            }
        });

        txtpreciototal.setPlaceholder("Precio Promoción");
        txtpreciototal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpreciototalActionPerformed(evt);
            }
        });
        txtpreciototal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpreciototalKeyReleased(evt);
            }
        });

        btnagregar.setText("Aceptar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CHECK);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });

        btncancelar.setText("Salir");
        btncancelar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btncancelar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncancelar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });
        btncancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btncancelarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout PanelContenedorLayout = new javax.swing.GroupLayout(PanelContenedor);
        PanelContenedor.setLayout(PanelContenedorLayout);
        PanelContenedorLayout.setHorizontalGroup(
            PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PanelContenedorLayout.createSequentialGroup()
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelContenedorLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelContenedorLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelContenedorLayout.createSequentialGroup()
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelContenedorLayout.createSequentialGroup()
                                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtcodigo, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                                    .addComponent(txtnombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtprecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtdescuento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtpreciototal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(12, 12, 12)))
                .addContainerGap())
        );
        PanelContenedorLayout.setVerticalGroup(
            PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelContenedorLayout.createSequentialGroup()
                .addComponent(PanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rSLabelIcon1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PanelContenedorLayout.createSequentialGroup()
                        .addComponent(txtcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtdescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtpreciototal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(PanelContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getContentPane().add(PanelContenedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 630));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed

    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartablaproducto(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void tablaproductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaproductoMouseClicked

    }//GEN-LAST:event_tablaproductoMouseClicked

    private void tablaproductoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaproductoKeyPressed

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void txtcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoActionPerformed

    private void txtcodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtdescuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdescuentoActionPerformed
        double pc = 0.0;
        double bon = 0.0;
        double pv = 0.0;

        pc = Double.valueOf(txtprecio.getText());
        bon = Double.valueOf(txtdescuento.getText());
        pv = pc - ((bon / 100) * pc);
        fnRedondear r = new fnRedondear();
        txtpreciototal.setText(Double.toString(r.dosDigitos(pv)));
    }//GEN-LAST:event_txtdescuentoActionPerformed

    private void txtdescuentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdescuentoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdescuentoKeyReleased

    private void txtprecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtprecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtprecioActionPerformed

    private void txtprecioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprecioKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtprecio.getText())) {
            txtprecio.setText("");
        }
    }//GEN-LAST:event_txtprecioKeyReleased

    private void txtpreciototalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpreciototalActionPerformed
        double pc = 0.0;
        double bon = 0.0;
        double pv = 0.0;

        pv = Double.valueOf(txtpreciototal.getText());
        pc = Double.valueOf(txtprecio.getText());
        bon = (1 - (pv / pc)) * 100;
        fnRedondear r = new fnRedondear();
        txtdescuento.setText(Double.toString(r.dosDigitos(bon)));
    }//GEN-LAST:event_txtpreciototalActionPerformed

    private void txtpreciototalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpreciototalKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtpreciototal.getText())) {
            txtpreciototal.setText("");
        }
    }//GEN-LAST:event_txtpreciototalKeyReleased

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        if (!"".equals(txtnombre.getText()) && !"".equals(txtnombre.getText()) && !"".equals(txtprecio.getText()) && !"".equals(txtpreciototal.getText())) {
            ///////////////////agregar datos//////////////////////////////
            fnCargarFecha fecha = new fnCargarFecha();
            String fechastock = fecha.cargarfechaTipoDate();
           // cboTipoIva iva = (cboTipoIva) cbotipoiva.getSelectedItem();
            int idTipoiva = 3;
            ClaseStock promo = new ClaseStock();
            String[][] datos = new String[tablaproducto2.getRowCount()][5];
            for (int i = 0; i < tablaproducto2.getRowCount(); i++) {
                datos[i][0] = tablaproducto2.getValueAt(i, 0).toString();
                datos[i][1] = tablaproducto2.getValueAt(i, 1).toString();
                datos[i][2] = tablaproducto2.getValueAt(i, 2).toString();
                datos[i][3] = tablaproducto2.getValueAt(i, 3).toString();
                datos[i][4] = tablaproducto2.getValueAt(i, 4).toString();
            }
            if (promo.AgregarPromos(datos, txtcodigo.getText(), txtnombre.getText(), txtprecio.getText(), txtpreciototal.getText(), txtdescuento.getText(), idTipoiva, fechastock) == 1) {

                txtnombre.setText("");
                txtprecio.setText("");
                txtpreciototal.setText("");
                /// cargarTipoIva();
                this.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios");
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
       
            this.dispose();
        
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btncancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btncancelarKeyPressed

    }//GEN-LAST:event_btncancelarKeyPressed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtbuscar.transferFocus();
        }
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void tablaproductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            idProducto = Integer.valueOf(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 0).toString());
            txtnombre.setText(tablaproducto.getValueAt(tablaproducto.getSelectedRow(), 2).toString());
        }
    }//GEN-LAST:event_tablaproductoKeyReleased

    private void txtdescuentoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtdescuentoFocusGained
        double pc = 0.0;
        double uti = 0.0;
        double pv = 0.0;
        if (!txtpreciototal.getText().equals("")) {
            pc = Double.valueOf(txtprecio.getText());
            uti = Double.valueOf(txtdescuento.getText());
            pv = ((uti / 100) + 1) * pc;
            fnRedondear r = new fnRedondear();
            txtpreciototal.setText(Double.toString(r.dosDigitos(pv)));
        } else {
            txtpreciototal.setText("0.0");
        }
    }//GEN-LAST:event_txtdescuentoFocusGained

    private void txtdescuentoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdescuentoKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            txtdescuento.transferFocus();

        }
    }//GEN-LAST:event_txtdescuentoKeyPressed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelContenedor;
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonMaterialIconOne btncancelar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSTableMetro1 tablaproducto;
    private rojerusan.RSTableMetro1 tablaproducto2;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscar;
    private RSMaterialComponent.RSTextFieldMaterial txtcodigo;
    private RSMaterialComponent.RSTextFieldMaterial txtdescuento;
    private RSMaterialComponent.RSTextFieldMaterial txtnombre;
    private RSMaterialComponent.RSTextFieldMaterial txtprecio;
    private RSMaterialComponent.RSTextFieldMaterial txtpreciototal;
    // End of variables declaration//GEN-END:variables
}
