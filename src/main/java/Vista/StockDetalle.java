/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnAlinear;
import Controlador.fnEscape;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static Vista.Stock.idProductos;

public class StockDetalle extends javax.swing.JDialog {

      DefaultTableModel model;
    public StockDetalle(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(450, 100);
        fnEscape.funcionescape(this);
        this.setResizable(false);
        cargartablaproducto(idProductos);
    }

    void cargartablaproducto(String Valor) {
        String[] Titulo = {"Descripcion", "Cantidad", "Fecha Ingreso", "P Compra$", "Bonificacion%", "P Venta 1","P Venta 2", "Iva%", "Deposito"};
        String[] Registros = new String[9];

        String sql = "SELECT * FROM vista_stockdetalle WHERE idProductos = " + Valor + " ";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectStock = null;
        try {
            SelectStock = cn.createStatement();
            ResultSet rs = SelectStock.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(12);
                Registros[7] = rs.getString(7);
                Registros[8] = rs.getString(8);
                txtcodigo.setText(rs.getString(9));
                txtproducto.setText(rs.getString(10));
                model.addRow(Registros);
            }
            tablaproductos.setModel(model);
            tablaproductos.setAutoCreateRowSorter(true);
            tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(200);
            fnAlinear alinear=new fnAlinear();
            tablaproductos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearCentro());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu2 = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        txtproducto = new RSMaterialComponent.RSTextFieldMaterial();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproductos = new rojerusan.RSTableMetro1();
        txtcodigo = new RSMaterialComponent.RSTextFieldMaterial();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        panelcontenedor.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(102, 102, 102), null));
        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu2.setEditable(false);
        lblMenu2.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu2.setText("Detalle del Producto");
        lblMenu2.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu2.setOpaque(false);
        lblMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenu2ActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu2, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 800, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(802, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 820, 20));

        btnsalir.setText("Salir");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLOSE);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 460, 141, -1));

        txtproducto.setEditable(false);
        txtproducto.setPlaceholder("Producto");
        txtproducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtproductoActionPerformed(evt);
            }
        });
        txtproducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtproductoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtproducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 580, -1));

        tablaproductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaproductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaproductosMouseClicked(evt);
            }
        });
        tablaproductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaproductosKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproductos);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 780, 340));

        txtcodigo.setEditable(false);
        txtcodigo.setPlaceholder("Codigo");
        txtcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodigoActionPerformed(evt);
            }
        });
        txtcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcodigoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcodigoKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 170, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenu2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenu2ActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
      
            this.dispose();
      
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed

    }//GEN-LAST:event_btnsalirKeyPressed

    private void tablaproductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaproductosMouseClicked
        
    }//GEN-LAST:event_tablaproductosMouseClicked

    private void tablaproductosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductosKeyReleased
        
    }//GEN-LAST:event_tablaproductosKeyReleased

    private void txtcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodigoActionPerformed

    }//GEN-LAST:event_txtcodigoActionPerformed

    private void txtcodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyPressed
       
    }//GEN-LAST:event_txtcodigoKeyPressed

    private void txtcodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyReleased
       
    }//GEN-LAST:event_txtcodigoKeyReleased

    private void txtproductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtproductoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtproductoKeyReleased

    private void txtproductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtproductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtproductoActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu2;
    private necesario.Panel panelcontenedor;
    private rojerusan.RSTableMetro1 tablaproductos;
    private RSMaterialComponent.RSTextFieldMaterial txtcodigo;
    private RSMaterialComponent.RSTextFieldMaterial txtproducto;
    // End of variables declaration//GEN-END:variables
}
