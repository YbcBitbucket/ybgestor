/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ConexionMySQL;
import Controlador.fnAlinear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ybc
 */
public class Productos extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String idProducto = "";

    public Productos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Logos/ybg.png")).getImage());
        this.setLocation(450, 100);
        this.setResizable(false);
        cargartabla("");
        cargardatoscategoria();
        cargardatosmarca();
        dobleclick();
    }

    //////////////////////FUNCION CARGAR CATEGORIA //////////////////////
    void cargardatoscategoria() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCategoria = null;
        sSQL = "SELECT nombre FROM categorias";
        cbocategorias.removeAllItems();
        cbocategorias.addItem("Categorias");
        try {
            SelectCategoria = cn.createStatement();
            ResultSet rs = SelectCategoria.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbocategorias.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCategoria != null) {
                    SelectCategoria.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR MARCA //////////////////////
    void cargardatosmarca() {
        String sSQL = "";
        String mar = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectMarca = null;
        sSQL = "SELECT nombre FROM marcas";
        cbomarcas.removeAllItems();
        cbomarcas.addItem("Marcas");
        try {
            SelectMarca = cn.createStatement();
            ResultSet rs = SelectMarca.executeQuery(sSQL);
            while (rs.next()) {
                mar = rs.getString("nombre");
                cbomarcas.addItem(mar);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarca != null) {
                    SelectMarca.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    public void cargartabla(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos WHERE CONCAT(codigo, ' ', nombre) "
                + "LIKE '%" + valor + "%'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);

                model.addRow(Registros);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);
        fnAlinear alinear = new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION FILTRAR CATEGORIA//////////////////////
    void filtrarcategoria(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos"
                + " WHERE categoria = '" + valor + "'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);

        fnAlinear alinear = new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);

        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION FILTRAR MARCA//////////////////////
    void filtrarmarca(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos"
                + " WHERE marca='" + valor + "'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);

        fnAlinear alinear = new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablaproductos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablaproductos.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
                        new ProductosModifica(null, true).setVisible(true);
                        cargartabla("");
                    }
                }

            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelcontenedor = new necesario.Panel();
        lblMenu = new rojeru_san.rsfield.RSTextField();
        PanelMenu = new necesario.Panel();
        btncerrar1 = new RSMaterialComponent.RSButtonIconUno();
        rSLabelIcon1 = new RSMaterialComponent.RSLabelIcon();
        txtbuscar = new RSMaterialComponent.RSTextFieldMaterial();
        btnmodificar = new RSMaterialComponent.RSButtonMaterialIconOne();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproductos = new rojerusan.RSTableMetro1();
        btnsalir = new RSMaterialComponent.RSButtonMaterialIconOne();
        btnagregar = new RSMaterialComponent.RSButtonMaterialIconOne();
        btndetalle = new RSMaterialComponent.RSButtonMaterialIconOne();
        cbocategorias = new RSMaterialComponent.RSComboBoxMaterial();
        cbomarcas = new RSMaterialComponent.RSComboBoxMaterial();
        panelcontenedorblanco = new necesario.Panel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        panelcontenedor.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMenu.setEditable(false);
        lblMenu.setForeground(new java.awt.Color(255, 255, 255));
        lblMenu.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblMenu.setText("Productos");
        lblMenu.setCaretColor(new java.awt.Color(255, 255, 255));
        lblMenu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblMenu.setOpaque(false);
        lblMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblMenuActionPerformed(evt);
            }
        });
        panelcontenedor.add(lblMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 0, 690, 20));

        PanelMenu.setColorBackground(new java.awt.Color(102, 102, 102));

        btncerrar1.setBackground(new java.awt.Color(102, 102, 102));
        btncerrar1.setBackgroundHover(new java.awt.Color(204, 23, 50));
        btncerrar1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CANCEL);
        btncerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelMenuLayout = new javax.swing.GroupLayout(PanelMenu);
        PanelMenu.setLayout(PanelMenuLayout);
        PanelMenuLayout.setHorizontalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addContainerGap(692, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        PanelMenuLayout.setVerticalGroup(
            PanelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMenuLayout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addComponent(btncerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelcontenedor.add(PanelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 710, 20));

        rSLabelIcon1.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_IN);
        panelcontenedor.add(rSLabelIcon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 40, 40));

        txtbuscar.setPlaceholder("Buscar Productos");
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        panelcontenedor.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 220, -1));

        btnmodificar.setText("Modificar");
        btnmodificar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnmodificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnmodificar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.UPDATE);
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });
        btnmodificar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnmodificarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnmodificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 450, 141, -1));

        tablaproductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaproductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaproductosMouseClicked(evt);
            }
        });
        tablaproductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaproductosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproductos);

        panelcontenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 690, 352));

        btnsalir.setText("Volver");
        btnsalir.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnsalir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnsalir.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.HOME);
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        btnsalir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnsalirKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 450, 141, -1));

        btnagregar.setText("Agregar");
        btnagregar.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnagregar.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });
        btnagregar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnagregarKeyPressed(evt);
            }
        });
        panelcontenedor.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 141, -1));

        btndetalle.setText("Detalle");
        btndetalle.setBackgroundHover(new java.awt.Color(102, 102, 102));
        btndetalle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btndetalle.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ZOOM_OUT_MAP);
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });
        btndetalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btndetalleKeyPressed(evt);
            }
        });
        panelcontenedor.add(btndetalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 450, 141, -1));

        cbocategorias.setForeground(new java.awt.Color(0, 112, 192));
        cbocategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriasActionPerformed(evt);
            }
        });
        panelcontenedor.add(cbocategorias, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, 190, -1));

        cbomarcas.setForeground(new java.awt.Color(0, 112, 192));
        cbomarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomarcasActionPerformed(evt);
            }
        });
        panelcontenedor.add(cbomarcas, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 40, 200, -1));

        getContentPane().add(panelcontenedor);

        panelcontenedorblanco.setColorBackground(new java.awt.Color(255, 255, 255));
        panelcontenedorblanco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(panelcontenedorblanco);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblMenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblMenuActionPerformed

    private void btncerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncerrar1ActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        int fila = tablaproductos.getRowCount();
        if (fila != 0) {
            txtbuscar.transferFocus();
            tablaproductos.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        int filasel = tablaproductos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            panelcontenedor.setVisible(false);
            panelcontenedorblanco.setVisible(true);
            idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
            new ProductosModifica(null, true).setVisible(true);
            cargartabla("");
            panelcontenedor.setVisible(true);
            panelcontenedorblanco.setVisible(false);
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void btnmodificarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnmodificarKeyPressed

    }//GEN-LAST:event_btnmodificarKeyPressed

    private void tablaproductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaproductosMouseClicked
        if (evt.getClickCount() == 2) {
            btnmodificar.doClick();
        }
    }//GEN-LAST:event_tablaproductosMouseClicked

    private void tablaproductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaproductosKeyPressed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnsalirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsalirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsalirKeyPressed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        panelcontenedor.setVisible(false);
        panelcontenedorblanco.setVisible(true);
        new ProductosAgrega(null, true).setVisible(true);
        cargartabla("");
        panelcontenedor.setVisible(true);
        panelcontenedorblanco.setVisible(false);


    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnagregarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnagregarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnagregarKeyPressed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablaproductos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            panelcontenedor.setVisible(false);
            panelcontenedorblanco.setVisible(true);
            idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
            new ProductosDetalle(null, true).setVisible(true);
            cargartabla("");
            panelcontenedor.setVisible(true);
            panelcontenedorblanco.setVisible(false);
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    private void btndetalleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btndetalleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndetalleKeyPressed

    private void cbocategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriasActionPerformed
        if (cbocategorias.getSelectedItem() != null) {
            if (!cbocategorias.getSelectedItem().toString().equals("Categorias")) {
                filtrarcategoria(cbocategorias.getSelectedItem().toString());
            } else {
                cargartabla("");
            }
        }

    }//GEN-LAST:event_cbocategoriasActionPerformed

    private void cbomarcasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomarcasActionPerformed
        if (cbomarcas.getSelectedItem() != null) {
            if (!cbomarcas.getSelectedItem().toString().equals("Marcas")) {
                filtrarmarca(cbomarcas.getSelectedItem().toString());
            } else {
                cargartabla("");
            }
        }
    }//GEN-LAST:event_cbomarcasActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private necesario.Panel PanelMenu;
    private RSMaterialComponent.RSButtonMaterialIconOne btnagregar;
    private RSMaterialComponent.RSButtonIconUno btncerrar1;
    private RSMaterialComponent.RSButtonMaterialIconOne btndetalle;
    private RSMaterialComponent.RSButtonMaterialIconOne btnmodificar;
    private RSMaterialComponent.RSButtonMaterialIconOne btnsalir;
    private RSMaterialComponent.RSComboBoxMaterial cbocategorias;
    private RSMaterialComponent.RSComboBoxMaterial cbomarcas;
    private javax.swing.JScrollPane jScrollPane1;
    private rojeru_san.rsfield.RSTextField lblMenu;
    private necesario.Panel panelcontenedor;
    private necesario.Panel panelcontenedorblanco;
    private RSMaterialComponent.RSLabelIcon rSLabelIcon1;
    private rojerusan.RSTableMetro1 tablaproductos;
    private RSMaterialComponent.RSTextFieldMaterial txtbuscar;
    // End of variables declaration//GEN-END:variables
}
